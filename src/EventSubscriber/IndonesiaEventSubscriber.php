<?php

namespace Drupal\address_id\EventSubscriber;

use CommerceGuys\Addressing\AddressFormat\AdministrativeAreaType;
use Drupal\address\Event\AddressEvents;
use Drupal\address\Event\AddressFormatEvent;
use Drupal\address\Event\SubdivisionsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Adds a city field and a predefined list of province for Indonesia.
 *
 * city are not provided by the library because they're not used for
 * addressing. However, sites might want to add them for other purposes.
 */
class IndonesiaEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[AddressEvents::ADDRESS_FORMAT][] = ['onAddressFormat'];
    $events[AddressEvents::SUBDIVISIONS][] = ['onSubdivisions'];
    return $events;
  }

  /**
   * Alters the address format for Indonesia.
   *
   * @param \Drupal\address\Event\AddressFormatEvent $event
   *   The address format event.
   */
  public function onAddressFormat(AddressFormatEvent $event): void {
    $definition = $event->getDefinition();
    if ($definition['country_code'] == 'ID') {
      $definition['subdivision_depth'] = 3;
      $definition['format'] = "%givenName %familyName\n%addressLine1\n%addressLine2\n%administrativeArea\n%locality\n%dependentLocality %postalCode";
      $definition['required_fields'][] = "locality";
      $definition['required_fields'][] = "dependentLocality";
      $event->setDefinition($definition);
    }
  }

  /**
   * Provides the subdivisions for Indonesia.
   *
   * @param \Drupal\address\Event\SubdivisionsEvent $event
   *   The subdivisions event.
   */
  public function onSubdivisions(SubdivisionsEvent $event): void {
    $parents = $event->getParents();

    if ($parents == ['ID']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bali' => [
            'iso_code' => 'ID-BA',
            'has_children' => TRUE,
          ],
          'Bangka Belitung' => [
            'iso_code' => 'ID-BB',
            'has_children' => TRUE,
          ],
          'Banten' => [
            'iso_code' => 'ID-BT',
            'has_children' => TRUE,
          ],
          'Bengkulu' => [
            'iso_code' => 'ID-BE',
            'has_children' => TRUE,
          ],
          'DI Yogyakarta' => [
            'iso_code' => 'ID-YO',
            'has_children' => TRUE,
          ],
          'DKI Jakarta' => [
            'iso_code' => 'ID-JK',
            'has_children' => TRUE,
          ],
          'Gorontalo' => [
            'iso_code' => 'ID-GO',
            'has_children' => TRUE,
          ],
          'Jambi' => [
            'iso_code' => 'ID-JA',
            'has_children' => TRUE,
          ],
          'Jawa Barat' => [
            'iso_code' => 'ID-JB',
            'has_children' => TRUE,
          ],
          'Jawa Tengah' => [
            'iso_code' => 'ID-JT',
            'has_children' => TRUE,
          ],
          'Jawa Timur' => [
            'iso_code' => 'ID-JI',
            'has_children' => TRUE,
          ],
          'Kalimantan Barat' => [
            'iso_code' => 'ID-KB',
            'has_children' => TRUE,
          ],
          'Kalimantan Selatan' => [
            'iso_code' => 'ID-KS',
            'has_children' => TRUE,
          ],
          'Kalimantan Tengah' => [
            'iso_code' => 'ID-KT',
            'has_children' => TRUE,
          ],
          'Kalimantan Timur' => [
            'iso_code' => 'ID-KI',
            'has_children' => TRUE,
          ],
          'Kalimantan Utara' => [
            'iso_code' => 'ID-KU',
            'has_children' => TRUE,
          ],
          'Kepulauan Riau' => [
            'iso_code' => 'ID-KR',
            'has_children' => TRUE,
          ],
          'Lampung' => [
            'iso_code' => 'ID-LA',
            'has_children' => TRUE,
          ],
          'Maluku' => [
            'iso_code' => 'ID-MA',
            'has_children' => TRUE,
          ],
          'Maluku Utara' => [
            'iso_code' => 'ID-MU',
            'has_children' => TRUE,
          ],
          'Nanggroe Aceh Darussalam (NAD)' => [
            'iso_code' => 'ID-AC',
            'has_children' => TRUE,
          ],
          'Nusa Tenggara Barat (NTB)' => [
            'iso_code' => 'ID-NB',
            'has_children' => TRUE,
          ],
          'Nusa Tenggara Timur (NTT)' => [
            'iso_code' => 'ID-NT',
            'has_children' => TRUE,
          ],
          'Papua' => [
            'iso_code' => 'ID-PA',
            'has_children' => TRUE,
          ],
          'Papua Barat' => [
            'iso_code' => 'ID-PB',
            'has_children' => TRUE,
          ],
          'Riau' => [
            'iso_code' => 'ID-RI',
            'has_children' => TRUE,
          ],
          'Sulawesi Barat' => [
            'iso_code' => 'ID-SR',
            'has_children' => TRUE,
          ],
          'Sulawesi Selatan' => [
            'iso_code' => 'ID-SN',
            'has_children' => TRUE,
          ],
          'Sulawesi Tengah' => [
            'iso_code' => 'ID-ST',
            'has_children' => TRUE,
          ],
          'Sulawesi Tenggara' => [
            'iso_code' => 'ID-SG',
            'has_children' => TRUE,
          ],
          'Sulawesi Utara' => [
            'iso_code' => 'ID-SA',
            'has_children' => TRUE,
          ],
          'Sumatera Barat' => [
            'iso_code' => 'ID-SB',
            'has_children' => TRUE,
          ],
          'Sumatera Selatan' => [
            'iso_code' => 'ID-SS',
            'has_children' => TRUE,
          ],
          'Sumatera Utara' => [
            'iso_code' => 'ID-SU',
            'has_children' => TRUE,
          ],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['ID', 'Bali']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Badung' => ['has_children' => TRUE],
          'Bangli' => ['has_children' => TRUE],
          'Buleleng' => ['has_children' => TRUE],
          'Denpasar' => ['has_children' => TRUE],
          'Gianyar' => ['has_children' => TRUE],
          'Jembrana' => ['has_children' => TRUE],
          'Karangasem' => ['has_children' => TRUE],
          'Klungkung' => ['has_children' => TRUE],
          'Tabanan' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bangka Belitung']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bangka' => ['has_children' => TRUE],
          'Bangka Barat' => ['has_children' => TRUE],
          'Bangka Selatan' => ['has_children' => TRUE],
          'Bangka Tengah' => ['has_children' => TRUE],
          'Belitung' => ['has_children' => TRUE],
          'Belitung Timur' => ['has_children' => TRUE],
          'Pangkal Pinang' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Banten']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Cilegon' => ['has_children' => TRUE],
          'Lebak' => ['has_children' => TRUE],
          'Pandeglang' => ['has_children' => TRUE],
          'Serang (Kabupaten)' => ['has_children' => TRUE],
          'Serang (Kota)' => ['has_children' => TRUE],
          'Tangerang (Kabupaten)' => ['has_children' => TRUE],
          'Tangerang (Kota)' => ['has_children' => TRUE],
          'Tangerang Selatan' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bengkulu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bengkulu' => ['has_children' => TRUE],
          'Bengkulu Selatan' => ['has_children' => TRUE],
          'Bengkulu Tengah' => ['has_children' => TRUE],
          'Bengkulu Utara' => ['has_children' => TRUE],
          'Kaur' => ['has_children' => TRUE],
          'Kepahiang' => ['has_children' => TRUE],
          'Lebong' => ['has_children' => TRUE],
          'Muko Muko' => ['has_children' => TRUE],
          'Rejang Lebong' => ['has_children' => TRUE],
          'Seluma' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'DI Yogyakarta']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bantul' => ['has_children' => TRUE],
          'Gunung Kidul' => ['has_children' => TRUE],
          'Kulon Progo' => ['has_children' => TRUE],
          'Sleman' => ['has_children' => TRUE],
          'Yogyakarta' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'DKI Jakarta']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Jakarta Barat' => ['has_children' => TRUE],
          'Jakarta Pusat' => ['has_children' => TRUE],
          'Jakarta Selatan' => ['has_children' => TRUE],
          'Jakarta Timur' => ['has_children' => TRUE],
          'Jakarta Utara' => ['has_children' => TRUE],
          'Kepulauan Seribu' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Gorontalo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Boalemo' => ['has_children' => TRUE],
          'Bone Bolango' => ['has_children' => TRUE],
          'Gorontalo (Kabupaten)' => ['has_children' => TRUE],
          'Gorontalo (Kota)' => ['has_children' => TRUE],
          'Gorontalo Utara' => ['has_children' => TRUE],
          'Pohuwato' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jambi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Batang Hari' => ['has_children' => TRUE],
          'Bungo' => ['has_children' => TRUE],
          'Jambi' => ['has_children' => TRUE],
          'Kerinci' => ['has_children' => TRUE],
          'Merangin' => ['has_children' => TRUE],
          'Muaro Jambi' => ['has_children' => TRUE],
          'Sarolangun' => ['has_children' => TRUE],
          'Sungaipenuh' => ['has_children' => TRUE],
          'Tanjung Jabung Barat' => ['has_children' => TRUE],
          'Tanjung Jabung Timur' => ['has_children' => TRUE],
          'Tebo' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bandung (Kabupaten)' => ['has_children' => TRUE],
          'Bandung (Kota)' => ['has_children' => TRUE],
          'Bandung Barat' => ['has_children' => TRUE],
          'Banjar' => ['has_children' => TRUE],
          'Bekasi (Kabupaten)' => ['has_children' => TRUE],
          'Bekasi (Kota)' => ['has_children' => TRUE],
          'Bogor (Kabupaten)' => ['has_children' => TRUE],
          'Bogor (Kota)' => ['has_children' => TRUE],
          'Ciamis' => ['has_children' => TRUE],
          'Cianjur' => ['has_children' => TRUE],
          'Cimahi' => ['has_children' => TRUE],
          'Cirebon (Kabupaten)' => ['has_children' => TRUE],
          'Cirebon (Kota)' => ['has_children' => TRUE],
          'Depok' => ['has_children' => TRUE],
          'Garut' => ['has_children' => TRUE],
          'Indramayu' => ['has_children' => TRUE],
          'Karawang' => ['has_children' => TRUE],
          'Kuningan' => ['has_children' => TRUE],
          'Majalengka' => ['has_children' => TRUE],
          'Pangandaran' => ['has_children' => TRUE],
          'Purwakarta' => ['has_children' => TRUE],
          'Subang' => ['has_children' => TRUE],
          'Sukabumi (Kabupaten)' => ['has_children' => TRUE],
          'Sukabumi (Kota)' => ['has_children' => TRUE],
          'Sumedang' => ['has_children' => TRUE],
          'Tasikmalaya (Kabupaten)' => ['has_children' => TRUE],
          'Tasikmalaya (Kota)' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Banjarnegara' => ['has_children' => TRUE],
          'Banyumas' => ['has_children' => TRUE],
          'Batang' => ['has_children' => TRUE],
          'Blora' => ['has_children' => TRUE],
          'Boyolali' => ['has_children' => TRUE],
          'Brebes' => ['has_children' => TRUE],
          'Cilacap' => ['has_children' => TRUE],
          'Demak' => ['has_children' => TRUE],
          'Grobogan' => ['has_children' => TRUE],
          'Jepara' => ['has_children' => TRUE],
          'Karanganyar' => ['has_children' => TRUE],
          'Kebumen' => ['has_children' => TRUE],
          'Kendal' => ['has_children' => TRUE],
          'Klaten' => ['has_children' => TRUE],
          'Kudus' => ['has_children' => TRUE],
          'Magelang (Kabupaten)' => ['has_children' => TRUE],
          'Magelang (Kota)' => ['has_children' => TRUE],
          'Pati' => ['has_children' => TRUE],
          'Pekalongan (Kabupaten)' => ['has_children' => TRUE],
          'Pekalongan (Kota)' => ['has_children' => TRUE],
          'Pemalang' => ['has_children' => TRUE],
          'Purbalingga' => ['has_children' => TRUE],
          'Purworejo' => ['has_children' => TRUE],
          'Rembang' => ['has_children' => TRUE],
          'Salatiga' => ['has_children' => TRUE],
          'Semarang (Kabupaten)' => ['has_children' => TRUE],
          'Semarang (Kota)' => ['has_children' => TRUE],
          'Sragen' => ['has_children' => TRUE],
          'Sukoharjo' => ['has_children' => TRUE],
          'Surakarta (Solo)' => ['has_children' => TRUE],
          'Tegal (Kabupaten)' => ['has_children' => TRUE],
          'Tegal (Kota)' => ['has_children' => TRUE],
          'Temanggung' => ['has_children' => TRUE],
          'Wonogiri' => ['has_children' => TRUE],
          'Wonosobo' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bangkalan' => ['has_children' => TRUE],
          'Banyuwangi' => ['has_children' => TRUE],
          'Batu' => ['has_children' => TRUE],
          'Blitar (Kabupaten)' => ['has_children' => TRUE],
          'Blitar (Kota)' => ['has_children' => TRUE],
          'Bojonegoro' => ['has_children' => TRUE],
          'Bondowoso' => ['has_children' => TRUE],
          'Gresik' => ['has_children' => TRUE],
          'Jember' => ['has_children' => TRUE],
          'Jombang' => ['has_children' => TRUE],
          'Kediri (Kabupaten)' => ['has_children' => TRUE],
          'Kediri (Kota)' => ['has_children' => TRUE],
          'Lamongan' => ['has_children' => TRUE],
          'Lumajang' => ['has_children' => TRUE],
          'Madiun (Kabupaten)' => ['has_children' => TRUE],
          'Madiun (Kota)' => ['has_children' => TRUE],
          'Magetan' => ['has_children' => TRUE],
          'Malang (Kota)' => ['has_children' => TRUE],
          'Malang (Kabupaten)' => ['has_children' => TRUE],
          'Mojokerto (Kabupaten)' => ['has_children' => TRUE],
          'Mojokerto (Kota)' => ['has_children' => TRUE],
          'Nganjuk' => ['has_children' => TRUE],
          'Ngawi' => ['has_children' => TRUE],
          'Pacitan' => ['has_children' => TRUE],
          'Pamekasan' => ['has_children' => TRUE],
          'Pasuruan (Kabupaten)' => ['has_children' => TRUE],
          'Pasuruan (Kota)' => ['has_children' => TRUE],
          'Ponorogo' => ['has_children' => TRUE],
          'Probolinggo (Kabupaten)' => ['has_children' => TRUE],
          'Probolinggo (Kota)' => ['has_children' => TRUE],
          'Sampang' => ['has_children' => TRUE],
          'Sidoarjo' => ['has_children' => TRUE],
          'Situbondo' => ['has_children' => TRUE],
          'Sumenep' => ['has_children' => TRUE],
          'Surabaya' => ['has_children' => TRUE],
          'Trenggalek' => ['has_children' => TRUE],
          'Tuban' => ['has_children' => TRUE],
          'Tulungagung' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bengkayang' => ['has_children' => TRUE],
          'Kapuas Hulu' => ['has_children' => TRUE],
          'Kayong Utara' => ['has_children' => TRUE],
          'Ketapang' => ['has_children' => TRUE],
          'Kubu Raya' => ['has_children' => TRUE],
          'Landak' => ['has_children' => TRUE],
          'Melawi' => ['has_children' => TRUE],
          'Pontianak (Kabupaten)' => ['has_children' => TRUE],
          'Pontianak (Kota)' => ['has_children' => TRUE],
          'Sambas' => ['has_children' => TRUE],
          'Sanggau' => ['has_children' => TRUE],
          'Sekadau' => ['has_children' => TRUE],
          'Singkawang' => ['has_children' => TRUE],
          'Sintang' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Balangan' => ['has_children' => TRUE],
          'Banjar' => ['has_children' => TRUE],
          'Banjarbaru' => ['has_children' => TRUE],
          'Banjarmasin' => ['has_children' => TRUE],
          'Barito Kuala' => ['has_children' => TRUE],
          'Hulu Sungai Selatan' => ['has_children' => TRUE],
          'Hulu Sungai Tengah' => ['has_children' => TRUE],
          'Hulu Sungai Utara' => ['has_children' => TRUE],
          'Kotabaru' => ['has_children' => TRUE],
          'Tabalong' => ['has_children' => TRUE],
          'Tanah Bumbu' => ['has_children' => TRUE],
          'Tanah Laut' => ['has_children' => TRUE],
          'Tapin' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Barito Selatan' => ['has_children' => TRUE],
          'Barito Timur' => ['has_children' => TRUE],
          'Barito Utara' => ['has_children' => TRUE],
          'Gunung Mas' => ['has_children' => TRUE],
          'Kapuas' => ['has_children' => TRUE],
          'Katingan' => ['has_children' => TRUE],
          'Kotawaringin Barat' => ['has_children' => TRUE],
          'Kotawaringin Timur' => ['has_children' => TRUE],
          'Lamandau' => ['has_children' => TRUE],
          'Murung Raya' => ['has_children' => TRUE],
          'Palangka Raya' => ['has_children' => TRUE],
          'Pulang Pisau' => ['has_children' => TRUE],
          'Seruyan' => ['has_children' => TRUE],
          'Sukamara' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Balikpapan' => ['has_children' => TRUE],
          'Berau' => ['has_children' => TRUE],
          'Bontang' => ['has_children' => TRUE],
          'Kutai Barat' => ['has_children' => TRUE],
          'Kutai Kartanegara' => ['has_children' => TRUE],
          'Kutai Timur' => ['has_children' => TRUE],
          'Paser' => ['has_children' => TRUE],
          'Penajam Paser Utara' => ['has_children' => TRUE],
          'Samarinda' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bulungan (Bulongan)' => ['has_children' => TRUE],
          'Malinau' => ['has_children' => TRUE],
          'Nunukan' => ['has_children' => TRUE],
          'Tana Tidung' => ['has_children' => TRUE],
          'Tarakan' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kepulauan Riau']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Batam' => ['has_children' => TRUE],
          'Bintan' => ['has_children' => TRUE],
          'Karimun' => ['has_children' => TRUE],
          'Kepulauan Anambas' => ['has_children' => TRUE],
          'Lingga' => ['has_children' => TRUE],
          'Natuna' => ['has_children' => TRUE],
          'Tanjung Pinang' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bandar Lampung' => ['has_children' => TRUE],
          'Lampung Barat' => ['has_children' => TRUE],
          'Lampung Selatan' => ['has_children' => TRUE],
          'Lampung Tengah' => ['has_children' => TRUE],
          'Lampung Timur' => ['has_children' => TRUE],
          'Lampung Utara' => ['has_children' => TRUE],
          'Mesuji' => ['has_children' => TRUE],
          'Metro' => ['has_children' => TRUE],
          'Pesawaran' => ['has_children' => TRUE],
          'Pesisir Barat' => ['has_children' => TRUE],
          'Pringsewu' => ['has_children' => TRUE],
          'Tanggamus' => ['has_children' => TRUE],
          'Tulang Bawang' => ['has_children' => TRUE],
          'Tulang Bawang Barat' => ['has_children' => TRUE],
          'Way Kanan' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Ambon' => ['has_children' => TRUE],
          'Buru' => ['has_children' => TRUE],
          'Buru Selatan' => ['has_children' => TRUE],
          'Kepulauan Aru' => ['has_children' => TRUE],
          'Maluku Barat Daya' => ['has_children' => TRUE],
          'Maluku Tengah' => ['has_children' => TRUE],
          'Maluku Tenggara' => ['has_children' => TRUE],
          'Maluku Tenggara Barat' => ['has_children' => TRUE],
          'Seram Bagian Barat' => ['has_children' => TRUE],
          'Seram Bagian Timur' => ['has_children' => TRUE],
          'Tual' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Halmahera Barat' => ['has_children' => TRUE],
          'Halmahera Selatan' => ['has_children' => TRUE],
          'Halmahera Tengah' => ['has_children' => TRUE],
          'Halmahera Timur' => ['has_children' => TRUE],
          'Halmahera Utara' => ['has_children' => TRUE],
          'Kepulauan Sula' => ['has_children' => TRUE],
          'Pulau Morotai' => ['has_children' => TRUE],
          'Ternate' => ['has_children' => TRUE],
          'Tidore Kepulauan' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Aceh Barat' => ['has_children' => TRUE],
          'Aceh Barat Daya' => ['has_children' => TRUE],
          'Aceh Besar' => ['has_children' => TRUE],
          'Aceh Jaya' => ['has_children' => TRUE],
          'Aceh Selatan' => ['has_children' => TRUE],
          'Aceh Singkil' => ['has_children' => TRUE],
          'Aceh Tamiang' => ['has_children' => TRUE],
          'Aceh Tengah' => ['has_children' => TRUE],
          'Aceh Tenggara' => ['has_children' => TRUE],
          'Aceh Timur' => ['has_children' => TRUE],
          'Aceh Utara' => ['has_children' => TRUE],
          'Banda Aceh' => ['has_children' => TRUE],
          'Bener Meriah' => ['has_children' => TRUE],
          'Bireuen' => ['has_children' => TRUE],
          'Gayo Lues' => ['has_children' => TRUE],
          'Langsa' => ['has_children' => TRUE],
          'Lhokseumawe' => ['has_children' => TRUE],
          'Nagan Raya' => ['has_children' => TRUE],
          'Pidie' => ['has_children' => TRUE],
          'Pidie Jaya' => ['has_children' => TRUE],
          'Sabang' => ['has_children' => TRUE],
          'Simeulue' => ['has_children' => TRUE],
          'Subulussalam' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Barat (NTB)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bima (Kabupaten)' => ['has_children' => TRUE],
          'Bima (Kota)' => ['has_children' => TRUE],
          'Dompu' => ['has_children' => TRUE],
          'Lombok Barat' => ['has_children' => TRUE],
          'Lombok Tengah' => ['has_children' => TRUE],
          'Lombok Timur' => ['has_children' => TRUE],
          'Lombok Utara' => ['has_children' => TRUE],
          'Mataram' => ['has_children' => TRUE],
          'Sumbawa' => ['has_children' => TRUE],
          'Sumbawa Barat' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Alor' => ['has_children' => TRUE],
          'Belu' => ['has_children' => TRUE],
          'Ende' => ['has_children' => TRUE],
          'Flores Timur' => ['has_children' => TRUE],
          'Kupang (Kabupaten)' => ['has_children' => TRUE],
          'Kupang (Kota)' => ['has_children' => TRUE],
          'Lembata' => ['has_children' => TRUE],
          'Manggarai' => ['has_children' => TRUE],
          'Manggarai Barat' => ['has_children' => TRUE],
          'Manggarai Timur' => ['has_children' => TRUE],
          'Nagekeo' => ['has_children' => TRUE],
          'Ngada' => ['has_children' => TRUE],
          'Rote Ndao' => ['has_children' => TRUE],
          'Sabu Raijua' => ['has_children' => TRUE],
          'Sikka' => ['has_children' => TRUE],
          'Sumba Barat' => ['has_children' => TRUE],
          'Sumba Barat Daya' => ['has_children' => TRUE],
          'Sumba Tengah' => ['has_children' => TRUE],
          'Sumba Timur' => ['has_children' => TRUE],
          'Timor Tengah Selatan' => ['has_children' => TRUE],
          'Timor Tengah Utara' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Asmat' => ['has_children' => TRUE],
          'Biak Numfor' => ['has_children' => TRUE],
          'Boven Digoel' => ['has_children' => TRUE],
          'Deiyai (Deliyai)' => ['has_children' => TRUE],
          'Dogiyai' => ['has_children' => TRUE],
          'Intan Jaya' => ['has_children' => TRUE],
          'Jayapura (Kabupaten)' => ['has_children' => TRUE],
          'Jayapura (Kota)' => ['has_children' => TRUE],
          'Jayawijaya' => ['has_children' => TRUE],
          'Keerom' => ['has_children' => TRUE],
          'Kepulauan Yapen (Yapen Waropen)' => ['has_children' => TRUE],
          'Lanny Jaya' => ['has_children' => TRUE],
          'Mamberamo Raya' => ['has_children' => TRUE],
          'Mamberamo Tengah' => ['has_children' => TRUE],
          'Mappi' => ['has_children' => TRUE],
          'Merauke' => ['has_children' => TRUE],
          'Mimika' => ['has_children' => TRUE],
          'Nabire' => ['has_children' => TRUE],
          'Nduga' => ['has_children' => TRUE],
          'Paniai' => ['has_children' => TRUE],
          'Pegunungan Bintang' => ['has_children' => TRUE],
          'Puncak' => ['has_children' => TRUE],
          'Puncak Jaya' => ['has_children' => TRUE],
          'Sarmi' => ['has_children' => TRUE],
          'Supiori' => ['has_children' => TRUE],
          'Tolikara' => ['has_children' => TRUE],
          'Waropen' => ['has_children' => TRUE],
          'Yahukimo' => ['has_children' => TRUE],
          'Yalimo' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Fakfak' => ['has_children' => TRUE],
          'Kaimana' => ['has_children' => TRUE],
          'Manokwari' => ['has_children' => TRUE],
          'Manokwari Selatan' => ['has_children' => TRUE],
          'Maybrat' => ['has_children' => TRUE],
          'Pegunungan Arfak' => ['has_children' => TRUE],
          'Raja Ampat' => ['has_children' => TRUE],
          'Sorong (Kabupaten)' => ['has_children' => TRUE],
          'Sorong (Kota)' => ['has_children' => TRUE],
          'Sorong Selatan' => ['has_children' => TRUE],
          'Tambrauw' => ['has_children' => TRUE],
          'Teluk Bintuni' => ['has_children' => TRUE],
          'Teluk Wondama' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Riau']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bengkalis' => ['has_children' => TRUE],
          'Dumai' => ['has_children' => TRUE],
          'Indragiri Hilir' => ['has_children' => TRUE],
          'Indragiri Hulu' => ['has_children' => TRUE],
          'Kampar' => ['has_children' => TRUE],
          'Kepulauan Meranti' => ['has_children' => TRUE],
          'Kuantan Singingi' => ['has_children' => TRUE],
          'Pekanbaru' => ['has_children' => TRUE],
          'Pelalawan' => ['has_children' => TRUE],
          'Rokan Hilir' => ['has_children' => TRUE],
          'Rokan Hulu' => ['has_children' => TRUE],
          'Siak' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Majene' => ['has_children' => TRUE],
          'Mamasa' => ['has_children' => TRUE],
          'Mamuju' => ['has_children' => TRUE],
          'Mamuju Utara' => ['has_children' => TRUE],
          'Polewali Mandar' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bantaeng' => ['has_children' => TRUE],
          'Barru' => ['has_children' => TRUE],
          'Bone' => ['has_children' => TRUE],
          'Bulukumba' => ['has_children' => TRUE],
          'Enrekang' => ['has_children' => TRUE],
          'Gowa' => ['has_children' => TRUE],
          'Jeneponto' => ['has_children' => TRUE],
          'Luwu' => ['has_children' => TRUE],
          'Luwu Timur' => ['has_children' => TRUE],
          'Luwu Utara' => ['has_children' => TRUE],
          'Makassar' => ['has_children' => TRUE],
          'Maros' => ['has_children' => TRUE],
          'Palopo' => ['has_children' => TRUE],
          'Pangkajene Kepulauan' => ['has_children' => TRUE],
          'Parepare' => ['has_children' => TRUE],
          'Pinrang' => ['has_children' => TRUE],
          'Selayar (Kepulauan Selayar)' => ['has_children' => TRUE],
          'Sidenreng Rappang/Rapang' => ['has_children' => TRUE],
          'Sinjai' => ['has_children' => TRUE],
          'Soppeng' => ['has_children' => TRUE],
          'Takalar' => ['has_children' => TRUE],
          'Tana Toraja' => ['has_children' => TRUE],
          'Toraja Utara' => ['has_children' => TRUE],
          'Wajo' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tengah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Banggai' => ['has_children' => TRUE],
          'Banggai Kepulauan' => ['has_children' => TRUE],
          'Buol' => ['has_children' => TRUE],
          'Donggala' => ['has_children' => TRUE],
          'Morowali' => ['has_children' => TRUE],
          'Palu' => ['has_children' => TRUE],
          'Parigi Moutong' => ['has_children' => TRUE],
          'Poso' => ['has_children' => TRUE],
          'Sigi' => ['has_children' => TRUE],
          'Tojo Una-Una' => ['has_children' => TRUE],
          'Toli-Toli' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tenggara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bau-Bau' => ['has_children' => TRUE],
          'Bombana' => ['has_children' => TRUE],
          'Buton' => ['has_children' => TRUE],
          'Buton Utara' => ['has_children' => TRUE],
          'Kendari' => ['has_children' => TRUE],
          'Kolaka' => ['has_children' => TRUE],
          'Kolaka Utara' => ['has_children' => TRUE],
          'Konawe' => ['has_children' => TRUE],
          'Konawe Selatan' => ['has_children' => TRUE],
          'Konawe Utara' => ['has_children' => TRUE],
          'Muna' => ['has_children' => TRUE],
          'Wakatobi' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bitung' => ['has_children' => TRUE],
          'Bolaang Mongondow (Bolmong)' => ['has_children' => TRUE],
          'Bolaang Mongondow Selatan' => ['has_children' => TRUE],
          'Bolaang Mongondow Timur' => ['has_children' => TRUE],
          'Bolaang Mongondow Utara' => ['has_children' => TRUE],
          'Kepulauan Sangihe' => ['has_children' => TRUE],
          'Kepulauan Siau Tagulandang Biaro (Sitaro)' => ['has_children' => TRUE],
          'Kepulauan Talaud' => ['has_children' => TRUE],
          'Kotamobagu' => ['has_children' => TRUE],
          'Manado' => ['has_children' => TRUE],
          'Minahasa' => ['has_children' => TRUE],
          'Minahasa Selatan' => ['has_children' => TRUE],
          'Minahasa Tenggara' => ['has_children' => TRUE],
          'Minahasa Utara' => ['has_children' => TRUE],
          'Tomohon' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Agam' => ['has_children' => TRUE],
          'Bukittinggi' => ['has_children' => TRUE],
          'Dharmasraya' => ['has_children' => TRUE],
          'Kepulauan Mentawai' => ['has_children' => TRUE],
          'Lima Puluh Koto/Kota' => ['has_children' => TRUE],
          'Padang' => ['has_children' => TRUE],
          'Padang Panjang' => ['has_children' => TRUE],
          'Padang Pariaman' => ['has_children' => TRUE],
          'Pariaman' => ['has_children' => TRUE],
          'Pasaman' => ['has_children' => TRUE],
          'Pasaman Barat' => ['has_children' => TRUE],
          'Payakumbuh' => ['has_children' => TRUE],
          'Pesisir Selatan' => ['has_children' => TRUE],
          'Sawah Lunto' => ['has_children' => TRUE],
          'Sijunjung (Sawah Lunto Sijunjung)' => ['has_children' => TRUE],
          'Solok (Kabupaten)' => ['has_children' => TRUE],
          'Solok (Kota)' => ['has_children' => TRUE],
          'Solok Selatan' => ['has_children' => TRUE],
          'Tanah Datar' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Banyuasin' => ['has_children' => TRUE],
          'Empat Lawang' => ['has_children' => TRUE],
          'Lahat' => ['has_children' => TRUE],
          'Lubuk Linggau' => ['has_children' => TRUE],
          'Muara Enim' => ['has_children' => TRUE],
          'Musi Banyuasin' => ['has_children' => TRUE],
          'Musi Rawas' => ['has_children' => TRUE],
          'Ogan Ilir' => ['has_children' => TRUE],
          'Ogan Komering Ilir' => ['has_children' => TRUE],
          'Ogan Komering Ulu' => ['has_children' => TRUE],
          'Ogan Komering Ulu Selatan' => ['has_children' => TRUE],
          'Ogan Komering Ulu Timur' => ['has_children' => TRUE],
          'Pagar Alam' => ['has_children' => TRUE],
          'Palembang' => ['has_children' => TRUE],
          'Prabumulih' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Asahan' => ['has_children' => TRUE],
          'Batu Bara' => ['has_children' => TRUE],
          'Binjai' => ['has_children' => TRUE],
          'Dairi' => ['has_children' => TRUE],
          'Deli Serdang' => ['has_children' => TRUE],
          'Gunungsitoli' => ['has_children' => TRUE],
          'Humbang Hasundutan' => ['has_children' => TRUE],
          'Karo' => ['has_children' => TRUE],
          'Labuhan Batu' => ['has_children' => TRUE],
          'Labuhan Batu Selatan' => ['has_children' => TRUE],
          'Labuhan Batu Utara' => ['has_children' => TRUE],
          'Langkat' => ['has_children' => TRUE],
          'Mandailing Natal' => ['has_children' => TRUE],
          'Medan' => ['has_children' => TRUE],
          'Nias' => ['has_children' => TRUE],
          'Nias Barat' => ['has_children' => TRUE],
          'Nias Selatan' => ['has_children' => TRUE],
          'Nias Utara' => ['has_children' => TRUE],
          'Padang Lawas' => ['has_children' => TRUE],
          'Padang Lawas Utara' => ['has_children' => TRUE],
          'Padang Sidempuan' => ['has_children' => TRUE],
          'Pakpak Bharat' => ['has_children' => TRUE],
          'Pematang Siantar' => ['has_children' => TRUE],
          'Samosir' => ['has_children' => TRUE],
          'Serdang Bedagai' => ['has_children' => TRUE],
          'Sibolga' => ['has_children' => TRUE],
          'Simalungun' => ['has_children' => TRUE],
          'Tanjung Balai' => ['has_children' => TRUE],
          'Tapanuli Selatan' => ['has_children' => TRUE],
          'Tapanuli Tengah' => ['has_children' => TRUE],
          'Tapanuli Utara' => ['has_children' => TRUE],
          'Tebing Tinggi' => ['has_children' => TRUE],
          'Toba Samosir' => ['has_children' => TRUE],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['ID', 'Bali', 'Badung']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Abiansemal" => [],
          "Kuta" => [],
          "Kuta Selatan" => [],
          "Kuta Utara" => [],
          "Mengwi" => [],
          "Petang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bali', 'Bangli']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bangli" => [],
          "Kintamani" => [],
          "Susut" => [],
          "Tembuku" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bali', 'Buleleng']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banjar" => [],
          "Buleleng" => [],
          "Busungbiu" => [],
          "Gerokgak" => [],
          "Kubutambahan" => [],
          "Sawan" => [],
          "Seririt" => [],
          "Sukasada" => [],
          "Tejakula" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bali', 'Denpasar']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Denpasar Barat" => [],
          "Denpasar Selatan" => [],
          "Denpasar Timur" => [],
          "Denpasar Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bali', 'Gianyar']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Belah Batuh (Blahbatuh)" => [],
          "Gianyar" => [],
          "Payangan" => [],
          "Sukawati" => [],
          "Tampak Siring" => [],
          "Tegallalang" => [],
          "Ubud" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bali', 'Jembrana']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Jembrana" => [],
          "Melaya" => [],
          "Mendoyo" => [],
          "Negara" => [],
          "Pekutatan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bali', 'Karangasem']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Abang" => [],
          "Bebandem" => [],
          "Karang Asem" => [],
          "Kubu" => [],
          "Manggis" => [],
          "Rendang" => [],
          "Selat" => [],
          "Sidemen" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bali', 'Klungkung']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banjarangkan" => [],
          "Dawan" => [],
          "Klungkung" => [],
          "Nusapenida" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bali', 'Tabanan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Baturiti" => [],
          "Kediri" => [],
          "Kerambitan" => [],
          "Marga" => [],
          "Penebel" => [],
          "Pupuan" => [],
          "Selemadeg" => [],
          "Selemadeg / Salamadeg Timur" => [],
          "Selemadeg / Salemadeg Barat" => [],
          "Tabanan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bangka Belitung', 'Bangka']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bakam" => [],
          "Belinyu" => [],
          "Mendo Barat" => [],
          "Merawang" => [],
          "Pemali" => [],
          "Puding Besar" => [],
          "Riau Silip" => [],
          "Sungai Liat" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bangka Belitung', 'Bangka Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Jebus" => [],
          "Kelapa" => [],
          "Mentok (Muntok)" => [],
          "Parittiga" => [],
          "Simpang Teritip" => [],
          "Tempilang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bangka Belitung', 'Bangka Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Air Gegas" => [],
          "Kepulauan Pongok" => [],
          "Lepar Pongok" => [],
          "Payung" => [],
          "Pulau Besar" => [],
          "Simpang Rimba" => [],
          "Toboali" => [],
          "Tukak Sadai" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bangka Belitung', 'Bangka Tengah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Koba" => [],
          "Lubuk Besar" => [],
          "Namang" => [],
          "Pangkalan Baru" => [],
          "Simpang Katis" => [],
          "Sungai Selan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bangka Belitung', 'Belitung']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Badau" => [],
          "Membalong" => [],
          "Selat Nasik" => [],
          "Sijuk" => [],
          "Tanjung Pandan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bangka Belitung', 'Belitung Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Damar" => [],
          "Dendang" => [],
          "Gantung" => [],
          "Kelapa Kampit" => [],
          "Manggar" => [],
          "Simpang Pesak" => [],
          "Simpang Renggiang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bangka Belitung', 'Pangkal Pinang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bukit Intan" => [],
          "Gabek" => [],
          "Gerunggang" => [],
          "Girimaya" => [],
          "Pangkal Balam" => [],
          "Rangkui" => [],
          "Taman Sari" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Banten', 'Cilegon']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Cibeber" => [],
          "Cilegon" => [],
          "Citangkil" => [],
          "Ciwandan" => [],
          "Gerogol" => [],
          "Jombang" => [],
          "Pulomerak" => [],
          "Purwakarta" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Banten', 'Lebak']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banjarsari" => [],
          "Bayah" => [],
          "Bojongmanik" => [],
          "Cibadak" => [],
          "Cibeber" => [],
          "Cigemblong" => [],
          "Cihara" => [],
          "Cijaku" => [],
          "Cikulur" => [],
          "Cileles" => [],
          "Cilograng" => [],
          "Cimarga" => [],
          "Cipanas" => [],
          "Cirinten" => [],
          "Curugbitung" => [],
          "Gunung Kencana" => [],
          "Kalanganyar" => [],
          "Lebakgedong" => [],
          "Leuwidamar" => [],
          "Maja" => [],
          "Malingping" => [],
          "Muncang" => [],
          "Panggarangan" => [],
          "Rangkasbitung" => [],
          "Sajira" => [],
          "Sobang" => [],
          "Wanasalam" => [],
          "Warunggunung" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Banten', 'Pandeglang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Angsana" => [],
          "Banjar" => [],
          "Bojong" => [],
          "Cadasari" => [],
          "Carita" => [],
          "Cibaliung" => [],
          "Cibitung" => [],
          "Cigeulis" => [],
          "Cikeudal (Cikedal)" => [],
          "Cikeusik" => [],
          "Cimanggu" => [],
          "Cimanuk" => [],
          "Cipeucang" => [],
          "Cisata" => [],
          "Jiput" => [],
          "Kaduhejo" => [],
          "Karang Tanjung" => [],
          "Koroncong" => [],
          "Labuan" => [],
          "Majasari" => [],
          "Mandalawangi" => [],
          "Mekarjaya" => [],
          "Menes" => [],
          "Munjul" => [],
          "Pagelaran" => [],
          "Pandeglang" => [],
          "Panimbang" => [],
          "Patia" => [],
          "Picung" => [],
          "Pulosari" => [],
          "Saketi" => [],
          "Sindangresmi" => [],
          "Sobang" => [],
          "Sukaresmi" => [],
          "Sumur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Banten', 'Serang (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Anyar" => [],
          "Bandung" => [],
          "Baros" => [],
          "Binuang" => [],
          "Bojonegara" => [],
          "Carenang (Cerenang)" => [],
          "Cikande" => [],
          "Cikeusal" => [],
          "Cinangka" => [],
          "Ciomas" => [],
          "Ciruas" => [],
          "Gunungsari" => [],
          "Jawilan" => [],
          "Kibin" => [],
          "Kopo" => [],
          "Kragilan" => [],
          "Kramatwatu" => [],
          "Lebak Wangi" => [],
          "Mancak" => [],
          "Pabuaran" => [],
          "Padarincang" => [],
          "Pamarayan" => [],
          "Petir" => [],
          "Pontang" => [],
          "Pulo Ampel" => [],
          "Tanara" => [],
          "Tirtayasa" => [],
          "Tunjung Teja" => [],
          "Waringin Kurung" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Banten', 'Serang (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Cipocok Jaya" => [],
          "Curug" => [],
          "Kasemen" => [],
          "Serang" => [],
          "Taktakan" => [],
          "Walantaka" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Banten', 'Tangerang (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Balaraja" => [],
          "Cikupa" => [],
          "Cisauk" => [],
          "Cisoka" => [],
          "Curug" => [],
          "Gunung Kaler" => [],
          "Jambe" => [],
          "Jayanti" => [],
          "Kelapa Dua" => [],
          "Kemiri" => [],
          "Kosambi" => [],
          "Kresek" => [],
          "Kronjo" => [],
          "Legok" => [],
          "Mauk" => [],
          "Mekar Baru" => [],
          "Pagedangan" => [],
          "Pakuhaji" => [],
          "Panongan" => [],
          "Pasar Kemis" => [],
          "Rajeg" => [],
          "Sepatan" => [],
          "Sepatan Timur" => [],
          "Sindang Jaya" => [],
          "Solear" => [],
          "Sukadiri" => [],
          "Sukamulya" => [],
          "Teluknaga" => [],
          "Tigaraksa" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Banten', 'Tangerang (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batuceper" => [],
          "Benda" => [],
          "Cibodas" => [],
          "Ciledug" => [],
          "Cipondoh" => [],
          "Jatiuwung" => [],
          "Karang Tengah" => [],
          "Karawaci" => [],
          "Larangan" => [],
          "Neglasari" => [],
          "Periuk" => [],
          "Pinang (Penang)" => [],
          "Tangerang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Banten', 'Tangerang Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ciputat" => [],
          "Ciputat Timur" => [],
          "Pamulang" => [],
          "Pondok Aren" => [],
          "Serpong" => [],
          "Serpong Utara" => [],
          "Setu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bengkulu', 'Bengkulu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Gading Cempaka" => [],
          "Kampung Melayu" => [],
          "Muara Bangka Hulu" => [],
          "Ratu Agung" => [],
          "Ratu Samban" => [],
          "Selebar" => [],
          "Singaran Pati" => [],
          "Sungai Serut" => [],
          "Teluk Segara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bengkulu', 'Bengkulu Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Air Nipis" => [],
          "Bunga Mas" => [],
          "Kedurang" => [],
          "Kedurang Ilir" => [],
          "Kota Manna" => [],
          "Manna" => [],
          "Pasar Manna" => [],
          "Pino" => [],
          "Pinoraya" => [],
          "Seginim" => [],
          "Ulu Manna" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bengkulu', 'Bengkulu Tengah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bang Haji" => [],
          "Karang Tinggi" => [],
          "Merigi Kelindang" => [],
          "Merigi Sakti" => [],
          "Pagar Jati" => [],
          "Pematang Tiga" => [],
          "Pondok Kelapa" => [],
          "Pondok Kubang" => [],
          "Taba Penanjung" => [],
          "Talang Empat" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bengkulu', 'Bengkulu Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Air Besi" => [],
          "Air Napal" => [],
          "Air Padang" => [],
          "Arga Makmur" => [],
          "Arma Jaya" => [],
          "Batik Nau" => [],
          "Enggano" => [],
          "Giri Mulia" => [],
          "Hulu Palik" => [],
          "Kerkap" => [],
          "Ketahun" => [],
          "Lais" => [],
          "Napal Putih" => [],
          "Padang Jaya" => [],
          "Putri Hijau" => [],
          "Tanjung Agung Palik" => [],
          "Ulok Kupai" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bengkulu', 'Kaur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kaur Selatan" => [],
          "Kaur Tengah" => [],
          "Kaur Utara" => [],
          "Kelam Tengah" => [],
          "Kinal" => [],
          "Luas" => [],
          "Lungkang Kule" => [],
          "Maje" => [],
          "Muara Sahung" => [],
          "Nasal" => [],
          "Padang Guci Hilir" => [],
          "Padang Guci Hulu" => [],
          "Semidang Gumai (Gumay)" => [],
          "Tanjung Kemuning" => [],
          "Tetap (Muara Tetap)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bengkulu', 'Kepahiang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bermani Ilir" => [],
          "Kebawetan (Kabawetan)" => [],
          "Kepahiang" => [],
          "Merigi" => [],
          "Muara Kemumu" => [],
          "Seberang Musi" => [],
          "Tebat Karai" => [],
          "Ujan Mas" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bengkulu', 'Lebong']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Amen" => [],
          "Bingin Kuning" => [],
          "Lebong Atas" => [],
          "Lebong Sakti" => [],
          "Lebong Selatan" => [],
          "Lebong Tengah" => [],
          "Lebong Utara" => [],
          "Pelabai" => [],
          "Pinang Belapis" => [],
          "Rimbo Pengadang" => [],
          "Topos" => [],
          "Uram Jaya" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bengkulu', 'Muko Muko']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Air Dikit" => [],
          "Air Majunto" => [],
          "Air Rami" => [],
          "Ipuh (Muko-Muko Selatan)" => [],
          "Kota Mukomuko (Mukomuko Utara)" => [],
          "Lubuk Pinang" => [],
          "Malin Deman" => [],
          "Penarik" => [],
          "Pondok Suguh" => [],
          "Selagan Raya" => [],
          "Sungai Rumbai" => [],
          "Teramang Jaya" => [],
          "Teras Terunjam" => [],
          "V Koto" => [],
          "XIV Koto" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bengkulu', 'Rejang Lebong']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bermani Ulu" => [],
          "Bermani Ulu Raya" => [],
          "Binduriang" => [],
          "Curup" => [],
          "Curup Selatan" => [],
          "Curup Tengah" => [],
          "Curup Timur" => [],
          "Curup Utara" => [],
          "Kota Padang" => [],
          "Padang Ulak Tanding" => [],
          "Selupu Rejang" => [],
          "Sindang Beliti Ilir" => [],
          "Sindang Beliti Ulu" => [],
          "Sindang Daratan" => [],
          "Sindang Kelingi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Bengkulu', 'Seluma']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Air Periukan" => [],
          "Ilir Talo" => [],
          "Lubuk Sandi" => [],
          "Seluma" => [],
          "Seluma Barat" => [],
          "Seluma Selatan" => [],
          "Seluma Timur" => [],
          "Seluma Utara" => [],
          "Semidang Alas" => [],
          "Semidang Alas Maras" => [],
          "Sukaraja" => [],
          "Talo" => [],
          "Talo Kecil" => [],
          "Ulu Talo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'DI Yogyakarta', 'Bantul']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bambang Lipuro" => [],
          "Banguntapan" => [],
          "Bantul" => [],
          "Dlingo" => [],
          "Imogiri" => [],
          "Jetis" => [],
          "Kasihan" => [],
          "Kretek" => [],
          "Pajangan" => [],
          "Pandak" => [],
          "Piyungan" => [],
          "Pleret" => [],
          "Pundong" => [],
          "Sanden" => [],
          "Sedayu" => [],
          "Sewon" => [],
          "Srandakan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'DI Yogyakarta', 'Gunung Kidul']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Gedang Sari" => [],
          "Girisubo" => [],
          "Karangmojo" => [],
          "Ngawen" => [],
          "Nglipar" => [],
          "Paliyan" => [],
          "Panggang" => [],
          "Patuk" => [],
          "Playen" => [],
          "Ponjong" => [],
          "Purwosari" => [],
          "Rongkop" => [],
          "Sapto Sari" => [],
          "Semanu" => [],
          "Semin" => [],
          "Tanjungsari" => [],
          "Tepus" => [],
          "Wonosari" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'DI Yogyakarta', 'Kulon Progo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Galur" => [],
          "Girimulyo" => [],
          "Kalibawang" => [],
          "Kokap" => [],
          "Lendah" => [],
          "Nanggulan" => [],
          "Panjatan" => [],
          "Pengasih" => [],
          "Samigaluh" => [],
          "Sentolo" => [],
          "Temon" => [],
          "Wates" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'DI Yogyakarta', 'Sleman']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Berbah" => [],
          "Cangkringan" => [],
          "Depok" => [],
          "Gamping" => [],
          "Godean" => [],
          "Kalasan" => [],
          "Minggir" => [],
          "Mlati" => [],
          "Moyudan" => [],
          "Ngaglik" => [],
          "Ngemplak" => [],
          "Pakem" => [],
          "Prambanan" => [],
          "Seyegan" => [],
          "Sleman" => [],
          "Tempel" => [],
          "Turi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'DI Yogyakarta', 'Yogyakarta']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Danurejan" => [],
          "Gedong Tengen" => [],
          "Gondokusuman" => [],
          "Gondomanan" => [],
          "Jetis" => [],
          "Kotagede" => [],
          "Kraton" => [],
          "Mantrijeron" => [],
          "Mergangsan" => [],
          "Ngampilan" => [],
          "Pakualaman" => [],
          "Tegalrejo" => [],
          "Umbulharjo" => [],
          "Wirobrajan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'DKI Jakarta', 'Jakarta Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Cengkareng" => [],
          "Grogol Petamburan" => [],
          "Kalideres" => [],
          "Kebon Jeruk" => [],
          "Kembangan" => [],
          "Palmerah" => [],
          "Taman Sari" => [],
          "Tambora" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'DKI Jakarta', 'Jakarta Pusat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Cempaka Putih" => [],
          "Gambir" => [],
          "Johar Baru" => [],
          "Kemayoran" => [],
          "Menteng" => [],
          "Sawah Besar" => [],
          "Senen" => [],
          "Tanah Abang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'DKI Jakarta', 'Jakarta Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Cilandak" => [],
          "Jagakarsa" => [],
          "Kebayoran Baru" => [],
          "Kebayoran Lama" => [],
          "Mampang Prapatan" => [],
          "Pancoran" => [],
          "Pasar Minggu" => [],
          "Pesanggrahan" => [],
          "Setia Budi" => [],
          "Tebet" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'DKI Jakarta', 'Jakarta Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Cakung" => [],
          "Cipayung" => [],
          "Ciracas" => [],
          "Duren Sawit" => [],
          "Jatinegara" => [],
          "Kramat Jati" => [],
          "Makasar" => [],
          "Matraman" => [],
          "Pasar Rebo" => [],
          "Pulo Gadung" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'DKI Jakarta', 'Jakarta Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Cilincing" => [],
          "Kelapa Gading" => [],
          "Koja" => [],
          "Pademangan" => [],
          "Penjaringan" => [],
          "Tanjung Priok" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'DKI Jakarta', 'Kepulauan Seribu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kepulauan Seribu Selatan" => [],
          "Kepulauan Seribu Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Gorontalo', 'Boalemo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Botumoita (Botumoito)" => [],
          "Dulupi" => [],
          "Mananggu" => [],
          "Paguyaman" => [],
          "Paguyaman Pantai" => [],
          "Tilamuta" => [],
          "Wonosari" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Gorontalo', 'Bone Bolango']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bone" => [],
          "Bone Raya" => [],
          "Bonepantai" => [],
          "Botu Pingge" => [],
          "Bulango Selatan" => [],
          "Bulango Timur" => [],
          "Bulango Ulu" => [],
          "Bulango Utara" => [],
          "Bulawa" => [],
          "Kabila" => [],
          "Kabila Bone" => [],
          "Pinogu" => [],
          "Suwawa" => [],
          "Suwawa Selatan" => [],
          "Suwawa Tengah" => [],
          "Suwawa Timur" => [],
          "Tapa" => [],
          "Tilongkabila" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Gorontalo', 'Gorontalo (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Asparaga" => [],
          "Batudaa" => [],
          "Batudaa Pantai" => [],
          "Bilato" => [],
          "Biluhu" => [],
          "Boliohuto (Boliyohuto)" => [],
          "Bongomeme" => [],
          "Dungaliyo" => [],
          "Limboto" => [],
          "Limboto Barat" => [],
          "Mootilango" => [],
          "Pulubala" => [],
          "Tabongo" => [],
          "Telaga" => [],
          "Telaga Biru" => [],
          "Telaga Jaya" => [],
          "Tibawa" => [],
          "Tilango" => [],
          "Tolangohula" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Gorontalo', 'Gorontalo (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Dumbo Raya" => [],
          "Dungingi" => [],
          "Hulonthalangi" => [],
          "Kota Barat" => [],
          "Kota Selatan" => [],
          "Kota Tengah" => [],
          "Kota Timur" => [],
          "Kota Utara" => [],
          "Sipatana" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Gorontalo', 'Gorontalo Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Anggrek" => [],
          "Atinggola" => [],
          "Biau" => [],
          "Gentuma Raya" => [],
          "Kwandang" => [],
          "Monano" => [],
          "Ponelo Kepulauan" => [],
          "Sumalata" => [],
          "Sumalata Timur" => [],
          "Tolinggula" => [],
          "Tomolito" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Gorontalo', 'Pohuwato']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Buntulia" => [],
          "Dengilo" => [],
          "Duhiadaa" => [],
          "Lemito" => [],
          "Marisa" => [],
          "Paguat" => [],
          "Patilanggio" => [],
          "Popayato" => [],
          "Popayato Barat" => [],
          "Popayato Timur" => [],
          "Randangan" => [],
          "Taluditi (Taluduti)" => [],
          "Wanggarasi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jambi', 'Batang Hari']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bajubang" => [],
          "Batin XXIV" => [],
          "Maro Sebo Ilir" => [],
          "Maro Sebo Ulu" => [],
          "Mersam" => [],
          "Muara Bulian" => [],
          "Muara Tembesi" => [],
          "Pemayung" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jambi', 'Bungo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bathin II Babeko" => [],
          "Bathin II Pelayang" => [],
          "Bathin III" => [],
          "Bathin III Ulu" => [],
          "Bungo Dani" => [],
          "Jujuhan" => [],
          "Jujuhan Ilir" => [],
          "Limbur Lubuk Mengkuang" => [],
          "Muko-Muko Batin VII" => [],
          "Pasar Muara Bungo" => [],
          "Pelepat" => [],
          "Pelepat Ilir" => [],
          "Rantau Pandan" => [],
          "Rimbo Tengah" => [],
          "Tanah Sepenggal" => [],
          "Tanah Sepenggal Lintas" => [],
          "Tanah Tumbuh" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jambi', 'Jambi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Danau Teluk" => [],
          "Jambi Selatan" => [],
          "Jambi Timur" => [],
          "Jelutung" => [],
          "Kota Baru" => [],
          "Pasar Jambi" => [],
          "Pelayangan" => [],
          "Telanaipura" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jambi', 'Kerinci']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Air Hangat" => [],
          "Air Hangat Barat" => [],
          "Air Hangat Timur" => [],
          "Batang Merangin" => [],
          "Bukitkerman" => [],
          "Danau Kerinci" => [],
          "Depati Tujuh" => [],
          "Gunung Kerinci" => [],
          "Gunung Raya" => [],
          "Gunung Tujuh" => [],
          "Kayu Aro" => [],
          "Kayu Aro Barat" => [],
          "Keliling Danau" => [],
          "Sitinjau Laut" => [],
          "Siulak" => [],
          "Siulak Mukai" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jambi', 'Merangin']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bangko" => [],
          "Bangko Barat" => [],
          "Batang Masumai" => [],
          "Jangkat" => [],
          "Lembah Masurai" => [],
          "Margo Tabir" => [],
          "Muara Siau" => [],
          "Nalo Tantan" => [],
          "Pamenang" => [],
          "Pamenang Barat" => [],
          "Pamenang Selatan" => [],
          "Pangkalan Jambu" => [],
          "Renah Pembarap" => [],
          "Renah Pemenang" => [],
          "Sungai Manau" => [],
          "Sungai Tenang" => [],
          "Tabir" => [],
          "Tabir Barat" => [],
          "Tabir Ilir" => [],
          "Tabir Lintas" => [],
          "Tabir Selatan" => [],
          "Tabir Timur" => [],
          "Tabir Ulu" => [],
          "Tiang Pumpung" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jambi', 'Muaro Jambi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bahar Selatan" => [],
          "Bahar Utara" => [],
          "Jambi Luar Kota" => [],
          "Kumpeh" => [],
          "Kumpeh Ulu" => [],
          "Maro Sebo" => [],
          "Mestong" => [],
          "Sekernan" => [],
          "Sungai Bahar" => [],
          "Sungai Gelam" => [],
          "Taman Rajo / Rejo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jambi', 'Sarolangun']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Air Hitam" => [],
          "Batang Asai" => [],
          "Bathin VIII (Batin VIII)" => [],
          "Cermin Nan Gadang" => [],
          "Limun" => [],
          "Mandiangin" => [],
          "Pauh" => [],
          "Pelawan" => [],
          "Sarolangun" => [],
          "Singkut" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jambi', 'Sungaipenuh']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Hamparan Rawang" => [],
          "Koto Baru" => [],
          "Kumun Debai" => [],
          "Pesisir Bukit" => [],
          "Pondok Tinggi" => [],
          "Sungai Bungkal" => [],
          "Sungai Penuh" => [],
          "Tanah Kampung" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jambi', 'Tanjung Jabung Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batang Asam" => [],
          "Betara" => [],
          "Bram Itam" => [],
          "Kuala Betara" => [],
          "Merlung" => [],
          "Muara Papalik" => [],
          "Pengabuan" => [],
          "Renah Mendaluh" => [],
          "Seberang Kota" => [],
          "Senyerang" => [],
          "Tebing Tinggi" => [],
          "Tungkal Ilir" => [],
          "Tungkal Ulu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jambi', 'Tanjung Jabung Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Berbak" => [],
          "Dendang" => [],
          "Geragai" => [],
          "Kuala Jambi" => [],
          "Mendahara" => [],
          "Mendahara Ulu" => [],
          "Muara Sabak Barat" => [],
          "Muara Sabak Timur" => [],
          "Nipah Panjang" => [],
          "Rantau Rasau" => [],
          "Sadu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jambi', 'Tebo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Muara Tabir" => [],
          "Rimbo Bujang" => [],
          "Rimbo Ilir" => [],
          "Rimbo Ulu" => [],
          "Serai Serumpun" => [],
          "Sumay" => [],
          "Tebo Ilir" => [],
          "Tebo Tengah" => [],
          "Tebo Ulu" => [],
          "Tengah Ilir" => [],
          "VII Koto" => [],
          "VII Koto Ilir" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Bandung (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Arjasari" => [],
          "Baleendah" => [],
          "Banjaran" => [],
          "Bojongsoang" => [],
          "Cangkuang" => [],
          "Cicalengka" => [],
          "Cikancung" => [],
          "Cilengkrang" => [],
          "Cileunyi" => [],
          "Cimaung" => [],
          "Cimeunyan" => [],
          "Ciparay" => [],
          "Ciwidey" => [],
          "Dayeuhkolot" => [],
          "Ibun" => [],
          "Katapang" => [],
          "Kertasari" => [],
          "Kutawaringin" => [],
          "Majalaya" => [],
          "Margaasih" => [],
          "Margahayu" => [],
          "Nagreg" => [],
          "Pacet" => [],
          "Pameungpeuk" => [],
          "Pangalengan" => [],
          "Paseh" => [],
          "Pasirjambu" => [],
          "Ranca Bali" => [],
          "Rancaekek" => [],
          "Solokan Jeruk" => [],
          "Soreang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Bandung (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Andir" => [],
          "Antapani (Cicadas)" => [],
          "Arcamanik" => [],
          "Astana Anyar" => [],
          "Babakan Ciparay" => [],
          "Bandung Kidul" => [],
          "Bandung Kulon" => [],
          "Bandung Wetan" => [],
          "Batununggal" => [],
          "Bojongloa Kaler" => [],
          "Bojongloa Kidul" => [],
          "Buahbatu (Margacinta)" => [],
          "Cibeunying Kaler" => [],
          "Cibeunying Kidul" => [],
          "Cibiru" => [],
          "Cicendo" => [],
          "Cidadap" => [],
          "Cinambo" => [],
          "Coblong" => [],
          "Gedebage" => [],
          "Kiaracondong" => [],
          "Lengkong" => [],
          "Mandalajati" => [],
          "Panyileukan" => [],
          "Rancasari" => [],
          "Regol" => [],
          "Sukajadi" => [],
          "Sukasari" => [],
          "Sumur Bandung" => [],
          "Ujung Berung" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Bandung Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batujajar" => [],
          "Cihampelas" => [],
          "Cikalong Wetan" => [],
          "Cililin" => [],
          "Cipatat" => [],
          "Cipeundeuy" => [],
          "Cipongkor" => [],
          "Cisarua" => [],
          "Gununghalu" => [],
          "Lembang" => [],
          "Ngamprah" => [],
          "Padalarang" => [],
          "Parongpong" => [],
          "Rongga" => [],
          "Saguling" => [],
          "Sindangkerta" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Banjar']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banjar" => [],
          "Langensari" => [],
          "Pataruman" => [],
          "Purwaharja" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Bekasi (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Babelan" => [],
          "Bojongmangu" => [],
          "Cabangbungin" => [],
          "Cibarusah" => [],
          "Cibitung" => [],
          "Cikarang Barat" => [],
          "Cikarang Pusat" => [],
          "Cikarang Selatan" => [],
          "Cikarang Timur" => [],
          "Cikarang Utara" => [],
          "Karangbahagia" => [],
          "Kedung Waringin" => [],
          "Muara Gembong" => [],
          "Pebayuran" => [],
          "Serang Baru" => [],
          "Setu" => [],
          "Sukakarya" => [],
          "Sukatani" => [],
          "Sukawangi" => [],
          "Tambelang" => [],
          "Tambun Selatan" => [],
          "Tambun Utara" => [],
          "Tarumajaya" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Bekasi (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bantar Gebang" => [],
          "Bekasi Barat" => [],
          "Bekasi Selatan" => [],
          "Bekasi Timur" => [],
          "Bekasi Utara" => [],
          "Jati Sampurna" => [],
          "Jatiasih" => [],
          "Medan Satria" => [],
          "Mustika Jaya" => [],
          "Pondok Gede" => [],
          "Pondok Melati" => [],
          "Rawalumbu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Bogor (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Babakan Madang" => [],
          "Bojonggede" => [],
          "Caringin" => [],
          "Cariu" => [],
          "Ciampea" => [],
          "Ciawi" => [],
          "Cibinong" => [],
          "Cibungbulang" => [],
          "Cigombong" => [],
          "Cigudeg" => [],
          "Cijeruk" => [],
          "Cileungsi" => [],
          "Ciomas" => [],
          "Cisarua" => [],
          "Ciseeng" => [],
          "Citeureup" => [],
          "Dramaga" => [],
          "Gunung Putri" => [],
          "Gunung Sindur" => [],
          "Jasinga" => [],
          "Jonggol" => [],
          "Kemang" => [],
          "Klapa Nunggal (Kelapa Nunggal)" => [],
          "Leuwiliang" => [],
          "Leuwisadeng" => [],
          "Megamendung" => [],
          "Nanggung" => [],
          "Pamijahan" => [],
          "Parung" => [],
          "Parung Panjang" => [],
          "Ranca Bungur" => [],
          "Rumpin" => [],
          "Sukajaya" => [],
          "Sukamakmur" => [],
          "Sukaraja" => [],
          "Tajurhalang" => [],
          "Tamansari" => [],
          "Tanjungsari" => [],
          "Tenjo" => [],
          "Tenjolaya" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Bogor (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bogor Barat - Kota" => [],
          "Bogor Selatan - Kota" => [],
          "Bogor Tengah - Kota" => [],
          "Bogor Timur - Kota" => [],
          "Bogor Utara - Kota" => [],
          "Tanah Sereal" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Ciamis']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banjarsari" => [],
          "Baregbeg" => [],
          "Ciamis" => [],
          "Cidolog" => [],
          "Cihaurbeuti" => [],
          "Cijeungjing" => [],
          "Cikoneng" => [],
          "Cimaragas" => [],
          "Cipaku" => [],
          "Cisaga" => [],
          "Jatinagara" => [],
          "Kawali" => [],
          "Lakbok" => [],
          "Lumbung" => [],
          "Pamarican" => [],
          "Panawangan" => [],
          "Panjalu" => [],
          "Panumbangan" => [],
          "Purwadadi" => [],
          "Rajadesa" => [],
          "Rancah" => [],
          "Sadananya" => [],
          "Sindangkasih" => [],
          "Sukadana" => [],
          "Sukamantri" => [],
          "Tambaksari" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Cianjur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Agrabinta" => [],
          "Bojongpicung" => [],
          "Campaka" => [],
          "Campaka Mulya" => [],
          "Cianjur" => [],
          "Cibeber" => [],
          "Cibinong" => [],
          "Cidaun" => [],
          "Cijati" => [],
          "Cikadu" => [],
          "Cikalongkulon" => [],
          "Cilaku" => [],
          "Cipanas" => [],
          "Ciranjang" => [],
          "Cugenang" => [],
          "Gekbrong" => [],
          "Haurwangi" => [],
          "Kadupandak" => [],
          "Karangtengah" => [],
          "Leles" => [],
          "Mande" => [],
          "Naringgul" => [],
          "Pacet" => [],
          "Pagelaran" => [],
          "Pasirkuda" => [],
          "Sindangbarang" => [],
          "Sukaluyu" => [],
          "Sukanagara" => [],
          "Sukaresmi" => [],
          "Takokak" => [],
          "Tanggeung" => [],
          "Warungkondang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Cimahi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Cimahi Selatan" => [],
          "Cimahi Tengah" => [],
          "Cimahi Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Cirebon (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Arjawinangun" => [],
          "Astanajapura" => [],
          "Babakan" => [],
          "Beber" => [],
          "Ciledug" => [],
          "Ciwaringin" => [],
          "Depok" => [],
          "Dukupuntang" => [],
          "Gebang" => [],
          "Gegesik" => [],
          "Gempol" => [],
          "Greged (Greget)" => [],
          "Gunung Jati (Cirebon Utara)" => [],
          "Jamblang" => [],
          "Kaliwedi" => [],
          "Kapetakan" => [],
          "Karangsembung" => [],
          "Karangwareng" => [],
          "Kedawung" => [],
          "Klangenan" => [],
          "Lemahabang" => [],
          "Losari" => [],
          "Mundu" => [],
          "Pabedilan" => [],
          "Pabuaran" => [],
          "Palimanan" => [],
          "Pangenan" => [],
          "Panguragan" => [],
          "Pasaleman" => [],
          "Plered" => [],
          "Plumbon" => [],
          "Sedong" => [],
          "Sumber" => [],
          "Suranenggala" => [],
          "Susukan" => [],
          "Susukan Lebak" => [],
          "Talun (Cirebon Selatan)" => [],
          "Tengah Tani" => [],
          "Waled" => [],
          "Weru" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Cirebon (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Harjamukti" => [],
          "Kejaksan" => [],
          "Kesambi" => [],
          "Lemahwungkuk" => [],
          "Pekalipan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Depok']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Beji" => [],
          "Bojongsari" => [],
          "Cilodong" => [],
          "Cimanggis" => [],
          "Cinere" => [],
          "Cipayung" => [],
          "Limo" => [],
          "Pancoran Mas" => [],
          "Sawangan" => [],
          "Sukmajaya" => [],
          "Tapos" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Garut']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banjarwangi" => [],
          "Banyuresmi" => [],
          "Bayongbong" => [],
          "Blubur Limbangan" => [],
          "Bungbulang" => [],
          "Caringin" => [],
          "Cibalong" => [],
          "Cibatu" => [],
          "Cibiuk" => [],
          "Cigedug" => [],
          "Cihurip" => [],
          "Cikajang" => [],
          "Cikelet" => [],
          "Cilawu" => [],
          "Cisewu" => [],
          "Cisompet" => [],
          "Cisurupan" => [],
          "Garut Kota" => [],
          "Kadungora" => [],
          "Karangpawitan" => [],
          "Karangtengah" => [],
          "Kersamanah" => [],
          "Leles" => [],
          "Leuwigoong" => [],
          "Malangbong" => [],
          "Mekarmukti" => [],
          "Pakenjeng" => [],
          "Pameungpeuk" => [],
          "Pamulihan" => [],
          "Pangatikan" => [],
          "Pasirwangi" => [],
          "Peundeuy" => [],
          "Samarang" => [],
          "Selaawi" => [],
          "Singajaya" => [],
          "Sucinaraja" => [],
          "Sukaresmi" => [],
          "Sukawening" => [],
          "Talegong" => [],
          "Tarogong Kaler" => [],
          "Tarogong Kidul" => [],
          "Wanaraja" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Indramayu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Anjatan" => [],
          "Arahan" => [],
          "Balongan" => [],
          "Bangodua" => [],
          "Bongas" => [],
          "Cantigi" => [],
          "Cikedung" => [],
          "Gabuswetan" => [],
          "Gantar" => [],
          "Haurgeulis" => [],
          "Indramayu" => [],
          "Jatibarang" => [],
          "Juntinyuat" => [],
          "Kandanghaur" => [],
          "Karangampel" => [],
          "Kedokan Bunder" => [],
          "Kertasemaya" => [],
          "Krangkeng" => [],
          "Kroya" => [],
          "Lelea" => [],
          "Lohbener" => [],
          "Losarang" => [],
          "Pasekan" => [],
          "Patrol" => [],
          "Sindang" => [],
          "Sliyeg" => [],
          "Sukagumiwang" => [],
          "Sukra" => [],
          "Trisi/Terisi" => [],
          "Tukdana" => [],
          "Widasari" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Karawang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banyusari" => [],
          "Batujaya" => [],
          "Ciampel" => [],
          "Cibuaya" => [],
          "Cikampek" => [],
          "Cilamaya Kulon" => [],
          "Cilamaya Wetan" => [],
          "Cilebar" => [],
          "Jatisari" => [],
          "Jayakerta" => [],
          "Karawang Barat" => [],
          "Karawang Timur" => [],
          "Klari" => [],
          "Kotabaru" => [],
          "Kutawaluya" => [],
          "Lemahabang" => [],
          "Majalaya" => [],
          "Pakisjaya" => [],
          "Pangkalan" => [],
          "Pedes" => [],
          "Purwasari" => [],
          "Rawamerta" => [],
          "Rengasdengklok" => [],
          "Talagasari" => [],
          "Tegalwaru" => [],
          "Telukjambe Barat" => [],
          "Telukjambe Timur" => [],
          "Tempuran" => [],
          "Tirtajaya" => [],
          "Tirtamulya" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Kuningan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ciawigebang" => [],
          "Cibeureum" => [],
          "Cibingbin" => [],
          "Cidahu" => [],
          "Cigandamekar" => [],
          "Cigugur" => [],
          "Cilebak" => [],
          "Cilimus" => [],
          "Cimahi" => [],
          "Ciniru" => [],
          "Cipicung" => [],
          "Ciwaru" => [],
          "Darma" => [],
          "Garawangi" => [],
          "Hantara" => [],
          "Jalaksana" => [],
          "Japara" => [],
          "Kadugede" => [],
          "Kalimanggis" => [],
          "Karangkancana" => [],
          "Kramat Mulya" => [],
          "Kuningan" => [],
          "Lebakwangi" => [],
          "Luragung" => [],
          "Maleber" => [],
          "Mandirancan" => [],
          "Nusaherang" => [],
          "Pancalang" => [],
          "Pasawahan" => [],
          "Selajambe" => [],
          "Sindangagung" => [],
          "Subang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Majalengka']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Argapura" => [],
          "Banjaran" => [],
          "Bantarujeg" => [],
          "Cigasong" => [],
          "Cikijing" => [],
          "Cingambul" => [],
          "Dawuan" => [],
          "Jatitujuh" => [],
          "Jatiwangi" => [],
          "Kadipaten" => [],
          "Kasokandel" => [],
          "Kertajati" => [],
          "Lemahsugih" => [],
          "Leuwimunding" => [],
          "Ligung" => [],
          "Maja" => [],
          "Majalengka" => [],
          "Malausma" => [],
          "Palasah" => [],
          "Panyingkiran" => [],
          "Rajagaluh" => [],
          "Sindang" => [],
          "Sindangwangi" => [],
          "Sukahaji" => [],
          "Sumberjaya" => [],
          "Talaga" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Pangandaran']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Cigugur" => [],
          "Cijulang" => [],
          "Cimerak" => [],
          "Kalipucang" => [],
          "Langkaplancar" => [],
          "Mangunjaya" => [],
          "Padaherang" => [],
          "Pangandaran" => [],
          "Parigi" => [],
          "Sidamulih" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Purwakarta']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Babakancikao" => [],
          "Bojong" => [],
          "Bungursari" => [],
          "Campaka" => [],
          "Cibatu" => [],
          "Darangdan" => [],
          "Jatiluhur" => [],
          "Kiarapedes" => [],
          "Maniis" => [],
          "Pasawahan" => [],
          "Plered" => [],
          "Pondoksalam" => [],
          "Purwakarta" => [],
          "Sukasari" => [],
          "Sukatani" => [],
          "Tegal Waru" => [],
          "Wanayasa" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Subang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Binong" => [],
          "Blanakan" => [],
          "Ciasem" => [],
          "Ciater" => [],
          "Cibogo" => [],
          "Cijambe" => [],
          "Cikaum" => [],
          "Cipeundeuy" => [],
          "Cipunagara" => [],
          "Cisalak" => [],
          "Compreng" => [],
          "Dawuan" => [],
          "Jalancagak" => [],
          "Kalijati" => [],
          "Kasomalang" => [],
          "Legonkulon" => [],
          "Pabuaran" => [],
          "Pagaden" => [],
          "Pagaden Barat" => [],
          "Pamanukan" => [],
          "Patokbeusi" => [],
          "Purwadadi" => [],
          "Pusakajaya" => [],
          "Pusakanagara" => [],
          "Sagalaherang" => [],
          "Serangpanjang" => [],
          "Subang" => [],
          "Sukasari" => [],
          "Tambakdahan" => [],
          "Tanjungsiang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Sukabumi (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bantargadung" => [],
          "Bojong Genteng" => [],
          "Caringin" => [],
          "Ciambar" => [],
          "Cibadak" => [],
          "Cibitung" => [],
          "Cicantayan" => [],
          "Cicurug" => [],
          "Cidadap" => [],
          "Cidahu" => [],
          "Cidolog" => [],
          "Ciemas" => [],
          "Cikakak" => [],
          "Cikembar" => [],
          "Cikidang" => [],
          "Cimanggu" => [],
          "Ciracap" => [],
          "Cireunghas" => [],
          "Cisaat" => [],
          "Cisolok" => [],
          "Curugkembar" => [],
          "Geger Bitung" => [],
          "Gunung Guruh" => [],
          "Jampang Kulon" => [],
          "Jampang Tengah" => [],
          "Kabandungan" => [],
          "Kadudampit" => [],
          "Kalapa Nunggal" => [],
          "Kali Bunder" => [],
          "Kebonpedes" => [],
          "Lengkong" => [],
          "Nagrak" => [],
          "Nyalindung" => [],
          "Pabuaran" => [],
          "Parakan Salak" => [],
          "Parung Kuda" => [],
          "Pelabuhan/Palabuhan Ratu" => [],
          "Purabaya" => [],
          "Sagaranten" => [],
          "Simpenan" => [],
          "Sukabumi" => [],
          "Sukalarang" => [],
          "Sukaraja" => [],
          "Surade" => [],
          "Tegal Buleud" => [],
          "Waluran" => [],
          "Warung Kiara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Sukabumi (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Baros" => [],
          "Cibeureum" => [],
          "Cikole" => [],
          "Citamiang" => [],
          "Gunung Puyuh" => [],
          "Lembursitu" => [],
          "Warudoyong" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Sumedang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Buahdua" => [],
          "Cibugel" => [],
          "Cimalaka" => [],
          "Cimanggung" => [],
          "Cisarua" => [],
          "Cisitu" => [],
          "Conggeang" => [],
          "Darmaraja" => [],
          "Ganeas" => [],
          "Jatigede" => [],
          "Jatinangor" => [],
          "Jatinunggal" => [],
          "Pamulihan" => [],
          "Paseh" => [],
          "Rancakalong" => [],
          "Situraja" => [],
          "Sukasari" => [],
          "Sumedang Selatan" => [],
          "Sumedang Utara" => [],
          "Surian" => [],
          "Tanjungkerta" => [],
          "Tanjungmedar" => [],
          "Tanjungsari" => [],
          "Tomo" => [],
          "Ujungjaya" => [],
          "Wado" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Tasikmalaya (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bantarkalong" => [],
          "Bojongasih" => [],
          "Bojonggambir" => [],
          "Ciawi" => [],
          "Cibalong" => [],
          "Cigalontang" => [],
          "Cikalong" => [],
          "Cikatomas" => [],
          "Cineam" => [],
          "Cipatujah" => [],
          "Cisayong" => [],
          "Culamega" => [],
          "Gunung Tanjung" => [],
          "Jamanis" => [],
          "Jatiwaras" => [],
          "Kadipaten" => [],
          "Karang Jaya" => [],
          "Karangnunggal" => [],
          "Leuwisari" => [],
          "Mangunreja" => [],
          "Manonjaya" => [],
          "Padakembang" => [],
          "Pagerageung" => [],
          "Pancatengah" => [],
          "Parungponteng" => [],
          "Puspahiang" => [],
          "Rajapolah" => [],
          "Salawu" => [],
          "Salopa" => [],
          "Sariwangi" => [],
          "Singaparna" => [],
          "Sodonghilir" => [],
          "Sukahening" => [],
          "Sukaraja" => [],
          "Sukarame" => [],
          "Sukaratu" => [],
          "Sukaresik" => [],
          "Tanjungjaya" => [],
          "Taraju" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Barat', 'Tasikmalaya (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bungursari" => [],
          "Cibeureum" => [],
          "Cihideung" => [],
          "Cipedes" => [],
          "Indihiang" => [],
          "Kawalu" => [],
          "Mangkubumi" => [],
          "Purbaratu" => [],
          "Tamansari" => [],
          "Tawang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Banjarnegara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banjarmangu" => [],
          "Banjarnegara" => [],
          "Batur" => [],
          "Bawang" => [],
          "Kalibening" => [],
          "Karangkobar" => [],
          "Madukara" => [],
          "Mandiraja" => [],
          "Pagedongan" => [],
          "Pagentan" => [],
          "Pandanarum" => [],
          "Pejawaran" => [],
          "Punggelan" => [],
          "Purwonegoro" => [],
          "Purworejo Klampok" => [],
          "Rakit" => [],
          "Sigaluh" => [],
          "Susukan" => [],
          "Wanadadi (Wonodadi)" => [],
          "Wanayasa" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Banyumas']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ajibarang" => [],
          "Banyumas" => [],
          "Baturaden" => [],
          "Cilongok" => [],
          "Gumelar" => [],
          "Jatilawang" => [],
          "Kalibagor" => [],
          "Karanglewas" => [],
          "Kebasen" => [],
          "Kedung Banteng" => [],
          "Kembaran" => [],
          "Kemranjen" => [],
          "Lumbir" => [],
          "Patikraja" => [],
          "Pekuncen" => [],
          "Purwojati" => [],
          "Purwokerto Barat" => [],
          "Purwokerto Selatan" => [],
          "Purwokerto Timur" => [],
          "Purwokerto Utara" => [],
          "Rawalo" => [],
          "Sokaraja" => [],
          "Somagede" => [],
          "Sumbang" => [],
          "Sumpiuh" => [],
          "Tambak" => [],
          "Wangon" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Batang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bandar" => [],
          "Banyuputih" => [],
          "Batang" => [],
          "Bawang" => [],
          "Blado" => [],
          "Gringsing" => [],
          "Kandeman" => [],
          "Limpung" => [],
          "Pecalungan" => [],
          "Reban" => [],
          "Subah" => [],
          "Tersono" => [],
          "Tulis" => [],
          "Warungasem" => [],
          "Wonotunggal" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Blora']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banjarejo" => [],
          "Blora kota" => [],
          "Bogorejo" => [],
          "Cepu" => [],
          "Japah" => [],
          "Jati" => [],
          "Jepon" => [],
          "Jiken" => [],
          "Kedungtuban" => [],
          "Kradenan" => [],
          "Kunduran" => [],
          "Ngawen" => [],
          "Randublatung" => [],
          "Sambong" => [],
          "Todanan" => [],
          "Tunjungan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Boyolali']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ampel" => [],
          "Andong" => [],
          "Banyudono" => [],
          "Boyolali" => [],
          "Cepogo" => [],
          "Juwangi" => [],
          "Karanggede" => [],
          "Kemusu" => [],
          "Klego" => [],
          "Mojosongo" => [],
          "Musuk" => [],
          "Ngemplak" => [],
          "Nogosari" => [],
          "Sambi" => [],
          "Sawit" => [],
          "Selo" => [],
          "Simo" => [],
          "Teras" => [],
          "Wonosegoro" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Brebes']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banjarharjo" => [],
          "Bantarkawung" => [],
          "Brebes" => [],
          "Bulakamba" => [],
          "Bumiayu" => [],
          "Jatibarang" => [],
          "Kersana" => [],
          "Ketanggungan" => [],
          "Larangan" => [],
          "Losari" => [],
          "Paguyangan" => [],
          "Salem" => [],
          "Sirampog" => [],
          "Songgom" => [],
          "Tanjung" => [],
          "Tonjong" => [],
          "Wanasari" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Cilacap']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Adipala" => [],
          "Bantarsari" => [],
          "Binangun" => [],
          "Cilacap Selatan" => [],
          "Cilacap Tengah" => [],
          "Cilacap Utara" => [],
          "Cimanggu" => [],
          "Cipari" => [],
          "Dayeuhluhur" => [],
          "Gandrungmangu" => [],
          "Jeruklegi" => [],
          "Kampung Laut" => [],
          "Karangpucung" => [],
          "Kawunganten" => [],
          "Kedungreja" => [],
          "Kesugihan" => [],
          "Kroya" => [],
          "Majenang" => [],
          "Maos" => [],
          "Nusawungu" => [],
          "Patimuan" => [],
          "Sampang" => [],
          "Sidareja" => [],
          "Wanareja" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Demak']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bonang" => [],
          "Demak" => [],
          "Dempet" => [],
          "Gajah" => [],
          "Guntur" => [],
          "Karang Tengah" => [],
          "Karanganyar" => [],
          "Karangawen" => [],
          "Kebonagung" => [],
          "Mijen" => [],
          "Mranggen" => [],
          "Sayung" => [],
          "Wedung" => [],
          "Wonosalam" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Grobogan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Brati" => [],
          "Gabus" => [],
          "Geyer" => [],
          "Godong" => [],
          "Grobogan" => [],
          "Gubug" => [],
          "Karangrayung" => [],
          "Kedungjati" => [],
          "Klambu" => [],
          "Kradenan" => [],
          "Ngaringan" => [],
          "Penawangan" => [],
          "Pulokulon" => [],
          "Purwodadi" => [],
          "Tanggungharjo" => [],
          "Tawangharjo" => [],
          "Tegowanu" => [],
          "Toroh" => [],
          "Wirosari" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Jepara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bangsri" => [],
          "Batealit" => [],
          "Donorojo" => [],
          "Jepara" => [],
          "Kalinyamatan" => [],
          "Karimunjawa" => [],
          "Kedung" => [],
          "Keling" => [],
          "Kembang" => [],
          "Mayong" => [],
          "Mlonggo" => [],
          "Nalumsari" => [],
          "Pakis Aji" => [],
          "Pecangaan" => [],
          "Tahunan" => [],
          "Welahan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Karanganyar']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Colomadu" => [],
          "Gondangrejo" => [],
          "Jaten" => [],
          "Jatipuro" => [],
          "Jatiyoso" => [],
          "Jenawi" => [],
          "Jumantono" => [],
          "Jumapolo" => [],
          "Karanganyar" => [],
          "Karangpandan" => [],
          "Kebakkramat" => [],
          "Kerjo" => [],
          "Matesih" => [],
          "Mojogedang" => [],
          "Ngargoyoso" => [],
          "Tasikmadu" => [],
          "Tawangmangu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Kebumen']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Adimulyo" => [],
          "Alian/Aliyan" => [],
          "Ambal" => [],
          "Ayah" => [],
          "Bonorowo" => [],
          "Buayan" => [],
          "Buluspesantren" => [],
          "Gombong" => [],
          "Karanganyar" => [],
          "Karanggayam" => [],
          "Karangsambung" => [],
          "Kebumen" => [],
          "Klirong" => [],
          "Kutowinangun" => [],
          "Kuwarasan" => [],
          "Mirit" => [],
          "Padureso" => [],
          "Pejagoan" => [],
          "Petanahan" => [],
          "Poncowarno" => [],
          "Prembun" => [],
          "Puring" => [],
          "Rowokele" => [],
          "Sadang" => [],
          "Sempor" => [],
          "Sruweng" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Kendal']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Boja" => [],
          "Brangsong" => [],
          "Cepiring" => [],
          "Gemuh" => [],
          "Kaliwungu" => [],
          "Kaliwungu Selatan" => [],
          "Kangkung" => [],
          "Kendal" => [],
          "Limbangan" => [],
          "Ngampel" => [],
          "Pagerruyung" => [],
          "Patean" => [],
          "Patebon" => [],
          "Pegandon" => [],
          "Plantungan" => [],
          "Ringinarum" => [],
          "Rowosari" => [],
          "Singorojo" => [],
          "Sukorejo" => [],
          "Weleri" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Klaten']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bayat" => [],
          "Cawas" => [],
          "Ceper" => [],
          "Delanggu" => [],
          "Gantiwarno" => [],
          "Jatinom" => [],
          "Jogonalan" => [],
          "Juwiring" => [],
          "Kalikotes" => [],
          "Karanganom" => [],
          "Karangdowo" => [],
          "Karangnongko" => [],
          "Kebonarum" => [],
          "Kemalang" => [],
          "Klaten Selatan" => [],
          "Klaten Tengah" => [],
          "Klaten Utara" => [],
          "Manisrenggo" => [],
          "Ngawen" => [],
          "Pedan" => [],
          "Polanharjo" => [],
          "Prambanan" => [],
          "Trucuk" => [],
          "Tulung" => [],
          "Wedi" => [],
          "Wonosari" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Kudus']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bae" => [],
          "Dawe" => [],
          "Gebog" => [],
          "Jati" => [],
          "Jekulo" => [],
          "Kaliwungu" => [],
          "Kudus Kota" => [],
          "Mejobo" => [],
          "Undaan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Magelang (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bandongan" => [],
          "Borobudur" => [],
          "Candimulyo" => [],
          "Dukun" => [],
          "Grabag" => [],
          "Kajoran" => [],
          "Kaliangkrik" => [],
          "Mertoyudan" => [],
          "Mungkid" => [],
          "Muntilan" => [],
          "Ngablak" => [],
          "Ngluwar" => [],
          "Pakis" => [],
          "Salam" => [],
          "Salaman" => [],
          "Sawangan" => [],
          "Secang" => [],
          "Srumbung" => [],
          "Tegalrejo" => [],
          "Tempuran" => [],
          "Windusari" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Magelang (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Magelang Selatan" => [],
          "Magelang Tengah" => [],
          "Magelang Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Pati']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batangan" => [],
          "Cluwak" => [],
          "Dukuhseti" => [],
          "Gabus" => [],
          "Gembong" => [],
          "Gunungwungkal" => [],
          "Jaken" => [],
          "Jakenan" => [],
          "Juwana" => [],
          "Kayen" => [],
          "Margorejo" => [],
          "Margoyoso" => [],
          "Pati" => [],
          "Pucakwangi" => [],
          "Sukolilo" => [],
          "Tambakromo" => [],
          "Tayu" => [],
          "Tlogowungu" => [],
          "Trangkil" => [],
          "Wedarijaksa" => [],
          "Winong" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Pekalongan (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bojong" => [],
          "Buaran" => [],
          "Doro" => [],
          "Kajen" => [],
          "Kandangserang" => [],
          "Karanganyar" => [],
          "Karangdadap" => [],
          "Kedungwuni" => [],
          "Kesesi" => [],
          "Lebakbarang" => [],
          "Paninggaran" => [],
          "Petungkriono/Petungkriyono" => [],
          "Siwalan" => [],
          "Sragi" => [],
          "Talun" => [],
          "Tirto" => [],
          "Wiradesa" => [],
          "Wonokerto" => [],
          "Wonopringgo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Pekalongan (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Pekalongan Barat" => [],
          "Pekalongan Selatan" => [],
          "Pekalongan Timur" => [],
          "Pekalongan Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Pemalang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ampelgading" => [],
          "Bantarbolang" => [],
          "Belik" => [],
          "Bodeh" => [],
          "Comal" => [],
          "Moga" => [],
          "Pemalang" => [],
          "Petarukan" => [],
          "Pulosari" => [],
          "Randudongkal" => [],
          "Taman" => [],
          "Ulujami" => [],
          "Warungpring" => [],
          "Watukumpul" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Purbalingga']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bobotsari" => [],
          "Bojongsari" => [],
          "Bukateja" => [],
          "Kaligondang" => [],
          "Kalimanah" => [],
          "Karanganyar" => [],
          "Karangjambu" => [],
          "Karangmoncol" => [],
          "Karangreja" => [],
          "Kejobong" => [],
          "Kemangkon" => [],
          "Kertanegara" => [],
          "Kutasari" => [],
          "Mrebet" => [],
          "Padamara" => [],
          "Pengadegan" => [],
          "Purbalingga" => [],
          "Rembang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Purworejo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bagelen" => [],
          "Banyuurip" => [],
          "Bayan" => [],
          "Bener" => [],
          "Bruno" => [],
          "Butuh" => [],
          "Gebang" => [],
          "Grabag" => [],
          "Kaligesing" => [],
          "Kemiri" => [],
          "Kutoarjo" => [],
          "Loano" => [],
          "Ngombol" => [],
          "Pituruh" => [],
          "Purwodadi" => [],
          "Purworejo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Rembang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bulu" => [],
          "Gunem" => [],
          "Kaliori" => [],
          "Kragan" => [],
          "Lasem" => [],
          "Pamotan" => [],
          "Pancur" => [],
          "Rembang" => [],
          "Sale" => [],
          "Sarang" => [],
          "Sedan" => [],
          "Sluke" => [],
          "Sulang" => [],
          "Sumber" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Salatiga']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Argomulyo" => [],
          "Sidomukti" => [],
          "Sidorejo" => [],
          "Tingkir" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Semarang (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ambarawa" => [],
          "Bancak" => [],
          "Bandungan" => [],
          "Banyubiru" => [],
          "Bawen" => [],
          "Bergas" => [],
          "Bringin" => [],
          "Getasan" => [],
          "Jambu" => [],
          "Kaliwungu" => [],
          "Pabelan" => [],
          "Pringapus" => [],
          "Sumowono" => [],
          "Suruh" => [],
          "Susukan" => [],
          "Tengaran" => [],
          "Tuntang" => [],
          "Ungaran Barat" => [],
          "Ungaran Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Semarang (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banyumanik" => [],
          "Candisari" => [],
          "Gajah Mungkur" => [],
          "Gayamsari" => [],
          "Genuk" => [],
          "Gunungpati" => [],
          "Mijen" => [],
          "Ngaliyan" => [],
          "Pedurungan" => [],
          "Semarang Barat" => [],
          "Semarang Selatan" => [],
          "Semarang Tengah" => [],
          "Semarang Timur" => [],
          "Semarang Utara" => [],
          "Tembalang" => [],
          "Tugu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Sragen']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Gemolong" => [],
          "Gesi" => [],
          "Gondang" => [],
          "Jenar" => [],
          "Kalijambe" => [],
          "Karangmalang" => [],
          "Kedawung" => [],
          "Masaran" => [],
          "Miri" => [],
          "Mondokan" => [],
          "Ngrampal" => [],
          "Plupuh" => [],
          "Sambirejo" => [],
          "Sambung Macan" => [],
          "Sidoharjo" => [],
          "Sragen" => [],
          "Sukodono" => [],
          "Sumberlawang" => [],
          "Tangen" => [],
          "Tanon" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Sukoharjo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Baki" => [],
          "Bendosari" => [],
          "Bulu" => [],
          "Gatak" => [],
          "Grogol" => [],
          "Kartasura" => [],
          "Mojolaban" => [],
          "Nguter" => [],
          "Polokarto" => [],
          "Sukoharjo" => [],
          "Tawangsari" => [],
          "Weru" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Surakarta (Solo)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banjarsari" => [],
          "Jebres" => [],
          "Laweyan" => [],
          "Pasar Kliwon" => [],
          "Serengan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Tegal (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Adiwerna" => [],
          "Balapulang" => [],
          "Bojong" => [],
          "Bumijawa" => [],
          "Dukuhturi" => [],
          "Dukuhwaru" => [],
          "Jatinegara" => [],
          "Kedung Banteng" => [],
          "Kramat" => [],
          "Lebaksiu" => [],
          "Margasari" => [],
          "Pagerbarang" => [],
          "Pangkah" => [],
          "Slawi" => [],
          "Surodadi" => [],
          "Talang" => [],
          "Tarub" => [],
          "Warurejo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Tegal (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Margadana" => [],
          "Tegal Barat" => [],
          "Tegal Selatan" => [],
          "Tegal Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Temanggung']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bansari" => [],
          "Bejen" => [],
          "Bulu" => [],
          "Candiroto" => [],
          "Gemawang" => [],
          "Jumo" => [],
          "Kaloran" => [],
          "Kandangan" => [],
          "Kedu" => [],
          "Kledung" => [],
          "Kranggan" => [],
          "Ngadirejo" => [],
          "Parakan" => [],
          "Pringsurat" => [],
          "Selopampang" => [],
          "Temanggung" => [],
          "Tembarak" => [],
          "Tlogomulyo" => [],
          "Tretep" => [],
          "Wonoboyo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Wonogiri']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Baturetno" => [],
          "Batuwarno" => [],
          "Bulukerto" => [],
          "Eromoko" => [],
          "Girimarto" => [],
          "Giritontro" => [],
          "Giriwoyo" => [],
          "Jatipurno" => [],
          "Jatiroto" => [],
          "Jatisrono" => [],
          "Karangtengah" => [],
          "Kismantoro" => [],
          "Manyaran" => [],
          "Ngadirojo" => [],
          "Nguntoronadi" => [],
          "Paranggupito" => [],
          "Pracimantoro" => [],
          "Puhpelem" => [],
          "Purwantoro" => [],
          "Selogiri" => [],
          "Sidoharjo" => [],
          "Slogohimo" => [],
          "Tirtomoyo" => [],
          "Wonogiri" => [],
          "Wuryantoro" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Tengah', 'Wonosobo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Garung" => [],
          "Kalibawang" => [],
          "Kalikajar" => [],
          "Kaliwiro" => [],
          "Kejajar" => [],
          "Kepil" => [],
          "Kertek" => [],
          "Leksono" => [],
          "Mojotengah" => [],
          "Sapuran" => [],
          "Selomerto" => [],
          "Sukoharjo" => [],
          "Wadaslintang" => [],
          "Watumalang" => [],
          "Wonosobo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Bangkalan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Arosbaya" => [],
          "Bangkalan" => [],
          "Blega" => [],
          "Burneh" => [],
          "Galis" => [],
          "Geger" => [],
          "Kamal" => [],
          "Klampis" => [],
          "Kokop" => [],
          "Konang" => [],
          "Kwanyar" => [],
          "Labang" => [],
          "Modung" => [],
          "Sepulu" => [],
          "Socah" => [],
          "Tanah Merah" => [],
          "Tanjungbumi" => [],
          "Tragah" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Banyuwangi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bangorejo" => [],
          "Banyuwangi" => [],
          "Cluring" => [],
          "Gambiran" => [],
          "Genteng" => [],
          "Giri" => [],
          "Glagah" => [],
          "Glenmore" => [],
          "Kabat" => [],
          "Kalibaru" => [],
          "Kalipuro" => [],
          "Licin" => [],
          "Muncar" => [],
          "Pesanggaran" => [],
          "Purwoharjo" => [],
          "Rogojampi" => [],
          "Sempu" => [],
          "Siliragung" => [],
          "Singojuruh" => [],
          "Songgon" => [],
          "Srono" => [],
          "Tegaldlimo" => [],
          "Tegalsari" => [],
          "Wongsorejo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Batu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batu" => [],
          "Bumiaji" => [],
          "Junrejo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Blitar (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bakung" => [],
          "Binangun" => [],
          "Doko" => [],
          "Gandusari" => [],
          "Garum" => [],
          "Kademangan" => [],
          "Kanigoro" => [],
          "Kesamben" => [],
          "Nglegok" => [],
          "Panggungrejo" => [],
          "Ponggok" => [],
          "Sanan Kulon" => [],
          "Selopuro" => [],
          "Selorejo" => [],
          "Srengat" => [],
          "Sutojayan" => [],
          "Talun" => [],
          "Udanawu" => [],
          "Wates" => [],
          "Wlingi" => [],
          "Wonodadi" => [],
          "Wonotirto" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Blitar (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kepanjen Kidul" => [],
          "Sanan Wetan" => [],
          "Sukorejo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Bojonegoro']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Balen" => [],
          "Baureno" => [],
          "Bojonegoro" => [],
          "Bubulan" => [],
          "Dander" => [],
          "Gayam" => [],
          "Gondang" => [],
          "Kalitidu" => [],
          "Kanor" => [],
          "Kapas" => [],
          "Kasiman" => [],
          "Kedewan" => [],
          "Kedungadem" => [],
          "Kepoh Baru" => [],
          "Malo" => [],
          "Margomulyo" => [],
          "Ngambon" => [],
          "Ngasem" => [],
          "Ngraho" => [],
          "Padangan" => [],
          "Purwosari" => [],
          "Sekar" => [],
          "Sugihwaras" => [],
          "Sukosewu" => [],
          "Sumberrejo" => [],
          "Tambakrejo" => [],
          "Temayang" => [],
          "Trucuk" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Bondowoso']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Binakal" => [],
          "Bondowoso" => [],
          "Botolinggo" => [],
          "Cermee" => [],
          "Curahdami" => [],
          "Grujugan" => [],
          "Jambe Sari Darus Sholah" => [],
          "Klabang" => [],
          "Maesan" => [],
          "Pakem" => [],
          "Prajekan" => [],
          "Pujer" => [],
          "Sempol" => [],
          "Sukosari" => [],
          "Sumber Wringin" => [],
          "Taman Krocok" => [],
          "Tamanan" => [],
          "Tapen" => [],
          "Tegalampel" => [],
          "Tenggarang" => [],
          "Tlogosari" => [],
          "Wonosari" => [],
          "Wringin" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Gresik']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Balong Panggang" => [],
          "Benjeng" => [],
          "Bungah" => [],
          "Cerme" => [],
          "Driyorejo" => [],
          "Duduk Sampeyan" => [],
          "Dukun" => [],
          "Gresik" => [],
          "Kebomas" => [],
          "Kedamean" => [],
          "Manyar" => [],
          "Menganti" => [],
          "Panceng" => [],
          "Sangkapura" => [],
          "Sidayu" => [],
          "Tambak" => [],
          "Ujung Pangkah" => [],
          "Wringin Anom" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Jember']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ajung" => [],
          "Ambulu" => [],
          "Arjasa" => [],
          "Balung" => [],
          "Bangsalsari" => [],
          "Gumuk Mas" => [],
          "Jelbuk" => [],
          "Jenggawah" => [],
          "Jombang" => [],
          "Kalisat" => [],
          "Kaliwates" => [],
          "Kencong" => [],
          "Ledokombo" => [],
          "Mayang" => [],
          "Mumbulsari" => [],
          "Pakusari" => [],
          "Panti" => [],
          "Patrang" => [],
          "Puger" => [],
          "Rambipuji" => [],
          "Semboro" => [],
          "Silo" => [],
          "Sukorambi" => [],
          "Sukowono" => [],
          "Sumber Baru" => [],
          "Sumber Jambe" => [],
          "Sumber Sari" => [],
          "Tanggul" => [],
          "Tempurejo" => [],
          "Umbulsari" => [],
          "Wuluhan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Jombang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bandar Kedung Mulyo" => [],
          "Bareng" => [],
          "Diwek" => [],
          "Gudo" => [],
          "Jogoroto" => [],
          "Jombang" => [],
          "Kabuh" => [],
          "Kesamben" => [],
          "Kudu" => [],
          "Megaluh" => [],
          "Mojoagung" => [],
          "Mojowarno" => [],
          "Ngoro" => [],
          "Ngusikan" => [],
          "Perak" => [],
          "Peterongan" => [],
          "Plandaan" => [],
          "Ploso" => [],
          "Sumobito" => [],
          "Tembelang" => [],
          "Wonosalam" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Kediri (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Badas" => [],
          "Banyakan" => [],
          "Gampengrejo" => [],
          "Grogol" => [],
          "Gurah" => [],
          "Kandangan" => [],
          "Kandat" => [],
          "Kayen Kidul" => [],
          "Kepung" => [],
          "Kras" => [],
          "Kunjang" => [],
          "Mojo" => [],
          "Ngadiluwih" => [],
          "Ngancar" => [],
          "Ngasem" => [],
          "Pagu" => [],
          "Papar" => [],
          "Pare" => [],
          "Plemahan" => [],
          "Plosoklaten" => [],
          "Puncu" => [],
          "Purwoasri" => [],
          "Ringinrejo" => [],
          "Semen" => [],
          "Tarokan" => [],
          "Wates" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Kediri (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kediri Kota" => [],
          "Mojoroto" => [],
          "Pesantren" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Lamongan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Babat" => [],
          "Bluluk" => [],
          "Brondong" => [],
          "Deket" => [],
          "Glagah" => [],
          "Kalitengah" => [],
          "Karang Geneng" => [],
          "Karangbinangun" => [],
          "Kedungpring" => [],
          "Kembangbahu" => [],
          "Lamongan" => [],
          "Laren" => [],
          "Maduran" => [],
          "Mantup" => [],
          "Modo" => [],
          "Ngimbang" => [],
          "Paciran" => [],
          "Pucuk" => [],
          "Sambeng" => [],
          "Sarirejo" => [],
          "Sekaran" => [],
          "Solokuro" => [],
          "Sugio" => [],
          "Sukodadi" => [],
          "Sukorame" => [],
          "Tikung" => [],
          "Turi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Lumajang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Candipuro" => [],
          "Gucialit" => [],
          "Jatiroto" => [],
          "Kedungjajang" => [],
          "Klakah" => [],
          "Kunir" => [],
          "Lumajang" => [],
          "Padang" => [],
          "Pasirian" => [],
          "Pasrujambe/Pasujambe" => [],
          "Pronojiwo" => [],
          "Randuagung" => [],
          "Ranuyoso" => [],
          "Rowokangkung" => [],
          "Senduro" => [],
          "Sukodono" => [],
          "Sumbersuko" => [],
          "Tekung" => [],
          "Tempeh" => [],
          "Tempursari" => [],
          "Yosowilangun" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Madiun (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Balerejo" => [],
          "Dagangan" => [],
          "Dolopo" => [],
          "Geger" => [],
          "Gemarang" => [],
          "Jiwan" => [],
          "Kare" => [],
          "Kebonsari" => [],
          "Madiun" => [],
          "Mejayan" => [],
          "Pilangkenceng" => [],
          "Saradan" => [],
          "Sawahan" => [],
          "Wonoasri" => [],
          "Wungu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Madiun (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kartoharjo" => [],
          "Manguharjo" => [],
          "Taman" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Magetan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Barat" => [],
          "Bendo" => [],
          "Karangrejo" => [],
          "Karas" => [],
          "Kartoharjo (Kertoharjo)" => [],
          "Kawedanan" => [],
          "Lembeyan" => [],
          "Magetan" => [],
          "Maospati" => [],
          "Ngariboyo" => [],
          "Nguntoronadi" => [],
          "Panekan" => [],
          "Parang" => [],
          "Plaosan" => [],
          "Poncol" => [],
          "Sidorejo" => [],
          "Sukomoro" => [],
          "Takeran" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Malang (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Blimbing" => [],
          "Kedungkandang" => [],
          "Klojen" => [],
          "Lowokwaru" => [],
          "Sukun" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Malang (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ampelgading" => [],
          "Bantur" => [],
          "Bululawang" => [],
          "Dampit" => [],
          "Dau" => [],
          "Donomulyo" => [],
          "Gedangan" => [],
          "Gondanglegi" => [],
          "Jabung" => [],
          "Kalipare" => [],
          "Karangploso" => [],
          "Kasembon" => [],
          "Kepanjen" => [],
          "Kromengan" => [],
          "Lawang" => [],
          "Ngajung (Ngajum)" => [],
          "Ngantang" => [],
          "Pagak" => [],
          "Pagelaran" => [],
          "Pakis" => [],
          "Pakisaji" => [],
          "Poncokusumo" => [],
          "Pujon" => [],
          "Singosari" => [],
          "Sumbermanjing Wetan" => [],
          "Sumberpucung" => [],
          "Tajinan" => [],
          "Tirtoyudo" => [],
          "Tumpang" => [],
          "Turen" => [],
          "Wagir" => [],
          "Wajak" => [],
          "Wonosari" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Mojokerto (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bangsal" => [],
          "Dawar Blandong" => [],
          "Dlanggu" => [],
          "Gedeg" => [],
          "Gondang" => [],
          "Jatirejo" => [],
          "Jetis" => [],
          "Kemlagi" => [],
          "Kutorejo" => [],
          "Mojoanyar" => [],
          "Mojosari" => [],
          "Ngoro" => [],
          "Pacet" => [],
          "Pungging" => [],
          "Puri" => [],
          "Sooko" => [],
          "Trawas" => [],
          "Trowulan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Mojokerto (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Magersari" => [],
          "Prajurit Kulon" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Nganjuk']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bagor" => [],
          "Baron" => [],
          "Berbek" => [],
          "Gondang" => [],
          "Jatikalen" => [],
          "Kertosono" => [],
          "Lengkong" => [],
          "Loceret" => [],
          "Nganjuk" => [],
          "Ngetos" => [],
          "Ngluyu" => [],
          "Ngronggot" => [],
          "Pace" => [],
          "Patianrowo" => [],
          "Prambon" => [],
          "Rejoso" => [],
          "Sawahan" => [],
          "Sukomoro" => [],
          "Tanjunganom" => [],
          "Wilangan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Ngawi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bringin" => [],
          "Geneng" => [],
          "Gerih" => [],
          "Jogorogo" => [],
          "Karanganyar" => [],
          "Karangjati" => [],
          "Kasreman" => [],
          "Kedunggalar" => [],
          "Kendal" => [],
          "Kwadungan" => [],
          "Mantingan" => [],
          "Ngawi" => [],
          "Ngrambe" => [],
          "Padas" => [],
          "Pangkur" => [],
          "Paron" => [],
          "Pitu" => [],
          "Sine" => [],
          "Widodaren" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Pacitan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Arjosari" => [],
          "Bandar" => [],
          "Donorojo" => [],
          "Kebon Agung" => [],
          "Nawangan" => [],
          "Ngadirojo" => [],
          "Pacitan" => [],
          "Pringkuku" => [],
          "Punung" => [],
          "Sudimoro" => [],
          "Tegalombo" => [],
          "Tulakan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Pamekasan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batumarmar" => [],
          "Galis" => [],
          "Kadur" => [],
          "Larangan" => [],
          "Pademawu" => [],
          "Pakong" => [],
          "Palenga'an" => [],
          "Pamekasan" => [],
          "Pasean" => [],
          "Pegantenan" => [],
          "Proppo" => [],
          "Tlanakan" => [],
          "Waru" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Pasuruan (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bangil" => [],
          "Beji" => [],
          "Gempol" => [],
          "Gondang Wetan" => [],
          "Grati" => [],
          "Kejayan" => [],
          "Kraton" => [],
          "Lekok" => [],
          "Lumbang" => [],
          "Nguling" => [],
          "Pandaan" => [],
          "Pasrepan" => [],
          "Pohjentrek" => [],
          "Prigen" => [],
          "Purwodadi" => [],
          "Purwosari" => [],
          "Puspo" => [],
          "Rejoso" => [],
          "Rembang" => [],
          "Sukorejo" => [],
          "Tosari" => [],
          "Tutur" => [],
          "Winongan" => [],
          "Wonorejo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Pasuruan (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bugul Kidul" => [],
          "Gadingrejo" => [],
          "Panggungrejo" => [],
          "Purworejo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Ponorogo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Babadan" => [],
          "Badegan" => [],
          "Balong" => [],
          "Bungkal" => [],
          "Jambon" => [],
          "Jenangan" => [],
          "Jetis" => [],
          "Kauman" => [],
          "Mlarak" => [],
          "Ngebel" => [],
          "Ngrayun" => [],
          "Ponorogo" => [],
          "Pudak" => [],
          "Pulung" => [],
          "Sambit" => [],
          "Sampung" => [],
          "Sawoo" => [],
          "Siman" => [],
          "Slahung" => [],
          "Sooko" => [],
          "Sukorejo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Probolinggo (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bantaran" => [],
          "Banyu Anyar" => [],
          "Besuk" => [],
          "Dringu" => [],
          "Gading" => [],
          "Gending" => [],
          "Kota Anyar" => [],
          "Kraksaan" => [],
          "Krejengan" => [],
          "Krucil" => [],
          "Kuripan" => [],
          "Leces" => [],
          "Lumbang" => [],
          "Maron" => [],
          "Paiton" => [],
          "Pajarakan" => [],
          "Pakuniran" => [],
          "Sukapura" => [],
          "Sumber" => [],
          "Sumberasih" => [],
          "Tegal Siwalan" => [],
          "Tiris" => [],
          "Tongas" => [],
          "Wonomerto" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Probolinggo (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kademangan" => [],
          "Kanigaran" => [],
          "Kedopok (Kedopak)" => [],
          "Mayangan" => [],
          "Wonoasih" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Sampang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banyuates" => [],
          "Camplong" => [],
          "Jrengik" => [],
          "Karang Penang" => [],
          "Kedungdung" => [],
          "Ketapang" => [],
          "Omben" => [],
          "Pangarengan" => [],
          "Robatal" => [],
          "Sampang" => [],
          "Sokobanah" => [],
          "Sreseh" => [],
          "Tambelangan" => [],
          "Torjun" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Sidoarjo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Balongbendo" => [],
          "Buduran" => [],
          "Candi" => [],
          "Gedangan" => [],
          "Jabon" => [],
          "Krembung" => [],
          "Krian" => [],
          "Porong" => [],
          "Prambon" => [],
          "Sedati" => [],
          "Sidoarjo" => [],
          "Sukodono" => [],
          "Taman" => [],
          "Tanggulangin" => [],
          "Tarik" => [],
          "Tulangan" => [],
          "Waru" => [],
          "Wonoayu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Situbondo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Arjasa" => [],
          "Asembagus" => [],
          "Banyuglugur" => [],
          "Banyuputih" => [],
          "Besuki" => [],
          "Bungatan" => [],
          "Jangkar" => [],
          "Jatibanteng" => [],
          "Kapongan" => [],
          "Kendit" => [],
          "Mangaran" => [],
          "Mlandingan" => [],
          "Panarukan" => [],
          "Panji" => [],
          "Situbondo" => [],
          "Suboh" => [],
          "Sumbermalang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Sumenep']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ambunten" => [],
          "Arjasa" => [],
          "Batang Batang" => [],
          "Batuan" => [],
          "Batuputih" => [],
          "Bluto" => [],
          "Dasuk" => [],
          "Dungkek" => [],
          "Ganding" => [],
          "Gapura" => [],
          "Gayam" => [],
          "Gili Ginting (Giligenteng)" => [],
          "Guluk Guluk" => [],
          "Kalianget" => [],
          "Kangayan" => [],
          "Kota Sumenep" => [],
          "Lenteng" => [],
          "Manding" => [],
          "Masalembu" => [],
          "Nonggunong" => [],
          "Pasongsongan" => [],
          "Pragaan" => [],
          "Ra'as (Raas)" => [],
          "Rubaru" => [],
          "Sapeken" => [],
          "Saronggi" => [],
          "Talango" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Surabaya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Asemrowo" => [],
          "Benowo" => [],
          "Bubutan" => [],
          "Bulak" => [],
          "Dukuh Pakis" => [],
          "Gayungan" => [],
          "Genteng" => [],
          "Gubeng" => [],
          "Gununganyar" => [],
          "Jambangan" => [],
          "Karangpilang" => [],
          "Kenjeran" => [],
          "Krembangan" => [],
          "Lakar Santri" => [],
          "Mulyorejo" => [],
          "Pabean Cantikan" => [],
          "Pakal" => [],
          "Rungkut" => [],
          "Sambikerep" => [],
          "Sawahan" => [],
          "Semampir" => [],
          "Simokerto" => [],
          "Sukolilo" => [],
          "Sukomanunggal" => [],
          "Tambaksari" => [],
          "Tandes" => [],
          "Tegalsari" => [],
          "Tenggilis Mejoyo" => [],
          "Wiyung" => [],
          "Wonocolo" => [],
          "Wonokromo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Trenggalek']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bendungan" => [],
          "Dongko" => [],
          "Durenan" => [],
          "Gandusari" => [],
          "Kampak" => [],
          "Karangan" => [],
          "Munjungan" => [],
          "Panggul" => [],
          "Pogalan" => [],
          "Pule" => [],
          "Suruh" => [],
          "Trenggalek" => [],
          "Tugu" => [],
          "Watulimo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Tuban']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bancar" => [],
          "Bangilan" => [],
          "Grabagan" => [],
          "Jatirogo" => [],
          "Jenu" => [],
          "Kenduruan" => [],
          "Kerek" => [],
          "Merakurak" => [],
          "Montong" => [],
          "Palang" => [],
          "Parengan" => [],
          "Plumpang" => [],
          "Rengel" => [],
          "Semanding" => [],
          "Senori" => [],
          "Singgahan" => [],
          "Soko" => [],
          "Tambakboyo" => [],
          "Tuban" => [],
          "Widang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Jawa Timur', 'Tulungagung']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bandung" => [],
          "Besuki" => [],
          "Boyolangu" => [],
          "Campur Darat" => [],
          "Gondang" => [],
          "Kalidawir" => [],
          "Karang Rejo" => [],
          "Kauman" => [],
          "Kedungwaru" => [],
          "Ngantru" => [],
          "Ngunut" => [],
          "Pagerwojo" => [],
          "Pakel" => [],
          "Pucanglaban" => [],
          "Rejotangan" => [],
          "Sendang" => [],
          "Sumbergempol" => [],
          "Tanggung Gunung" => [],
          "Tulungagung" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat', 'Bengkayang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bengkayang" => [],
          "Capkala" => [],
          "Jagoi Babang" => [],
          "Ledo" => [],
          "Lembah Bawang" => [],
          "Lumar" => [],
          "Monterado" => [],
          "Samalantan" => [],
          "Sanggau Ledo" => [],
          "Seluas" => [],
          "Siding" => [],
          "Sungai Betung" => [],
          "Sungai Raya" => [],
          "Sungai Raya Kepulauan" => [],
          "Suti Semarang" => [],
          "Teriak" => [],
          "Tujuh Belas" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat', 'Kapuas Hulu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Badau" => [],
          "Batang Lupar" => [],
          "Bika" => [],
          "Boyan Tanjung" => [],
          "Bunut Hilir" => [],
          "Bunut Hulu" => [],
          "Embaloh Hilir" => [],
          "Embaloh Hulu" => [],
          "Empanang" => [],
          "Hulu Gurung" => [],
          "Jongkong (Jengkong)" => [],
          "Kalis" => [],
          "Mentebah" => [],
          "Pengkadan (Batu Datu)" => [],
          "Puring Kencana" => [],
          "Putussibau Selatan" => [],
          "Putussibau Utara" => [],
          "Seberuang" => [],
          "Selimbau" => [],
          "Semitau" => [],
          "Silat Hilir" => [],
          "Silat Hulu" => [],
          "Suhaid" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat', 'Kayong Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kepulauan Karimata" => [],
          "Pulau Maya (Pulau Maya Karimata)" => [],
          "Seponti" => [],
          "Simpang Hilir" => [],
          "Sukadana" => [],
          "Teluk Batang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat', 'Ketapang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Air Upas" => [],
          "Benua Kayong" => [],
          "Delta Pawan" => [],
          "Hulu Sungai" => [],
          "Jelai Hulu" => [],
          "Kendawangan" => [],
          "Manis Mata" => [],
          "Marau" => [],
          "Matan Hilir Selatan" => [],
          "Matan Hilir Utara" => [],
          "Muara Pawan" => [],
          "Nanga Tayap" => [],
          "Pemahan" => [],
          "Sandai" => [],
          "Simpang Dua" => [],
          "Simpang Hulu" => [],
          "Singkup" => [],
          "Sungai Laur" => [],
          "Sungai Melayu Rayak" => [],
          "Tumbang Titi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat', 'Kubu Raya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batu Ampar" => [],
          "Kuala Mandor-B" => [],
          "Kubu" => [],
          "Rasau Jaya" => [],
          "Sei/Sungai Ambawang" => [],
          "Sei/Sungai Kakap" => [],
          "Sei/Sungai Raya" => [],
          "Teluk/Telok Pakedai" => [],
          "Terentang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat', 'Landak']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Air Besar" => [],
          "Banyuke Hulu" => [],
          "Jelimpo" => [],
          "Kuala Behe" => [],
          "Mandor" => [],
          "Mempawah Hulu" => [],
          "Menjalin" => [],
          "Menyuke" => [],
          "Meranti" => [],
          "Ngabang" => [],
          "Sebangki" => [],
          "Sengah Temila" => [],
          "Sompak" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat', 'Melawi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Belimbing" => [],
          "Belimbing Hulu" => [],
          "Ella Hilir" => [],
          "Menukung" => [],
          "Nanga Pinoh" => [],
          "Pinoh Selatan" => [],
          "Pinoh Utara" => [],
          "Sayan" => [],
          "Sokan" => [],
          "Tanah Pinoh" => [],
          "Tanah Pinoh Barat" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat', 'Pontianak (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Anjongan" => [],
          "Mempawah Hilir" => [],
          "Mempawah Timur" => [],
          "Sadaniang" => [],
          "Segedong" => [],
          "Sei/Sungai Kunyit" => [],
          "Sei/Sungai Pinyuh" => [],
          "Siantan" => [],
          "Toho" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat', 'Pontianak (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Pontianak Barat" => [],
          "Pontianak Kota" => [],
          "Pontianak Selatan" => [],
          "Pontianak Tenggara" => [],
          "Pontianak Timur" => [],
          "Pontianak Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat', 'Sambas']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Galing" => [],
          "Jawai" => [],
          "Jawai Selatan" => [],
          "Paloh" => [],
          "Pemangkat" => [],
          "Sajad" => [],
          "Sajingan Besar" => [],
          "Salatiga" => [],
          "Sambas" => [],
          "Sebawi" => [],
          "Sejangkung" => [],
          "Selakau" => [],
          "Selakau Timur" => [],
          "Semparuk" => [],
          "Subah" => [],
          "Tangaran" => [],
          "Tebas" => [],
          "Tekarang" => [],
          "Teluk Keramat" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat', 'Sanggau']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Balai" => [],
          "Beduai (Beduwai)" => [],
          "Bonti" => [],
          "Entikong" => [],
          "Jangkang" => [],
          "Kapuas (Sanggau Kapuas)" => [],
          "Kembayan" => [],
          "Meliau" => [],
          "Mukok" => [],
          "Noyan" => [],
          "Parindu" => [],
          "Sekayam" => [],
          "Tayan Hilir" => [],
          "Tayan Hulu" => [],
          "Toba" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat', 'Sekadau']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Belitang" => [],
          "Belitang Hilir" => [],
          "Belitang Hulu" => [],
          "Nanga Mahap" => [],
          "Nanga Taman" => [],
          "Sekadau Hilir" => [],
          "Sekadau Hulu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat', 'Singkawang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Singkawang Barat" => [],
          "Singkawang Selatan" => [],
          "Singkawang Tengah" => [],
          "Singkawang Timur" => [],
          "Singkawang Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Barat', 'Sintang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ambalau" => [],
          "Binjai Hulu" => [],
          "Dedai" => [],
          "Kayan Hilir" => [],
          "Kayan Hulu" => [],
          "Kelam Permai" => [],
          "Ketungau Hilir" => [],
          "Ketungau Hulu" => [],
          "Ketungau Tengah" => [],
          "Sepauk" => [],
          "Serawai (Nanga Serawai)" => [],
          "Sintang" => [],
          "Sungai Tebelian" => [],
          "Tempunak" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Selatan', 'Balangan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Awayan" => [],
          "Batu Mandi" => [],
          "Halong" => [],
          "Juai" => [],
          "Lampihong" => [],
          "Paringin" => [],
          "Paringin Selatan" => [],
          "Tebing Tinggi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Selatan', 'Banjar']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aluh-Aluh" => [],
          "Aranio" => [],
          "Astambul" => [],
          "Beruntung Baru" => [],
          "Gambut" => [],
          "Karang Intan" => [],
          "Kertak Hanyar" => [],
          "Martapura Barat" => [],
          "Martapura Kota" => [],
          "Martapura Timur" => [],
          "Mataraman" => [],
          "Pengaron" => [],
          "Peramasan" => [],
          "Sambung Makmur" => [],
          "Sei/Sungai Pinang" => [],
          "Sei/Sungai Tabuk" => [],
          "Simpang Empat" => [],
          "Tatah Makmur" => [],
          "Telaga Bauntung" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Selatan', 'Banjarbaru']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banjar Baru Selatan" => [],
          "Banjar Baru Utara" => [],
          "Cempaka" => [],
          "Landasan Ulin" => [],
          "Liang Anggang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Selatan', 'Banjarmasin']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banjarmasin Barat" => [],
          "Banjarmasin Selatan" => [],
          "Banjarmasin Tengah" => [],
          "Banjarmasin Timur" => [],
          "Banjarmasin Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Selatan', 'Barito Kuala']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Alalak" => [],
          "Anjir Muara" => [],
          "Anjir Pasar" => [],
          "Bakumpai" => [],
          "Barambai" => [],
          "Belawang" => [],
          "Cerbon" => [],
          "Jejangkit" => [],
          "Kuripan" => [],
          "Mandastana" => [],
          "Marabahan" => [],
          "Mekar Sari" => [],
          "Rantau Badauh" => [],
          "Tabukan" => [],
          "Tabunganen" => [],
          "Tamban" => [],
          "Wanaraya" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Selatan', 'Hulu Sungai Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Angkinang" => [],
          "Daha Barat" => [],
          "Daha Selatan" => [],
          "Daha Utara" => [],
          "Kalumpang (Kelumpang)" => [],
          "Kandangan" => [],
          "Loksado" => [],
          "Padang Batung" => [],
          "Simpur" => [],
          "Sungai Raya" => [],
          "Telaga Langsat" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Selatan', 'Hulu Sungai Tengah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Barabai" => [],
          "Batang Alai Selatan" => [],
          "Batang Alai Timur" => [],
          "Batang Alai Utara" => [],
          "Batu Benawa" => [],
          "Hantakan" => [],
          "Haruyan" => [],
          "Labuan Amas Selatan" => [],
          "Labuan Amas Utara" => [],
          "Limpasu" => [],
          "Pandawan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Selatan', 'Hulu Sungai Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Amuntai Selatan" => [],
          "Amuntai Tengah" => [],
          "Amuntai Utara" => [],
          "Babirik" => [],
          "Banjang" => [],
          "Danau Panggang" => [],
          "Haur Gading" => [],
          "Paminggir" => [],
          "Sungai Pandan" => [],
          "Sungai Tabukan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Selatan', 'Kotabaru']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Hampang" => [],
          "Kelumpang Barat" => [],
          "Kelumpang Hilir" => [],
          "Kelumpang Hulu" => [],
          "Kelumpang Selatan" => [],
          "Kelumpang Tengah" => [],
          "Kelumpang Utara" => [],
          "Pamukan Barat" => [],
          "Pamukan Selatan" => [],
          "Pamukan Utara" => [],
          "Pulau Laut Barat" => [],
          "Pulau Laut Kepulauan" => [],
          "Pulau Laut Selatan" => [],
          "Pulau Laut Tanjung Selayar" => [],
          "Pulau Laut Tengah" => [],
          "Pulau Laut Timur" => [],
          "Pulau Laut Utara" => [],
          "Pulau Sebuku" => [],
          "Pulau Sembilan" => [],
          "Sampanahan" => [],
          "Sungai Durian" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Selatan', 'Tabalong']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banua Lawas" => [],
          "Bintang Ara" => [],
          "Haruai" => [],
          "Jaro" => [],
          "Kelua (Klua)" => [],
          "Muara Harus" => [],
          "Muara Uya" => [],
          "Murung Pudak" => [],
          "Pugaan" => [],
          "Tanjung" => [],
          "Tanta" => [],
          "Upau" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Selatan', 'Tanah Bumbu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Angsana" => [],
          "Batulicin" => [],
          "Karang Bintang" => [],
          "Kuranji" => [],
          "Kusan Hilir" => [],
          "Kusan Hulu" => [],
          "Mantewe" => [],
          "Satui" => [],
          "Simpang Empat" => [],
          "Sungai Loban" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Selatan', 'Tanah Laut']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bajuin" => [],
          "Bati-Bati" => [],
          "Batu Ampar" => [],
          "Bumi Makmur" => [],
          "Jorong" => [],
          "Kintap" => [],
          "Kurau" => [],
          "Panyipatan" => [],
          "Pelaihari" => [],
          "Takisung" => [],
          "Tambang Ulang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Selatan', 'Tapin']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bakarangan" => [],
          "Binuang" => [],
          "Bungur" => [],
          "Candi Laras Selatan" => [],
          "Candi Laras Utara" => [],
          "Hatungun" => [],
          "Lokpaikat" => [],
          "Piani" => [],
          "Salam Babaris" => [],
          "Tapin Selatan" => [],
          "Tapin Tengah" => [],
          "Tapin Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah', 'Barito Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Dusun Hilir" => [],
          "Dusun Selatan" => [],
          "Dusun Utara" => [],
          "Gunung Bintang Awai" => [],
          "Jenamas" => [],
          "Karau Kuala" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah', 'Barito Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Awang" => [],
          "Benua Lima" => [],
          "Dusun Tengah" => [],
          "Dusun Timur" => [],
          "Karusen Janang" => [],
          "Paju Epat" => [],
          "Paku" => [],
          "Patangkep Tutui" => [],
          "Pematang Karau" => [],
          "Raren Batuah" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah', 'Barito Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Gunung Purei" => [],
          "Gunung Timang" => [],
          "Lahei" => [],
          "Lahei Barat" => [],
          "Montallat (Montalat)" => [],
          "Teweh Baru" => [],
          "Teweh Selatan" => [],
          "Teweh Tengah" => [],
          "Teweh Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah', 'Gunung Mas']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Damang Batu" => [],
          "Kahayan Hulu Utara" => [],
          "Kurun" => [],
          "Manuhing" => [],
          "Manuhing Raya" => [],
          "Mihing Raya" => [],
          "Miri Manasa" => [],
          "Rungan" => [],
          "Rungan Barat" => [],
          "Rungan Hulu" => [],
          "Sepang (Sepang Simin)" => [],
          "Tewah" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah', 'Kapuas']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Basarang" => [],
          "Bataguh" => [],
          "Dadahup" => [],
          "Kapuas Barat" => [],
          "Kapuas Hilir" => [],
          "Kapuas Hulu" => [],
          "Kapuas Kuala" => [],
          "Kapuas Murung" => [],
          "Kapuas Tengah" => [],
          "Kapuas Timur" => [],
          "Mandau Talawang" => [],
          "Mantangai" => [],
          "Pasak Talawang" => [],
          "Pulau Petak" => [],
          "Selat" => [],
          "Tamban Catur" => [],
          "Timpah" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah', 'Katingan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bukit Raya" => [],
          "Kamipang" => [],
          "Katingan Hilir" => [],
          "Katingan Hulu" => [],
          "Katingan Kuala" => [],
          "Katingan Tengah" => [],
          "Marikit" => [],
          "Mendawai" => [],
          "Petak Malai" => [],
          "Pulau Malan" => [],
          "Sanaman Mantikei (Senamang Mantikei)" => [],
          "Tasik Payawan" => [],
          "Tewang Sanggalang Garing (Sangalang)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah', 'Kotawaringin Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Arut Selatan" => [],
          "Arut Utara" => [],
          "Kotawaringin Lama" => [],
          "Kumai" => [],
          "Pangkalan Banteng" => [],
          "Pangkalan Lada" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah', 'Kotawaringin Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Antang Kalang" => [],
          "Baamang" => [],
          "Bukit Santuei" => [],
          "Cempaga" => [],
          "Cempaga Hulu" => [],
          "Kota Besi" => [],
          "Mentawa Baru (Ketapang)" => [],
          "Mentaya Hilir Selatan" => [],
          "Mentaya Hilir Utara" => [],
          "Mentaya Hulu" => [],
          "Parenggean" => [],
          "Pulau Hanaut" => [],
          "Seranau" => [],
          "Telaga Antang" => [],
          "Telawang" => [],
          "Teluk Sampit" => [],
          "Tualan Hulu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah', 'Lamandau']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batangkawa" => [],
          "Belantikan Raya" => [],
          "Bulik" => [],
          "Bulik Timur" => [],
          "Delang" => [],
          "Lamandau" => [],
          "Menthobi Raya" => [],
          "Sematu Jaya" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah', 'Murung Raya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Barito Tuhup Raya" => [],
          "Laung Tuhup" => [],
          "Murung" => [],
          "Permata Intan" => [],
          "Seribu Riam" => [],
          "Sumber Barito" => [],
          "Sungai Babuat" => [],
          "Tanah Siang" => [],
          "Tanah Siang Selatan" => [],
          "Uut Murung" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah', 'Palangka Raya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bukit Batu" => [],
          "Jekan Raya" => [],
          "Pahandut" => [],
          "Rakumpit" => [],
          "Sebangau" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah', 'Pulang Pisau']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banama Tingang" => [],
          "Jabiren Raya" => [],
          "Kahayan Hilir" => [],
          "Kahayan Kuala" => [],
          "Kahayan Tengah" => [],
          "Maliku" => [],
          "Pandih Batu" => [],
          "Sebangau Kuala" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah', 'Seruyan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batu Ampar" => [],
          "Danau Seluluk" => [],
          "Danau Sembuluh" => [],
          "Hanau" => [],
          "Seruyan Hilir" => [],
          "Seruyan Hilir Timur" => [],
          "Seruyan Hulu" => [],
          "Seruyan Raya" => [],
          "Seruyan Tengah" => [],
          "Suling Tambun" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Tengah', 'Sukamara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Balai Riam" => [],
          "Jelai" => [],
          "Pantai Lunci" => [],
          "Permata Kecubung" => [],
          "Sukamara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Timur', 'Balikpapan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Balikpapan Barat" => [],
          "Balikpapan Kota" => [],
          "Balikpapan Selatan" => [],
          "Balikpapan Tengah" => [],
          "Balikpapan Timur" => [],
          "Balikpapan Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Timur', 'Berau']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batu Putih" => [],
          "Biatan" => [],
          "Biduk-Biduk" => [],
          "Derawan (Pulau Derawan)" => [],
          "Gunung Tabur" => [],
          "Kelay" => [],
          "Maratua" => [],
          "Sambaliung" => [],
          "Segah" => [],
          "Tabalar" => [],
          "Talisayan" => [],
          "Tanjung Redeb" => [],
          "Teluk Bayur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Timur', 'Bontang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bontang Barat" => [],
          "Bontang Selatan" => [],
          "Bontang Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Timur', 'Kutai Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Barong Tongkok" => [],
          "Bentian Besar" => [],
          "Bongan" => [],
          "Damai" => [],
          "Jempang" => [],
          "Laham" => [],
          "Linggang Bigung" => [],
          "Long Apari" => [],
          "Long Bagun" => [],
          "Long Hubung" => [],
          "Long Iram" => [],
          "Long Pahangai" => [],
          "Manor Bulatin (Mook Manaar Bulatn)" => [],
          "Melak" => [],
          "Muara Lawa" => [],
          "Muara Pahu" => [],
          "Nyuatan" => [],
          "Penyinggahan" => [],
          "Sekolaq Darat" => [],
          "Siluq Ngurai" => [],
          "Tering" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Timur', 'Kutai Kartanegara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Anggana" => [],
          "Kembang Janggut" => [],
          "Kenohan" => [],
          "Kota Bangun" => [],
          "Loa Janan" => [],
          "Loa Kulu" => [],
          "Marang Kayu" => [],
          "Muara Badak" => [],
          "Muara Jawa" => [],
          "Muara Kaman" => [],
          "Muara Muntai" => [],
          "Muara Wis" => [],
          "Samboja (Semboja)" => [],
          "Sanga-Sanga" => [],
          "Sebulu" => [],
          "Tabang" => [],
          "Tenggarong" => [],
          "Tenggarong Seberang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Timur', 'Kutai Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batu Ampar" => [],
          "Bengalon" => [],
          "Busang" => [],
          "Kaliorang" => [],
          "Karangan" => [],
          "Kaubun" => [],
          "Kongbeng" => [],
          "Long Mesangat (Mesengat)" => [],
          "Muara Ancalong" => [],
          "Muara Bengkal" => [],
          "Muara Wahau" => [],
          "Rantau Pulung" => [],
          "Sandaran" => [],
          "Sangatta Selatan" => [],
          "Sangatta Utara" => [],
          "Sangkulirang" => [],
          "Telen" => [],
          "Teluk Pandan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Timur', 'Paser']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batu Engau" => [],
          "Batu Sopang" => [],
          "Kuaro" => [],
          "Long Ikis" => [],
          "Long Kali" => [],
          "Muara Komam" => [],
          "Muara Samu" => [],
          "Pasir Belengkong" => [],
          "Tanah Grogot" => [],
          "Tanjung Harapan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Timur', 'Penajam Paser Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Babulu" => [],
          "Penajam" => [],
          "Sepaku" => [],
          "Waru" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Timur', 'Samarinda']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Loa Janan Ilir" => [],
          "Palaran" => [],
          "Samarinda Ilir" => [],
          "Samarinda Kota" => [],
          "Samarinda Seberang" => [],
          "Samarinda Ulu" => [],
          "Samarinda Utara" => [],
          "Sambutan" => [],
          "Sungai Kunjang" => [],
          "Sungai Pinang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Utara', 'Bulungan (Bulongan)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Peso" => [],
          "Peso Hilir/Ilir" => [],
          "Pulau Bunyu" => [],
          "Sekatak" => [],
          "Tanjung Palas" => [],
          "Tanjung Palas Barat" => [],
          "Tanjung Palas Tengah" => [],
          "Tanjung Palas Timur" => [],
          "Tanjung Palas Utara" => [],
          "Tanjung Selor" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Utara', 'Malinau']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bahau Hulu" => [],
          "Kayan Hilir" => [],
          "Kayan Hulu" => [],
          "Kayan Selatan" => [],
          "Malinau Barat" => [],
          "Malinau Kota" => [],
          "Malinau Selatan" => [],
          "Malinau Selatan Hilir" => [],
          "Malinau Selatan Hulu" => [],
          "Malinau Utara" => [],
          "Mentarang" => [],
          "Mentarang Hulu" => [],
          "Pujungan" => [],
          "Sungai Boh" => [],
          "Sungai Tubu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Utara', 'Nunukan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Krayan" => [],
          "Krayan Selatan" => [],
          "Lumbis" => [],
          "Lumbis Ogong" => [],
          "Nunukan" => [],
          "Nunukan Selatan" => [],
          "Sebatik" => [],
          "Sebatik Barat" => [],
          "Sebatik Tengah" => [],
          "Sebatik Timur" => [],
          "Sebatik Utara" => [],
          "Sebuku" => [],
          "Sei Menggaris" => [],
          "Sembakung" => [],
          "Tulin Onsoi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Utara', 'Tana Tidung']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Betayau" => [],
          "Sesayap" => [],
          "Sesayap Hilir" => [],
          "Tana Lia (Tanah Lia)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kalimantan Utara', 'Tarakan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Tarakan Barat" => [],
          "Tarakan Tengah" => [],
          "Tarakan Timur" => [],
          "Tarakan Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kepulauan Riau', 'Batam']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batam Kota" => [],
          "Batu Aji" => [],
          "Batu Ampar" => [],
          "Belakang Padang" => [],
          "Bengkong" => [],
          "Bulang" => [],
          "Galang" => [],
          "Lubuk Baja" => [],
          "Nongsa" => [],
          "Sagulung" => [],
          "Sei/Sungai Beduk" => [],
          "Sekupang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kepulauan Riau', 'Bintan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bintan Pesisir" => [],
          "Bintan Timur" => [],
          "Bintan Utara" => [],
          "Gunung Kijang" => [],
          "Mantang" => [],
          "Seri/Sri Kuala Lobam" => [],
          "Tambelan" => [],
          "Teluk Bintan" => [],
          "Teluk Sebong" => [],
          "Toapaya" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kepulauan Riau', 'Karimun']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Belat" => [],
          "Buru" => [],
          "Durai" => [],
          "Karimun" => [],
          "Kundur" => [],
          "Kundur Barat" => [],
          "Kundur Utara" => [],
          "Meral" => [],
          "Meral Barat" => [],
          "Moro" => [],
          "Tebing" => [],
          "Ungar" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kepulauan Riau', 'Kepulauan Anambas']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Jemaja" => [],
          "Jemaja Timur" => [],
          "Palmatak" => [],
          "Siantan" => [],
          "Siantan Selatan" => [],
          "Siantan Tengah" => [],
          "Siantan Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kepulauan Riau', 'Lingga']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Lingga" => [],
          "Lingga Timur" => [],
          "Lingga Utara" => [],
          "Selayar" => [],
          "Senayang" => [],
          "Singkep" => [],
          "Singkep Barat" => [],
          "Singkep Pesisir" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kepulauan Riau', 'Natuna']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bunguran Barat" => [],
          "Bunguran Selatan" => [],
          "Bunguran Tengah" => [],
          "Bunguran Timur" => [],
          "Bunguran Timur Laut" => [],
          "Bunguran Utara" => [],
          "Midai" => [],
          "Pulau Laut" => [],
          "Pulau Tiga" => [],
          "Serasan" => [],
          "Serasan Timur" => [],
          "Subi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Kepulauan Riau', 'Tanjung Pinang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bukit Bestari" => [],
          "Tanjung Pinang Barat" => [],
          "Tanjung Pinang Kota" => [],
          "Tanjung Pinang Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Bandar Lampung']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bumi Waras" => [],
          "Enggal" => [],
          "Kedamaian" => [],
          "Kedaton" => [],
          "Kemiling" => [],
          "Labuhan Ratu" => [],
          "Langkapura" => [],
          "Panjang" => [],
          "Rajabasa" => [],
          "Sukabumi" => [],
          "Sukarame" => [],
          "Tanjung Karang Barat" => [],
          "Tanjung Karang Pusat" => [],
          "Tanjung Karang Timur" => [],
          "Tanjung Senang" => [],
          "Telukbetung Barat" => [],
          "Telukbetung Selatan" => [],
          "Telukbetung Timur" => [],
          "Telukbetung Utara" => [],
          "Way Halim" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Lampung Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Air Hitam" => [],
          "Balik Bukit" => [],
          "Bandar Negeri Suoh" => [],
          "Batu Brak" => [],
          "Batu Ketulis" => [],
          "Belalau" => [],
          "Gedung Surian" => [],
          "Kebun Tebu" => [],
          "Lumbok Seminung" => [],
          "Pagar Dewa" => [],
          "Sekincau" => [],
          "Sukau" => [],
          "Sumber Jaya" => [],
          "Suoh" => [],
          "Way Tenong" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Lampung Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bakauheni" => [],
          "Candipuro" => [],
          "Jati Agung" => [],
          "Kalianda" => [],
          "Katibung" => [],
          "Ketapang" => [],
          "Merbau Mataram" => [],
          "Natar" => [],
          "Palas" => [],
          "Penengahan" => [],
          "Rajabasa" => [],
          "Sidomulyo" => [],
          "Sragi" => [],
          "Tanjung Bintang" => [],
          "Tanjung Sari" => [],
          "Way Panji" => [],
          "Way Sulan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Lampung Tengah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Anak Ratu Aji" => [],
          "Anak Tuha" => [],
          "Bandar Mataram" => [],
          "Bandar Surabaya" => [],
          "Bangunrejo" => [],
          "Bekri" => [],
          "Bumi Nabung" => [],
          "Bumi Ratu Nuban" => [],
          "Gunung Sugih" => [],
          "Kalirejo" => [],
          "Kota Gajah" => [],
          "Padang Ratu" => [],
          "Pubian" => [],
          "Punggur" => [],
          "Putra Rumbia" => [],
          "Rumbia" => [],
          "Selagai Lingga" => [],
          "Sendang Agung" => [],
          "Seputih Agung" => [],
          "Seputih Banyak" => [],
          "Seputih Mataram" => [],
          "Seputih Raman" => [],
          "Seputih Surabaya" => [],
          "Terbanggi Besar" => [],
          "Terusan Nunyai" => [],
          "Trimurjo" => [],
          "Way Pangubuan (Pengubuan)" => [],
          "Way Seputih" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Lampung Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bandar Sribawono" => [],
          "Batanghari" => [],
          "Batanghari Nuban" => [],
          "Braja Slebah" => [],
          "Bumi Agung" => [],
          "Gunung Pelindung" => [],
          "Jabung" => [],
          "Labuhan Maringgai" => [],
          "Labuhan Ratu" => [],
          "Marga Sekampung" => [],
          "Margatiga" => [],
          "Mataram Baru" => [],
          "Melinting" => [],
          "Metro Kibang" => [],
          "Pasir Sakti" => [],
          "Pekalongan" => [],
          "Purbolinggo" => [],
          "Raman Utara" => [],
          "Sekampung" => [],
          "Sekampung Udik" => [],
          "Sukadana" => [],
          "Waway Karya" => [],
          "Way Bungur (Purbolinggo Utara)" => [],
          "Way Jepara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Lampung Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Abung Barat" => [],
          "Abung Kunang" => [],
          "Abung Pekurun" => [],
          "Abung Selatan" => [],
          "Abung Semuli" => [],
          "Abung Surakarta" => [],
          "Abung Tengah" => [],
          "Abung Timur" => [],
          "Abung Tinggi" => [],
          "Blambangan Pagar" => [],
          "Bukit Kemuning" => [],
          "Bunga Mayang" => [],
          "Hulu Sungkai" => [],
          "Kotabumi" => [],
          "Kotabumi Selatan" => [],
          "Kotabumi Utara" => [],
          "Muara Sungkai" => [],
          "Sungkai Barat" => [],
          "Sungkai Jaya" => [],
          "Sungkai Selatan" => [],
          "Sungkai Tengah" => [],
          "Sungkai Utara" => [],
          "Tanjung Raja" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Mesuji']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Mesuji" => [],
          "Mesuji Timur" => [],
          "Panca Jaya" => [],
          "Rawa Jitu Utara" => [],
          "Simpang Pematang" => [],
          "Tanjung Raya" => [],
          "Way Serdang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Metro']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Metro Barat" => [],
          "Metro Pusat" => [],
          "Metro Selatan" => [],
          "Metro Timur" => [],
          "Metro Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Pesawaran']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Gedong Tataan (Gedung Tataan)" => [],
          "Kedondong" => [],
          "Marga Punduh" => [],
          "Negeri Katon" => [],
          "Padang Cermin" => [],
          "Punduh Pidada (Pedada)" => [],
          "Tegineneng" => [],
          "Way Khilau" => [],
          "Way Lima" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Pesisir Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bengkunat" => [],
          "Bengkunat Belimbing" => [],
          "Karya Penggawa" => [],
          "Krui Selatan" => [],
          "Lemong" => [],
          "Ngambur" => [],
          "Pesisir Selatan" => [],
          "Pesisir Tengah" => [],
          "Pesisir Utara" => [],
          "Pulau Pisang" => [],
          "Way Krui" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Pringsewu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Adi Luwih" => [],
          "Ambarawa" => [],
          "Banyumas" => [],
          "Gading Rejo" => [],
          "Pagelaran" => [],
          "Pagelaran Utara" => [],
          "Pardasuka" => [],
          "Pringsewu" => [],
          "Sukoharjo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Tanggamus']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Air Naningan" => [],
          "Bandar Negeri Semuong" => [],
          "Bulok" => [],
          "Cukuh Balak" => [],
          "Gisting" => [],
          "Gunung Alip" => [],
          "Kelumbayan" => [],
          "Kelumbayan Barat" => [],
          "Kota Agung (Kota Agung Pusat)" => [],
          "Kota Agung Barat" => [],
          "Kota Agung Timur" => [],
          "Limau" => [],
          "Pematang Sawa" => [],
          "Pugung" => [],
          "Pulau Panggung" => [],
          "Semaka" => [],
          "Sumberejo" => [],
          "Talang Padang" => [],
          "Ulubelu" => [],
          "Wonosobo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Tulang Bawang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banjar Agung" => [],
          "Banjar Baru" => [],
          "Banjar Margo" => [],
          "Dente Teladas" => [],
          "Gedung Aji" => [],
          "Gedung Aji Baru" => [],
          "Gedung Meneng" => [],
          "Menggala" => [],
          "Menggala Timur" => [],
          "Meraksa Aji" => [],
          "Penawar Aji" => [],
          "Penawar Tama" => [],
          "Rawa Pitu" => [],
          "Rawajitu Selatan" => [],
          "Rawajitu Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Tulang Bawang Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Gunung Agung" => [],
          "Gunung Terang" => [],
          "Lambu Kibang" => [],
          "Pagar Dewa" => [],
          "Tulang Bawang Tengah" => [],
          "Tulang Bawang Udik" => [],
          "Tumijajar" => [],
          "Way Kenanga" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Lampung', 'Way Kanan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bahuga" => [],
          "Banjit" => [],
          "Baradatu" => [],
          "Blambangan Umpu" => [],
          "Buay Bahuga" => [],
          "Bumi Agung" => [],
          "Gunung Labuhan" => [],
          "Kasui" => [],
          "Negara Batin" => [],
          "Negeri Agung" => [],
          "Negeri Besar" => [],
          "Pakuan Ratu" => [],
          "Rebang Tangkas" => [],
          "Way Tuba" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku', 'Ambon']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Baguala" => [],
          "Leitimur Selatan" => [],
          "Nusaniwe (Nusanive)" => [],
          "Sirimau" => [],
          "Teluk Ambon" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku', 'Buru']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Airbuaya" => [],
          "Batabual" => [],
          "Fena Leisela" => [],
          "Lilialy" => [],
          "Lolong Guba" => [],
          "Namlea" => [],
          "Teluk Kaiely" => [],
          "Waeapo" => [],
          "Waelata" => [],
          "Waplau" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku', 'Buru Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ambalau" => [],
          "Fena Fafan" => [],
          "Kepala Madan" => [],
          "Leksula" => [],
          "Namrole" => [],
          "Waesama" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku', 'Kepulauan Aru']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aru Selatan" => [],
          "Aru Selatan Timur" => [],
          "Aru Selatan Utara" => [],
          "Aru Tengah" => [],
          "Aru Tengah Selatan" => [],
          "Aru Tengah Timur" => [],
          "Aru Utara" => [],
          "Aru Utara Timur Batuley" => [],
          "Pulau-Pulau Aru" => [],
          "Sir-Sir" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku', 'Maluku Barat Daya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Damer" => [],
          "Dawelor Dawera" => [],
          "Kepulauan Romang" => [],
          "Kisar Utara" => [],
          "Mdona Hyera/Hiera" => [],
          "Moa Lakor" => [],
          "Pulau Lakor" => [],
          "Pulau Letti (Leti Moa Lakor)" => [],
          "Pulau Masela" => [],
          "Pulau Pulau Babar" => [],
          "Pulau Pulau Terselatan" => [],
          "Pulau Wetang" => [],
          "Pulau-Pulau Babar Timur" => [],
          "Wetar" => [],
          "Wetar Barat" => [],
          "Wetar Timur" => [],
          "Wetar Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku', 'Maluku Tengah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Amahai" => [],
          "Banda" => [],
          "Leihitu" => [],
          "Leihitu Barat" => [],
          "Masohi Kota" => [],
          "Nusalaut" => [],
          "Pulau Haruku" => [],
          "Salahutu" => [],
          "Saparua" => [],
          "Saparua Timur" => [],
          "Seram Utara" => [],
          "Seram Utara Barat" => [],
          "Seram Utara Timur Kobi" => [],
          "Seram Utara Timur Seti" => [],
          "Tehoru" => [],
          "Teluk Elpaputih" => [],
          "Telutih" => [],
          "Teon Nila Serua" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku', 'Maluku Tenggara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Hoat Sorbay" => [],
          "Kei Besar" => [],
          "Kei Besar Selatan" => [],
          "Kei Besar Selatan Barat" => [],
          "Kei Besar Utara Barat" => [],
          "Kei Besar Utara Timur" => [],
          "Kei Kecil" => [],
          "Kei Kecil Barat" => [],
          "Kei Kecil Timur" => [],
          "Kei Kecil Timur Selatan" => [],
          "Manyeuw" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku', 'Maluku Tenggara Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kormomolin" => [],
          "Molu Maru" => [],
          "Nirunmas" => [],
          "Selaru" => [],
          "Tanimbar Selatan" => [],
          "Tanimbar Utara" => [],
          "Wermakatian (Wer Maktian)" => [],
          "Wertamrian" => [],
          "Wuarlabobar" => [],
          "Yaru" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku', 'Seram Bagian Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Amalatu" => [],
          "Elpaputih" => [],
          "Huamual" => [],
          "Huamual Belakang (Waisala)" => [],
          "Inamosol" => [],
          "Kairatu" => [],
          "Kairatu Barat" => [],
          "Kepulauan Manipa" => [],
          "Seram Barat" => [],
          "Taniwel" => [],
          "Taniwel Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku', 'Seram Bagian Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bula" => [],
          "Bula Barat" => [],
          "Gorom Timur" => [],
          "Kian Darat" => [],
          "Kilmury" => [],
          "Pulau Gorong (Gorom)" => [],
          "Pulau Panjang" => [],
          "Seram Timur" => [],
          "Siritaun Wida Timur" => [],
          "Siwalalat" => [],
          "Teluk Waru" => [],
          "Teor" => [],
          "Tutuk Tolu" => [],
          "Wakate" => [],
          "Werinama" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku', 'Tual']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kur Selatan" => [],
          "Pulau Dullah Selatan" => [],
          "Pulau Dullah Utara" => [],
          "Pulau Tayando Tam" => [],
          "Pulau-Pulau Kur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku Utara', 'Halmahera Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ibu" => [],
          "Ibu Selatan" => [],
          "Ibu Utara" => [],
          "Jailolo" => [],
          "Jailolo Selatan" => [],
          "Loloda" => [],
          "Sahu" => [],
          "Sahu Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku Utara', 'Halmahera Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bacan" => [],
          "Bacan Barat" => [],
          "Bacan Barat Utara" => [],
          "Bacan Selatan" => [],
          "Bacan Timur" => [],
          "Bacan Timur Selatan" => [],
          "Bacan Timur Tengah" => [],
          "Gane Barat" => [],
          "Gane Barat Selatan" => [],
          "Gane Barat Utara" => [],
          "Gane Timur" => [],
          "Gane Timur Selatan" => [],
          "Gane Timur Tengah" => [],
          "Kasiruta Barat" => [],
          "Kasiruta Timur" => [],
          "Kayoa" => [],
          "Kayoa Barat" => [],
          "Kayoa Selatan" => [],
          "Kayoa Utara" => [],
          "Kepulauan Botanglomang" => [],
          "Kepulauan Joronga" => [],
          "Makian (Pulau Makian)" => [],
          "Makian Barat (Pulau Makian)" => [],
          "Mandioli Selatan" => [],
          "Mandioli Utara" => [],
          "Obi" => [],
          "Obi Barat" => [],
          "Obi Selatan" => [],
          "Obi Timur" => [],
          "Obi Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku Utara', 'Halmahera Tengah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Patani" => [],
          "Patani Barat" => [],
          "Patani Utara" => [],
          "Pulau Gebe" => [],
          "Weda" => [],
          "Weda Selatan" => [],
          "Weda Tengah" => [],
          "Weda Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku Utara', 'Halmahera Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kota Maba" => [],
          "Maba" => [],
          "Maba Selatan" => [],
          "Maba Tengah" => [],
          "Maba Utara" => [],
          "Wasile" => [],
          "Wasile Selatan" => [],
          "Wasile Tengah" => [],
          "Wasile Timur" => [],
          "Wasile Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku Utara', 'Halmahera Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Galela" => [],
          "Galela Barat" => [],
          "Galela Selatan" => [],
          "Galela Utara" => [],
          "Kao" => [],
          "Kao Barat" => [],
          "Kao Teluk" => [],
          "Kao Utara" => [],
          "Loloda Kepulauan" => [],
          "Loloda Utara" => [],
          "Malifut" => [],
          "Tobelo" => [],
          "Tobelo Barat" => [],
          "Tobelo Selatan" => [],
          "Tobelo Tengah" => [],
          "Tobelo Timur" => [],
          "Tobelo Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku Utara', 'Kepulauan Sula']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Lede" => [],
          "Mangoli Barat" => [],
          "Mangoli Selatan" => [],
          "Mangoli Tengah" => [],
          "Mangoli Timur" => [],
          "Mangoli Utara" => [],
          "Mangoli Utara Timur" => [],
          "Sanana" => [],
          "Sanana Utara" => [],
          "Sulabesi Barat" => [],
          "Sulabesi Selatan" => [],
          "Sulabesi Tengah" => [],
          "Sulabesi Timur" => [],
          "Taliabu Barat" => [],
          "Taliabu Barat Laut" => [],
          "Taliabu Selatan" => [],
          "Taliabu Timur" => [],
          "Taliabu Timur Selatan" => [],
          "Taliabu Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku Utara', 'Pulau Morotai']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Morotai Jaya" => [],
          "Morotai Selatan" => [],
          "Morotai Selatan Barat" => [],
          "Morotai Timur" => [],
          "Morotai Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku Utara', 'Ternate']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Moti (Pulau Moti)" => [],
          "Pulau Batang Dua" => [],
          "Pulau Hiri" => [],
          "Pulau Ternate" => [],
          "Ternate Selatan (Kota)" => [],
          "Ternate Tengah (Kota)" => [],
          "Ternate Utara (Kota)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Maluku Utara', 'Tidore Kepulauan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Oba" => [],
          "Oba Selatan" => [],
          "Oba Tengah" => [],
          "Oba Utara" => [],
          "Tidore (Pulau Tidore)" => [],
          "Tidore Selatan" => [],
          "Tidore Timur (Pulau Tidore)" => [],
          "Tidore Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Aceh Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Arongan Lambalek" => [],
          "Bubon" => [],
          "Johan Pahlawan" => [],
          "Kaway XVI" => [],
          "Meureubo" => [],
          "Pante Ceureumen (Pantai Ceuremen)" => [],
          "Panton Reu" => [],
          "Samatiga" => [],
          "Sungai Mas" => [],
          "Woyla" => [],
          "Woyla Barat" => [],
          "Woyla Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Aceh Barat Daya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Babah Rot" => [],
          "Blang Pidie" => [],
          "Jeumpa" => [],
          "Kuala Batee" => [],
          "Lembah Sabil" => [],
          "Manggeng" => [],
          "Setia" => [],
          "Susoh" => [],
          "Tangan-Tangan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Aceh Besar']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Baitussalam" => [],
          "Blank Bintang" => [],
          "Darul Imarah" => [],
          "Darul Kamal" => [],
          "Darussalam" => [],
          "Indrapuri" => [],
          "Ingin Jaya" => [],
          "Kota Cot Glie (Kuta Cot Glie)" => [],
          "Kota Jantho" => [],
          "Kota Malaka (Kuta Malaka)" => [],
          "Krueng Barona Jaya" => [],
          "Kuta Baro" => [],
          "Lembah Seulawah" => [],
          "Leupung" => [],
          "Lhoknga (Lho'nga)" => [],
          "Lhoong" => [],
          "Mantasiek (Montasik)" => [],
          "Mesjid Raya" => [],
          "Peukan Bada" => [],
          "Pulo Aceh" => [],
          "Seulimeum" => [],
          "Simpang Tiga" => [],
          "Suka Makmur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Aceh Jaya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Darul Hikmah" => [],
          "Indra Jaya" => [],
          "Jaya" => [],
          "Keude Panga" => [],
          "Krueng Sabee" => [],
          "Pasie Raya" => [],
          "Sampoiniet" => [],
          "Setia Bakti" => [],
          "Teunom" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Aceh Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bakongan" => [],
          "Bakongan Timur" => [],
          "Kluet Selatan" => [],
          "Kluet Tengah" => [],
          "Kluet Timur" => [],
          "Kluet Utara" => [],
          "Kota Bahagia" => [],
          "Labuhan Haji" => [],
          "Labuhan Haji Barat" => [],
          "Labuhan Haji Timur" => [],
          "Meukek" => [],
          "Pasie Raja" => [],
          "Sama Dua" => [],
          "Sawang" => [],
          "Tapak Tuan" => [],
          "Trumon" => [],
          "Trumon Tengah" => [],
          "Trumon Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Aceh Singkil']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Danau Paris" => [],
          "Gunung Meriah (Mariah)" => [],
          "Kota Baharu" => [],
          "Kuala Baru" => [],
          "Pulau Banyak" => [],
          "Pulau Banyak Barat" => [],
          "Simpang Kanan" => [],
          "Singkil" => [],
          "Singkil Utara" => [],
          "Singkohor" => [],
          "Suro Makmur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Aceh Tamiang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banda Mulia" => [],
          "Bandar Pusaka" => [],
          "Bendahara" => [],
          "Karang Baru" => [],
          "Kejuruan Muda" => [],
          "Kota Kuala Simpang" => [],
          "Manyak Payed" => [],
          "Rantau" => [],
          "Sekerak" => [],
          "Seruway" => [],
          "Tamiang Hulu" => [],
          "Tenggulun" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Aceh Tengah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Atu Lintang" => [],
          "Bebesen" => [],
          "Bies" => [],
          "Bintang" => [],
          "Celala" => [],
          "Jagong Jeget" => [],
          "Kebayakan" => [],
          "Ketol" => [],
          "Kute Panang" => [],
          "Linge" => [],
          "Lut Tawar" => [],
          "Pegasing" => [],
          "Rusip Antara" => [],
          "Silih Nara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Aceh Tenggara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Babul Makmur" => [],
          "Babul Rahmah" => [],
          "Babussalam" => [],
          "Badar" => [],
          "Bambel" => [],
          "Bukit Tusam" => [],
          "Darul Hasanah" => [],
          "Deleng Pokhisen" => [],
          "Ketambe" => [],
          "Lawe Alas" => [],
          "Lawe Bulan" => [],
          "Lawe Sigala-Gala" => [],
          "Lawe Sumur" => [],
          "Leuser" => [],
          "Semadam" => [],
          "Tanah Alas" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Aceh Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banda Alam" => [],
          "Birem Bayeun" => [],
          "Darul Aman" => [],
          "Darul Falah" => [],
          "Darul Iksan (Ihsan)" => [],
          "Idi Rayeuk" => [],
          "Idi Timur" => [],
          "Idi Tunong" => [],
          "Indra Makmur" => [],
          "Julok" => [],
          "Madat" => [],
          "Nurussalam" => [],
          "Pante Bidari (Beudari)" => [],
          "Peudawa" => [],
          "Peunaron" => [],
          "Peureulak" => [],
          "Peureulak Barat" => [],
          "Peureulak Timur" => [],
          "Rantau Selamat" => [],
          "Ranto Peureulak" => [],
          "Serba Jadi" => [],
          "Simpang Jernih" => [],
          "Simpang Ulim" => [],
          "Sungai Raya" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Aceh Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Baktiya" => [],
          "Baktiya Barat" => [],
          "Banda Baro" => [],
          "Cot Girek" => [],
          "Dewantara" => [],
          "Geuredong Pase" => [],
          "Kuta Makmur" => [],
          "Langkahan" => [],
          "Lapang" => [],
          "Lhoksukon" => [],
          "Matangkuli" => [],
          "Meurah Mulia" => [],
          "Muara Batu" => [],
          "Nibong" => [],
          "Nisam" => [],
          "Nisam Antara" => [],
          "Paya Bakong" => [],
          "Pirak Timur" => [],
          "Samudera" => [],
          "Sawang" => [],
          "Seunuddon (Seunudon)" => [],
          "Simpang Kramat (Keramat)" => [],
          "Syamtalira Aron" => [],
          "Syamtalira Bayu" => [],
          "Tanah Jambo Aye" => [],
          "Tanah Luas" => [],
          "Tanah Pasir" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Banda Aceh']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Baiturrahman" => [],
          "Banda Raya" => [],
          "Jaya Baru" => [],
          "Kuta Alam" => [],
          "Kuta Raja" => [],
          "Lueng Bata" => [],
          "Meuraxa" => [],
          "Syiah Kuala" => [],
          "Ulee Kareng" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Bener Meriah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bandar" => [],
          "Bener Kelipah" => [],
          "Bukit" => [],
          "Gajah Putih" => [],
          "Mesidah" => [],
          "Permata" => [],
          "Pintu Rime Gayo" => [],
          "Syiah Utama" => [],
          "Timang Gajah" => [],
          "Wih Pesam" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Bireuen']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ganda Pura" => [],
          "Jangka" => [],
          "Jeumpa" => [],
          "Jeunieb" => [],
          "Juli" => [],
          "Kota Juang" => [],
          "Kuala" => [],
          "Kuta Blang" => [],
          "Makmur" => [],
          "Pandrah" => [],
          "Peudada" => [],
          "Peulimbang (Plimbang)" => [],
          "Peusangan" => [],
          "Peusangan Selatan" => [],
          "Peusangan Siblah Krueng" => [],
          "Samalanga" => [],
          "Simpang Mamplam" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Gayo Lues']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Blang Jerango" => [],
          "Blang Kejeren" => [],
          "Blang Pegayon" => [],
          "Dabun Gelang (Debun Gelang)" => [],
          "Kuta Panjang" => [],
          "Pantan Cuaca" => [],
          "Pining (Pinding)" => [],
          "Putri Betung" => [],
          "Rikit Gaib" => [],
          "Terangun (Terangon)" => [],
          "Teripe/Tripe Jaya" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Langsa']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Langsa Barat" => [],
          "Langsa Baro" => [],
          "Langsa Kota" => [],
          "Langsa Lama" => [],
          "Langsa Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Lhokseumawe']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banda Sakti" => [],
          "Blang Mangat" => [],
          "Muara Dua" => [],
          "Muara Satu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Nagan Raya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Beutong" => [],
          "Beutong Ateuh Banggalang" => [],
          "Darul Makmur" => [],
          "Kuala" => [],
          "Kuala Pesisir" => [],
          "Seunagan" => [],
          "Seunagan Timur" => [],
          "Suka Makmue" => [],
          "Tadu Raya" => [],
          "Tripa Makmur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Pidie']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batee" => [],
          "Delima" => [],
          "Geumpang" => [],
          "Glumpang Baro" => [],
          "Glumpang Tiga (Geulumpang Tiga)" => [],
          "Grong Grong" => [],
          "Indrajaya" => [],
          "Kembang Tanjong (Tanjung)" => [],
          "Keumala" => [],
          "Kota Sigli" => [],
          "Mane" => [],
          "Mila" => [],
          "Muara Tiga" => [],
          "Mutiara" => [],
          "Mutiara Timur" => [],
          "Padang Tiji" => [],
          "Peukan Baro" => [],
          "Pidie" => [],
          "Sakti" => [],
          "Simpang Tiga" => [],
          "Tangse" => [],
          "Tiro/Truseb" => [],
          "Titeue" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Pidie Jaya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bandar Baru" => [],
          "Bandar Dua" => [],
          "Jangka Buya" => [],
          "Meurah Dua" => [],
          "Meureudu" => [],
          "Panteraja" => [],
          "Trienggadeng" => [],
          "Ulim" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Sabang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Sukajaya" => [],
          "Sukakarya" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Simeulue']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Alapan (Alafan)" => [],
          "Salang" => [],
          "Simeuleu Barat" => [],
          "Simeuleu Tengah" => [],
          "Simeuleu Timur" => [],
          "Simeulue Cut" => [],
          "Teluk Dalam" => [],
          "Teupah Barat" => [],
          "Teupah Selatan" => [],
          "Teupah Tengah" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nanggroe Aceh Darussalam (NAD)', 'Subulussalam']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Longkib" => [],
          "Penanggalan" => [],
          "Rundeng" => [],
          "Simpang Kiri" => [],
          "Sultan Daulat" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Barat (NTB)', 'Bima (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ambalawi" => [],
          "Belo" => [],
          "Bolo" => [],
          "Donggo" => [],
          "Lambitu" => [],
          "Lambu" => [],
          "Langgudu" => [],
          "Madapangga" => [],
          "Monta" => [],
          "Palibelo" => [],
          "Parado" => [],
          "Sanggar" => [],
          "Sape" => [],
          "Soromandi" => [],
          "Tambora" => [],
          "Wawo" => [],
          "Wera" => [],
          "Woha" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Barat (NTB)', 'Bima (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Asakota" => [],
          "Mpunda" => [],
          "Raba" => [],
          "Rasanae Barat" => [],
          "Rasanae Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Barat (NTB)', 'Dompu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Dompu" => [],
          "Hu'u" => [],
          "Kempo" => [],
          "Kilo" => [],
          "Menggelewa (Manggelewa)" => [],
          "Pajo" => [],
          "Pekat" => [],
          "Woja" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Barat (NTB)', 'Lombok Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batu Layar" => [],
          "Gerung" => [],
          "Gunungsari" => [],
          "Kediri" => [],
          "Kuripan" => [],
          "Labuapi" => [],
          "Lembar" => [],
          "Lingsar" => [],
          "Narmada" => [],
          "Sekotong" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Barat (NTB)', 'Lombok Tengah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batukliang" => [],
          "Batukliang Utara" => [],
          "Janapria" => [],
          "Jonggat" => [],
          "Kopang" => [],
          "Praya" => [],
          "Praya Barat" => [],
          "Praya Barat Daya" => [],
          "Praya Tengah" => [],
          "Praya Timur" => [],
          "Pringgarata" => [],
          "Pujut" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Barat (NTB)', 'Lombok Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aikmel" => [],
          "Jerowaru" => [],
          "Keruak" => [],
          "Labuhan Haji" => [],
          "Masbagik" => [],
          "Montong Gading" => [],
          "Pringgabaya" => [],
          "Pringgasela" => [],
          "Sakra" => [],
          "Sakra Barat" => [],
          "Sakra Timur" => [],
          "Sambalia (Sambelia)" => [],
          "Selong" => [],
          "Sembalun" => [],
          "Sikur" => [],
          "Suela (Suwela)" => [],
          "Sukamulia" => [],
          "Suralaga" => [],
          "Terara" => [],
          "Wanasaba" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Barat (NTB)', 'Lombok Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bayan" => [],
          "Gangga" => [],
          "Kayangan" => [],
          "Pemenang" => [],
          "Tanjung" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Barat (NTB)', 'Mataram']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ampenan" => [],
          "Cakranegara" => [],
          "Mataram" => [],
          "Sandubaya (Sandujaya)" => [],
          "Sekarbela" => [],
          "Selaparang (Selaprang)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Barat (NTB)', 'Sumbawa']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Alas" => [],
          "Alas Barat" => [],
          "Batulanteh" => [],
          "Buer" => [],
          "Empang" => [],
          "Labangka" => [],
          "Labuhan Badas" => [],
          "Lantung" => [],
          "Lape (Lape Lopok)" => [],
          "Lenangguar" => [],
          "Lopok" => [],
          "Lunyuk" => [],
          "Maronge" => [],
          "Moyo Hilir" => [],
          "Moyo Hulu" => [],
          "Moyo Utara" => [],
          "Orong Telu" => [],
          "Plampang" => [],
          "Rhee" => [],
          "Ropang" => [],
          "Sumbawa" => [],
          "Tarano" => [],
          "Unter Iwes (Unterwiris)" => [],
          "Utan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Barat (NTB)', 'Sumbawa Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Brang Ene" => [],
          "Brang Rea" => [],
          "Jereweh" => [],
          "Maluk" => [],
          "Poto Tano" => [],
          "Sateluk (Seteluk)" => [],
          "Sekongkang" => [],
          "Taliwang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Alor']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Alor Barat Daya" => [],
          "Alor Barat Laut" => [],
          "Alor Selatan" => [],
          "Alor Tengah Utara" => [],
          "Alor Timur" => [],
          "Alor Timur Laut" => [],
          "Kabola" => [],
          "Lembur" => [],
          "Mataru" => [],
          "Pantar" => [],
          "Pantar Barat" => [],
          "Pantar Barat Laut" => [],
          "Pantar Tengah" => [],
          "Pantar Timur" => [],
          "Pulau Pura" => [],
          "Pureman" => [],
          "Teluk Mutiara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Belu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Atambua Barat" => [],
          "Atambua Kota" => [],
          "Atambua Selatan" => [],
          "Botin Leo Bele" => [],
          "Io Kufeu" => [],
          "Kakuluk Mesak" => [],
          "Kobalima" => [],
          "Kobalima Timur" => [],
          "Laen Manen" => [],
          "Lamaknen" => [],
          "Lamaknen Selatan" => [],
          "Lasiolat" => [],
          "Malaka Barat" => [],
          "Malaka Tengah" => [],
          "Malaka Timur" => [],
          "Nanaet Duabesi" => [],
          "Raihat" => [],
          "Raimanuk" => [],
          "Rinhat" => [],
          "Sasitamean" => [],
          "Tasifeto Barat" => [],
          "Tasifeto Timur" => [],
          "Weliman" => [],
          "Wewiku" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Ende']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Detukeli" => [],
          "Detusoko" => [],
          "Ende" => [],
          "Ende Selatan" => [],
          "Ende Tengah" => [],
          "Ende Timur" => [],
          "Ende Utara" => [],
          "Kelimutu" => [],
          "Kotabaru" => [],
          "Lepembusu Kelisoke" => [],
          "Lio Timur" => [],
          "Maukaro" => [],
          "Maurole" => [],
          "Nangapanda" => [],
          "Ndona" => [],
          "Ndona Timur" => [],
          "Ndori" => [],
          "Pulau Ende" => [],
          "Wewaria" => [],
          "Wolojita" => [],
          "Wolowaru" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Flores Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Adonara" => [],
          "Adonara Barat" => [],
          "Adonara Tengah" => [],
          "Adonara Timur" => [],
          "Demon Pagong" => [],
          "Ile Boleng" => [],
          "Ile Bura" => [],
          "Ile Mandiri" => [],
          "Kelubagolit (Klubagolit)" => [],
          "Larantuka" => [],
          "Lewolema" => [],
          "Solor Barat" => [],
          "Solor Selatan" => [],
          "Solor Timur" => [],
          "Tanjung Bunga" => [],
          "Titehena" => [],
          "Witihama (Watihama)" => [],
          "Wotan Ulumado" => [],
          "Wulanggitang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Kupang (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Amabi Oefeto" => [],
          "Amabi Oefeto Timur" => [],
          "Amarasi" => [],
          "Amarasi Barat" => [],
          "Amarasi Selatan" => [],
          "Amarasi Timur" => [],
          "Amfoang Barat Daya" => [],
          "Amfoang Barat Laut" => [],
          "Amfoang Selatan" => [],
          "Amfoang Tengah" => [],
          "Amfoang Timur" => [],
          "Amfoang Utara" => [],
          "Fatuleu" => [],
          "Fatuleu Barat" => [],
          "Fatuleu Tengah" => [],
          "Kupang Barat" => [],
          "Kupang Tengah" => [],
          "Kupang Timur" => [],
          "Nekamese" => [],
          "Semau" => [],
          "Semau Selatan" => [],
          "Sulamu" => [],
          "Taebenu" => [],
          "Takari" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Kupang (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Alak" => [],
          "Kelapa Lima" => [],
          "Kota Lama" => [],
          "Kota Raja" => [],
          "Maulafa" => [],
          "Oebobo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Lembata']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Atadei" => [],
          "Buyasuri (Buyasari)" => [],
          "Ile Ape" => [],
          "Ile Ape Timur" => [],
          "Lebatukan" => [],
          "Nagawutung" => [],
          "Nubatukan" => [],
          "Omesuri" => [],
          "Wulandoni (Wulandioni)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Manggarai']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Cibal" => [],
          "Cibal Barat" => [],
          "Langke Rembong" => [],
          "Lelak" => [],
          "Rahong Utara" => [],
          "Reok" => [],
          "Reok Barat" => [],
          "Ruteng" => [],
          "Satar Mese" => [],
          "Satar Mese Barat" => [],
          "Wae Rii" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Manggarai Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Boleng" => [],
          "Komodo" => [],
          "Kuwus" => [],
          "Lembor" => [],
          "Lembor Selatan" => [],
          "Macang Pacar" => [],
          "Mbeliling" => [],
          "Ndoso" => [],
          "Sano Nggoang" => [],
          "Welak" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Manggarai Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Borong" => [],
          "Elar" => [],
          "Elar Selatan" => [],
          "Kota Komba" => [],
          "Lamba Leda" => [],
          "Poco Ranaka" => [],
          "Poco Ranaka Timur" => [],
          "Rana Mese" => [],
          "Sambi Rampas" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Nagekeo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aesesa" => [],
          "Aesesa Selatan" => [],
          "Boawae" => [],
          "Keo Tengah" => [],
          "Mauponggo" => [],
          "Nangaroro" => [],
          "Wolowae" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Ngada']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aimere" => [],
          "Bajawa" => [],
          "Bajawa Utara" => [],
          "Golewa" => [],
          "Golewa Barat" => [],
          "Golewa Selatan" => [],
          "Inerie" => [],
          "Jerebuu" => [],
          "Riung" => [],
          "Riung Barat" => [],
          "Soa" => [],
          "Wolomeze (Riung Selatan)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Rote Ndao']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Landu Leko" => [],
          "Lobalain" => [],
          "Ndao Nuse" => [],
          "Pantai Baru" => [],
          "Rote Barat" => [],
          "Rote Barat Daya" => [],
          "Rote Barat Laut" => [],
          "Rote Selatan" => [],
          "Rote Tengah" => [],
          "Rote Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Sabu Raijua']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Hawu Mehara" => [],
          "Raijua" => [],
          "Sabu Barat" => [],
          "Sabu Liae" => [],
          "Sabu Tengah" => [],
          "Sabu Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Sikka']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Alok" => [],
          "Alok Barat" => [],
          "Alok Timur" => [],
          "Bola" => [],
          "Doreng" => [],
          "Hewokloang" => [],
          "Kangae" => [],
          "Kewapante" => [],
          "Koting" => [],
          "Lela" => [],
          "Magepanda" => [],
          "Mapitara" => [],
          "Mego" => [],
          "Nelle (Maumerei)" => [],
          "Nita" => [],
          "Paga" => [],
          "Palue" => [],
          "Talibura" => [],
          "Tana Wawo" => [],
          "Waiblama" => [],
          "Waigete" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Sumba Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kota Waikabubak" => [],
          "Lamboya" => [],
          "Lamboya Barat" => [],
          "Loli" => [],
          "Tana Righu" => [],
          "Wanokaka" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Sumba Barat Daya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kodi" => [],
          "Kodi Balaghar" => [],
          "Kodi Bangedo" => [],
          "Kodi Utara" => [],
          "Kota Tambolaka" => [],
          "Loura (Laura)" => [],
          "Wewewa Barat" => [],
          "Wewewa Selatan" => [],
          "Wewewa Tengah (Wewera Tengah)" => [],
          "Wewewa Timur" => [],
          "Wewewa Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Sumba Tengah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Katikutana" => [],
          "Katikutana Selatan" => [],
          "Mamboro" => [],
          "Umbu Ratu Nggay" => [],
          "Umbu Ratu Nggay Barat" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Sumba Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Haharu" => [],
          "Kahaunguweti (Kahaungu Eti)" => [],
          "Kambata Mapambuhang" => [],
          "Kambera" => [],
          "Kanatang" => [],
          "Karera" => [],
          "Katala Hamu Lingu" => [],
          "Kota Waingapu" => [],
          "Lewa" => [],
          "Lewa Tidahu" => [],
          "Mahu" => [],
          "Matawai Lappau (La Pawu)" => [],
          "Ngadu Ngala" => [],
          "Nggaha Oriangu" => [],
          "Paberiwai" => [],
          "Pahunga Lodu" => [],
          "Pandawai" => [],
          "Pinupahar (Pirapahar)" => [],
          "Rindi" => [],
          "Tabundung" => [],
          "Umalulu" => [],
          "Wula Waijelu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Timor Tengah Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Amanatun Selatan" => [],
          "Amanatun Utara" => [],
          "Amanuban Barat" => [],
          "Amanuban Selatan" => [],
          "Amanuban Tengah" => [],
          "Amanuban Timur" => [],
          "Batu Putih" => [],
          "Boking" => [],
          "Fatukopa" => [],
          "Fatumnasi" => [],
          "Fautmolo" => [],
          "Kie (Ki'e)" => [],
          "Kok Baun" => [],
          "Kolbano" => [],
          "Kot Olin" => [],
          "Kota Soe" => [],
          "Kualin" => [],
          "Kuanfatu" => [],
          "Kuatnana" => [],
          "Mollo Barat" => [],
          "Mollo Selatan" => [],
          "Mollo Tengah" => [],
          "Mollo Utara" => [],
          "Noebana" => [],
          "Noebeba" => [],
          "Nunbena" => [],
          "Nunkolo" => [],
          "Oenino" => [],
          "Polen" => [],
          "Santian" => [],
          "Tobu" => [],
          "Toianas" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Nusa Tenggara Timur (NTT)', 'Timor Tengah Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Biboki Anleu" => [],
          "Biboki Feotleu" => [],
          "Biboki Moenleu" => [],
          "Biboki Selatan" => [],
          "Biboki Tan Pah" => [],
          "Biboki Utara" => [],
          "Bikomi Nilulat" => [],
          "Bikomi Selatan" => [],
          "Bikomi Tengah" => [],
          "Bikomi Utara" => [],
          "Insana" => [],
          "Insana Barat" => [],
          "Insana Fafinesu" => [],
          "Insana Tengah" => [],
          "Insana Utara" => [],
          "Kota Kefamenanu" => [],
          "Miomaffo Barat" => [],
          "Miomaffo Tengah" => [],
          "Miomaffo Timur" => [],
          "Musi" => [],
          "Mutis" => [],
          "Naibenu" => [],
          "Noemuti" => [],
          "Noemuti Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Asmat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Agats" => [],
          "Akat" => [],
          "Atsy / Atsj" => [],
          "Ayip" => [],
          "Betcbamu" => [],
          "Der Koumur" => [],
          "Fayit" => [],
          "Jetsy" => [],
          "Joerat" => [],
          "Kolf Braza" => [],
          "Kopay" => [],
          "Pantai Kasuari" => [],
          "Pulau Tiga" => [],
          "Safan" => [],
          "Sawa Erma" => [],
          "Sirets" => [],
          "Suator" => [],
          "Suru-suru" => [],
          "Unir Sirau" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Biak Numfor']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aimando Padaido" => [],
          "Andey (Andei)" => [],
          "Biak Barat" => [],
          "Biak Kota" => [],
          "Biak Timur" => [],
          "Biak Utara" => [],
          "Bondifuar" => [],
          "Bruyadori" => [],
          "Numfor Barat" => [],
          "Numfor Timur" => [],
          "Oridek" => [],
          "Orkeri" => [],
          "Padaido" => [],
          "Poiru" => [],
          "Samofa" => [],
          "Swandiwe" => [],
          "Warsa" => [],
          "Yawosi" => [],
          "Yendidori" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Boven Digoel']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ambatkwi (Ambatkui)" => [],
          "Arimop" => [],
          "Bomakia" => [],
          "Firiwage" => [],
          "Fofi" => [],
          "Iniyandit" => [],
          "Jair" => [],
          "Kawagit" => [],
          "Ki" => [],
          "Kombay" => [],
          "Kombut" => [],
          "Kouh" => [],
          "Mandobo" => [],
          "Manggelum" => [],
          "Mindiptana" => [],
          "Ninati" => [],
          "Sesnuk" => [],
          "Subur" => [],
          "Waropko" => [],
          "Yaniruma" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Deiyai (Deliyai)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bowobado" => [],
          "Kapiraya" => [],
          "Tigi" => [],
          "Tigi Barat" => [],
          "Tigi Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Dogiyai']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Dogiyai" => [],
          "Kamu" => [],
          "Kamu Selatan" => [],
          "Kamu Timur" => [],
          "Kamu Utara (Ikrar/Ikrat)" => [],
          "Mapia" => [],
          "Mapia Barat" => [],
          "Mapia Tengah" => [],
          "Piyaiye (Sukikai)" => [],
          "Sukikai Selatan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Intan Jaya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Agisiga" => [],
          "Biandoga" => [],
          "Hitadipa" => [],
          "Homeo (Homeyo)" => [],
          "Sugapa" => [],
          "Wandai" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Jayapura (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Airu" => [],
          "Demta" => [],
          "Depapre" => [],
          "Ebungfau (Ebungfa)" => [],
          "Gresi Selatan" => [],
          "Kaureh" => [],
          "Kemtuk" => [],
          "Kemtuk Gresi" => [],
          "Nambluong" => [],
          "Nimbokrang" => [],
          "Nimboran" => [],
          "Ravenirara" => [],
          "Sentani" => [],
          "Sentani Barat" => [],
          "Sentani Timur" => [],
          "Unurum Guay" => [],
          "Waibu" => [],
          "Yapsi" => [],
          "Yokari" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Jayapura (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Abepura" => [],
          "Heram" => [],
          "Jayapura Selatan" => [],
          "Jayapura Utara" => [],
          "Muara Tami" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Jayawijaya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Asologaima (Asalogaima)" => [],
          "Asolokobal" => [],
          "Asotipo" => [],
          "Bolakme" => [],
          "Bpiri" => [],
          "Bugi" => [],
          "Hubikiak" => [],
          "Hubikosi (Hobikosi)" => [],
          "Ibele" => [],
          "Itlay Hisage" => [],
          "Koragi" => [],
          "Kurulu" => [],
          "Libarek" => [],
          "Maima" => [],
          "Molagalome" => [],
          "Muliama" => [],
          "Musatfak" => [],
          "Napua" => [],
          "Pelebaga" => [],
          "Piramid" => [],
          "Pisugi" => [],
          "Popugoba" => [],
          "Siepkosi" => [],
          "Silo Karno Doga" => [],
          "Taelarek" => [],
          "Tagime" => [],
          "Tagineri" => [],
          "Trikora" => [],
          "Usilimo" => [],
          "Wadangku" => [],
          "Walaik" => [],
          "Walelagama" => [],
          "Wame" => [],
          "Wamena" => [],
          "Welesi" => [],
          "Wesaput" => [],
          "Wita Waya" => [],
          "Wollo (Wolo)" => [],
          "Wouma" => [],
          "Yalengga" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Keerom']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Arso" => [],
          "Arso Timur" => [],
          "Senggi" => [],
          "Skamto (Skanto)" => [],
          "Towe" => [],
          "Waris" => [],
          "Web" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Kepulauan Yapen (Yapen Waropen)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Angkaisera" => [],
          "Kepulauan Ambai" => [],
          "Kosiwo" => [],
          "Poom" => [],
          "Pulau Kurudu" => [],
          "Pulau Yerui" => [],
          "Raimbawi" => [],
          "Teluk Ampimoi" => [],
          "Windesi" => [],
          "Wonawa" => [],
          "Yapen Barat" => [],
          "Yapen Selatan" => [],
          "Yapen Timur" => [],
          "Yapen Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Lanny Jaya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Balingga" => [],
          "Dimba" => [],
          "Gamelia" => [],
          "Kuyawage" => [],
          "Makki (Maki)" => [],
          "Malagaineri (Malagineri)" => [],
          "Pirime" => [],
          "Poga" => [],
          "Tiom" => [],
          "Tiomneri" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Mamberamo Raya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Benuki" => [],
          "Mamberamo Hilir/Ilir" => [],
          "Mamberamo Hulu/Ulu" => [],
          "Mamberamo Tengah" => [],
          "Mamberamo Tengah Timur" => [],
          "Rofaer (Rufaer)" => [],
          "Sawai" => [],
          "Waropen Atas" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Mamberamo Tengah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Eragayam" => [],
          "Ilugwa" => [],
          "Kelila" => [],
          "Kobakma" => [],
          "Megabilis (Megambilis)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Mappi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Assue" => [],
          "Bamgi" => [],
          "Citakmitak" => [],
          "Edera" => [],
          "Haju" => [],
          "Kaibar" => [],
          "Minyamur" => [],
          "Nambioman Bapai (Mambioman)" => [],
          "Obaa" => [],
          "Passue" => [],
          "Passue Bawah" => [],
          "Syahcame" => [],
          "Ti Zain" => [],
          "Venaha" => [],
          "Yakomi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Merauke']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Animha" => [],
          "Eligobel" => [],
          "Ilyawab" => [],
          "Jagebob" => [],
          "Kaptel" => [],
          "Kimaam" => [],
          "Kurik" => [],
          "Malind" => [],
          "Merauke" => [],
          "Muting" => [],
          "Naukenjerai" => [],
          "Ngguti (Nggunti)" => [],
          "Okaba" => [],
          "Semangga" => [],
          "Sota" => [],
          "Tabonji" => [],
          "Tanah Miring" => [],
          "Tubang" => [],
          "Ulilin" => [],
          "Waan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Mimika']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Agimuga" => [],
          "Jila" => [],
          "Jita" => [],
          "Kuala Kencana" => [],
          "Mimika Barat (Mibar)" => [],
          "Mimika Barat Jauh" => [],
          "Mimika Barat Tengah" => [],
          "Mimika Baru" => [],
          "Mimika Timur" => [],
          "Mimika Timur Jauh" => [],
          "Mimika Timur Tengah" => [],
          "Tembagapura" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Nabire']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Dipa" => [],
          "Makimi" => [],
          "Menou" => [],
          "Moora" => [],
          "Nabire" => [],
          "Nabire Barat" => [],
          "Napan" => [],
          "Siriwo" => [],
          "Teluk Kimi" => [],
          "Teluk Umar" => [],
          "Uwapa" => [],
          "Wanggar" => [],
          "Wapoga" => [],
          "Yaro (Yaro Kabisay)" => [],
          "Yaur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Nduga']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Alama" => [],
          "Dal" => [],
          "Embetpen" => [],
          "Gearek" => [],
          "Geselma (Geselema)" => [],
          "Inikgal" => [],
          "Iniye" => [],
          "Kegayem" => [],
          "Kenyam" => [],
          "Kilmid" => [],
          "Kora" => [],
          "Koroptak" => [],
          "Krepkuri" => [],
          "Mam" => [],
          "Mapenduma" => [],
          "Mbua (Mbuga)" => [],
          "Mbua Tengah" => [],
          "Mbulmu Yalma" => [],
          "Mebarok" => [],
          "Moba" => [],
          "Mugi" => [],
          "Nenggeagin" => [],
          "Nirkuri" => [],
          "Paro" => [],
          "Pasir Putih" => [],
          "Pija" => [],
          "Wosak" => [],
          "Wusi" => [],
          "Wutpaga" => [],
          "Yal" => [],
          "Yenggelo" => [],
          "Yigi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Paniai']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aradide" => [],
          "Bibida" => [],
          "Bogobaida" => [],
          "Dumadama" => [],
          "Ekadide" => [],
          "Kebo" => [],
          "Paniai Barat" => [],
          "Paniai Timur" => [],
          "Siriwo" => [],
          "Yatamo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Pegunungan Bintang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aboy" => [],
          "Alemsom" => [],
          "Awinbon" => [],
          "Batani" => [],
          "Batom" => [],
          "Bime" => [],
          "Borme" => [],
          "Eipumek" => [],
          "Iwur (Okiwur)" => [],
          "Jetfa" => [],
          "Kalomdol" => [],
          "Kawor" => [],
          "Kiwirok" => [],
          "Kiwirok Timur" => [],
          "Mofinop" => [],
          "Murkim" => [],
          "Nongme" => [],
          "Ok Aom" => [],
          "Okbab" => [],
          "Okbape" => [],
          "Okbemtau" => [],
          "Okbibab" => [],
          "Okhika" => [],
          "Oklip" => [],
          "Oksamol" => [],
          "Oksebang" => [],
          "Oksibil" => [],
          "Oksop" => [],
          "Pamek" => [],
          "Pepera" => [],
          "Serambakon" => [],
          "Tarup" => [],
          "Teiraplu" => [],
          "Weime" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Puncak']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Agadugume" => [],
          "Beoga" => [],
          "Doufu" => [],
          "Gome" => [],
          "Ilaga" => [],
          "Pogoma" => [],
          "Sinak" => [],
          "Wangbe" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Puncak Jaya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Fawi" => [],
          "Ilu" => [],
          "Jigonikme" => [],
          "Mewoluk (Mewulok)" => [],
          "Mulia" => [],
          "Tingginambut" => [],
          "Torere" => [],
          "Yamo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Sarmi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Apawer Hulu" => [],
          "Bonggo" => [],
          "Bonggo Timur" => [],
          "Pantai Barat" => [],
          "Pantai Timur" => [],
          "Pantai Timur Barat" => [],
          "Sarmi" => [],
          "Sarmi Selatan" => [],
          "Sarmi Timur" => [],
          "Tor Atas" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Supiori']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kepulauan Aruri" => [],
          "Supiori Barat" => [],
          "Supiori Selatan" => [],
          "Supiori Timur" => [],
          "Supiori Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Tolikara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Airgaram" => [],
          "Anawi" => [],
          "Aweku" => [],
          "Bewani" => [],
          "Biuk" => [],
          "Bogonuk" => [],
          "Bokondini" => [],
          "Bokoneri" => [],
          "Danime" => [],
          "Dow" => [],
          "Dundu (Ndundu)" => [],
          "Egiam" => [],
          "Geya" => [],
          "Gika" => [],
          "Gilubandu (Gilumbandu/Gilimbandu)" => [],
          "Goyage" => [],
          "Gundagi (Gudage)" => [],
          "Kai" => [],
          "Kamboneri" => [],
          "Kanggime (Kanggima )" => [],
          "Karubaga" => [],
          "Kembu" => [],
          "Kondaga (Konda)" => [],
          "Kuari" => [],
          "Kubu" => [],
          "Li Anogomma" => [],
          "Nabunage" => [],
          "Nelawi" => [],
          "Numba" => [],
          "Nunggawi (Munggawi)" => [],
          "Panaga" => [],
          "Poganeri" => [],
          "Tagime" => [],
          "Tagineri" => [],
          "Telenggeme" => [],
          "Timori" => [],
          "Umagi" => [],
          "Wakuwo" => [],
          "Wari (Taiyeve)" => [],
          "Wenam" => [],
          "Wina" => [],
          "Wonoki (Woniki)" => [],
          "Wugi" => [],
          "Wunin (Wumin)" => [],
          "Yuko" => [],
          "Yuneri" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Waropen']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Demba Masirei" => [],
          "Inggerus" => [],
          "Kirihi" => [],
          "Masirei" => [],
          "Oudate Waropen" => [],
          "Risei Sayati" => [],
          "Ureifasei" => [],
          "Wapoga Inggerus" => [],
          "Waropen Bawah" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Yahukimo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Amuma" => [],
          "Anggruk" => [],
          "Bomela" => [],
          "Dekai" => [],
          "Dirwemna (Diruwena)" => [],
          "Duram" => [],
          "Endomen" => [],
          "Hereapini (Hereanini)" => [],
          "Hilipuk" => [],
          "Hogio (Hugio)" => [],
          "Holuon" => [],
          "Kabianggama (Kabianggema)" => [],
          "Kayo" => [],
          "Kona" => [],
          "Koropun (Korupun)" => [],
          "Kosarek" => [],
          "Kurima" => [],
          "Kwelemdua (Kwelamdua)" => [],
          "Kwikma" => [],
          "Langda" => [],
          "Lolat" => [],
          "Mugi" => [],
          "Musaik" => [],
          "Nalca" => [],
          "Ninia" => [],
          "Nipsan" => [],
          "Obio" => [],
          "Panggema" => [],
          "Pasema" => [],
          "Pronggoli (Proggoli)" => [],
          "Puldama" => [],
          "Samenage" => [],
          "Sela" => [],
          "Seredela (Seredala)" => [],
          "Silimo" => [],
          "Soba" => [],
          "Sobaham" => [],
          "Soloikma" => [],
          "Sumo" => [],
          "Suntamon" => [],
          "Suru Suru" => [],
          "Talambo" => [],
          "Tangma" => [],
          "Ubahak" => [],
          "Ubalihi" => [],
          "Ukha" => [],
          "Walma" => [],
          "Werima" => [],
          "Wusuma" => [],
          "Yahuliambut" => [],
          "Yogosem" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua', 'Yalimo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Abenaho" => [],
          "Apalapsili" => [],
          "Benawa" => [],
          "Elelim" => [],
          "Welarek" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua Barat', 'Fakfak']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bombarai (Bomberay)" => [],
          "Fakfak" => [],
          "Fakfak Barat" => [],
          "Fakfak Tengah" => [],
          "Fakfak Timur" => [],
          "Karas" => [],
          "Kokas" => [],
          "Kramongmongga (Kramamongga)" => [],
          "Teluk Patipi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua Barat', 'Kaimana']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Buruway" => [],
          "Kaimana" => [],
          "Kambraw (Kamberau)" => [],
          "Teluk Arguni Atas" => [],
          "Teluk Arguni Bawah (Yerusi)" => [],
          "Teluk Etna" => [],
          "Yamor" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua Barat', 'Manokwari']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Manokwari Barat" => [],
          "Manokwari Selatan" => [],
          "Manokwari Timur" => [],
          "Manokwari Utara" => [],
          "Masni" => [],
          "Prafi" => [],
          "Sidey" => [],
          "Tanah Rubuh" => [],
          "Warmare" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua Barat', 'Manokwari Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Dataran Isim" => [],
          "Momi Waren" => [],
          "Neney (Nenei)" => [],
          "Oransbari" => [],
          "Ransiki" => [],
          "Tahota (Tohota)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua Barat', 'Maybrat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aifat" => [],
          "Aifat Selatan" => [],
          "Aifat Timur" => [],
          "Aifat Timur Jauh" => [],
          "Aifat Timur Selatan" => [],
          "Aifat Timur Tengah" => [],
          "Aifat Utara" => [],
          "Aitinyo" => [],
          "Aitinyo Barat" => [],
          "Aitinyo Raya" => [],
          "Aitinyo Tengah" => [],
          "Aitinyo Utara" => [],
          "Ayamaru" => [],
          "Ayamaru Barat" => [],
          "Ayamaru Jaya" => [],
          "Ayamaru Selatan" => [],
          "Ayamaru Selatan Jaya" => [],
          "Ayamaru Tengah" => [],
          "Ayamaru Timur" => [],
          "Ayamaru Timur Selatan" => [],
          "Ayamaru Utara" => [],
          "Ayamaru Utara Timur" => [],
          "Mare" => [],
          "Mare Selatan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua Barat', 'Pegunungan Arfak']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Anggi" => [],
          "Anggi Gida" => [],
          "Catubouw" => [],
          "Didohu" => [],
          "Hingk" => [],
          "Membey" => [],
          "Menyambouw (Minyambouw)" => [],
          "Sururey" => [],
          "Taige" => [],
          "Testega" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua Barat', 'Raja Ampat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ayau" => [],
          "Batanta Selatan" => [],
          "Batanta Utara" => [],
          "Kepulauan Ayau" => [],
          "Kepulauan Sembilan" => [],
          "Kofiau" => [],
          "Kota Waisai" => [],
          "Meos Mansar" => [],
          "Misool (Misool Utara)" => [],
          "Misool Barat" => [],
          "Misool Selatan" => [],
          "Misool Timur" => [],
          "Salawati Barat" => [],
          "Salawati Tengah" => [],
          "Salawati Utara (Samate)" => [],
          "Supnin" => [],
          "Teluk Mayalibit" => [],
          "Tiplol Mayalibit" => [],
          "Waigeo Barat" => [],
          "Waigeo Barat Kepulauan" => [],
          "Waigeo Selatan" => [],
          "Waigeo Timur" => [],
          "Waigeo Utara" => [],
          "Warwabomi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua Barat', 'Sorong (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aimas" => [],
          "Beraur" => [],
          "Klabot" => [],
          "Klamono" => [],
          "Klaso" => [],
          "Klawak" => [],
          "Klayili" => [],
          "Makbon" => [],
          "Mariat" => [],
          "Maudus" => [],
          "Mayamuk" => [],
          "Moisegen" => [],
          "Salawati" => [],
          "Salawati Selatan" => [],
          "Sayosa" => [],
          "Seget" => [],
          "Segun" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua Barat', 'Sorong (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Sorong" => [],
          "Sorong Barat" => [],
          "Sorong Kepulauan" => [],
          "Sorong Manoi" => [],
          "Sorong Timur" => [],
          "Sorong Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua Barat', 'Sorong Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Fokour" => [],
          "Inanwatan (Inawatan)" => [],
          "Kais (Matemani Kais)" => [],
          "Kokoda" => [],
          "Kokoda Utara" => [],
          "Konda" => [],
          "Matemani" => [],
          "Moswaren" => [],
          "Saifi" => [],
          "Sawiat" => [],
          "Seremuk" => [],
          "Teminabuan" => [],
          "Wayer" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua Barat', 'Tambrauw']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Abun" => [],
          "Amberbaken" => [],
          "Fef (Peef)" => [],
          "Kebar" => [],
          "Kwoor" => [],
          "Miyah (Meyah)" => [],
          "Moraid" => [],
          "Mubrani" => [],
          "Sausapor" => [],
          "Senopi" => [],
          "Syujak" => [],
          "Yembun" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua Barat', 'Teluk Bintuni']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aranday" => [],
          "Aroba" => [],
          "Babo" => [],
          "Bintuni" => [],
          "Biscoop" => [],
          "Dataran Beimes" => [],
          "Fafurwar (Irorutu)" => [],
          "Kaitaro" => [],
          "Kamundan" => [],
          "Kuri" => [],
          "Manimeri" => [],
          "Masyeta" => [],
          "Mayado" => [],
          "Merdey" => [],
          "Moskona Barat" => [],
          "Moskona Selatan" => [],
          "Moskona Timur" => [],
          "Moskona Utara" => [],
          "Sumuri (Simuri)" => [],
          "Tembuni" => [],
          "Tomu" => [],
          "Tuhiba" => [],
          "Wamesa (Idoor)" => [],
          "Weriagar" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Papua Barat', 'Teluk Wondama']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kuri Wamesa" => [],
          "Naikere (Wasior Barat)" => [],
          "Nikiwar" => [],
          "Rasiei" => [],
          "Roon" => [],
          "Roswar" => [],
          "Rumberpon" => [],
          "Soug Jaya" => [],
          "Teluk Duairi (Wasior Utara)" => [],
          "Wamesa" => [],
          "Wasior" => [],
          "Windesi" => [],
          "Wondiboy (Wasior Selatan)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Riau', 'Bengkalis']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bantan" => [],
          "Bengkalis" => [],
          "Bukit Batu" => [],
          "Mandau" => [],
          "Pinggir" => [],
          "Rupat" => [],
          "Rupat Utara" => [],
          "Siak Kecil" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Riau', 'Dumai']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bukit Kapur" => [],
          "Dumai Barat" => [],
          "Dumai Kota" => [],
          "Dumai Selatan" => [],
          "Dumai Timur" => [],
          "Medang Kampai" => [],
          "Sungai Sembilan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Riau', 'Indragiri Hilir']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batang Tuaka" => [],
          "Concong" => [],
          "Enok" => [],
          "Gaung" => [],
          "Gaung Anak Serka" => [],
          "Kateman" => [],
          "Kempas" => [],
          "Kemuning" => [],
          "Keritang" => [],
          "Kuala Indragiri" => [],
          "Mandah" => [],
          "Pelangiran" => [],
          "Pulau Burung" => [],
          "Reteh" => [],
          "Sungai Batang" => [],
          "Tanah Merah" => [],
          "Teluk Belengkong" => [],
          "Tembilahan" => [],
          "Tembilahan Hulu" => [],
          "Tempuling" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Riau', 'Indragiri Hulu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batang Cenaku" => [],
          "Batang Gansal" => [],
          "Batang Peranap" => [],
          "Kelayang" => [],
          "Kuala Cenaku" => [],
          "Lirik" => [],
          "Lubuk Batu Jaya" => [],
          "Pasir Penyu" => [],
          "Peranap" => [],
          "Rakit Kulim" => [],
          "Rengat" => [],
          "Rengat Barat" => [],
          "Seberida" => [],
          "Sungai Lala" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Riau', 'Kampar']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bangkinang" => [],
          "Bangkinang Seberang" => [],
          "Gunung Sahilan" => [],
          "Kampar" => [],
          "Kampar Kiri" => [],
          "Kampar Kiri Hilir" => [],
          "Kampar Kiri Hulu" => [],
          "Kampar Kiri Tengah" => [],
          "Kampar Timur" => [],
          "Kampar Utara" => [],
          "Koto Kampar Hulu" => [],
          "Kuok (Bangkinang Barat)" => [],
          "Perhentian Raja" => [],
          "Rumbio Jaya" => [],
          "Salo" => [],
          "Siak Hulu" => [],
          "Tambang" => [],
          "Tapung" => [],
          "Tapung Hilir" => [],
          "Tapung Hulu" => [],
          "XIII Koto Kampar" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Riau', 'Kepulauan Meranti']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Merbau" => [],
          "Pulaumerbau" => [],
          "Rangsang" => [],
          "Rangsang Barat" => [],
          "Rangsang Pesisir" => [],
          "Tasik Putri Puyu" => [],
          "Tebing Tinggi" => [],
          "Tebing Tinggi Barat" => [],
          "Tebing Tinggi Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Riau', 'Kuantan Singingi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Benai" => [],
          "Cerenti" => [],
          "Gunung Toar" => [],
          "Hulu Kuantan" => [],
          "Inuman" => [],
          "Kuantan Hilir" => [],
          "Kuantan Hilir Seberang" => [],
          "Kuantan Mudik" => [],
          "Kuantan Tengah" => [],
          "Logas Tanah Darat" => [],
          "Pangean" => [],
          "Pucuk Rantau" => [],
          "Sentajo Raya" => [],
          "Singingi" => [],
          "Singingi Hilir" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Riau', 'Pekanbaru']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bukit Raya" => [],
          "Lima Puluh" => [],
          "Marpoyan Damai" => [],
          "Payung Sekaki" => [],
          "Pekanbaru Kota" => [],
          "Rumbai" => [],
          "Rumbai Pesisir" => [],
          "Sail" => [],
          "Senapelan" => [],
          "Sukajadi" => [],
          "Tampan" => [],
          "Tenayan Raya" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Riau', 'Pelalawan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bandar Petalangan" => [],
          "Bandar Sei Kijang" => [],
          "Bunut" => [],
          "Kerumutan" => [],
          "Kuala Kampar" => [],
          "Langgam" => [],
          "Pangkalan Kerinci" => [],
          "Pangkalan Kuras" => [],
          "Pangkalan Lesung" => [],
          "Pelalawan" => [],
          "Teluk Meranti" => [],
          "Ukui" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Riau', 'Rokan Hilir']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bagan Sinembah" => [],
          "Bangko" => [],
          "Bangko Pusaka (Pusako)" => [],
          "Batu Hampar" => [],
          "Kubu" => [],
          "Kubu Babussalam" => [],
          "Pasir Limau Kapas" => [],
          "Pekaitan" => [],
          "Pujud" => [],
          "Rantau Kopar" => [],
          "Rimba Melintang" => [],
          "Simpang Kanan" => [],
          "Sinaboi (Senaboi)" => [],
          "Tanah Putih" => [],
          "Tanah Putih Tanjung Melawan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Riau', 'Rokan Hulu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bangun Purba" => [],
          "Bonai Darussalam" => [],
          "Kabun" => [],
          "Kepenuhan" => [],
          "Kepenuhan Hulu" => [],
          "Kunto Darussalam" => [],
          "Pagaran Tapah Darussalam" => [],
          "Pendalian IV Koto" => [],
          "Rambah" => [],
          "Rambah Hilir" => [],
          "Rambah Samo" => [],
          "Rokan IV Koto" => [],
          "Tambusai" => [],
          "Tambusai Utara" => [],
          "Tandun" => [],
          "Ujung Batu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Riau', 'Siak']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bunga Raya" => [],
          "Dayun" => [],
          "Kandis" => [],
          "Kerinci Kanan" => [],
          "Koto Gasib" => [],
          "Lubuk Dalam" => [],
          "Mempura" => [],
          "Minas" => [],
          "Pusako" => [],
          "Sabak Auh" => [],
          "Siak" => [],
          "Sungai Apit" => [],
          "Sungai Mandau" => [],
          "Tualang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Barat', 'Majene']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banggae" => [],
          "Banggae Timur" => [],
          "Malunda" => [],
          "Pamboang" => [],
          "Sendana" => [],
          "Tammeredo Sendana" => [],
          "Tubo (Tubo Sendana)" => [],
          "Ulumunda" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Barat', 'Mamasa']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aralle (Arrale)" => [],
          "Balla" => [],
          "Bambang" => [],
          "Buntumalangka" => [],
          "Mamasa" => [],
          "Mambi" => [],
          "Mehalaan" => [],
          "Messawa" => [],
          "Nosu" => [],
          "Pana" => [],
          "Rantebulahan Timur" => [],
          "Sesena Padang" => [],
          "Sumarorong" => [],
          "Tabang" => [],
          "Tabulahan" => [],
          "Tanduk Kalua" => [],
          "Tawalian" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Barat', 'Mamuju']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bonehau" => [],
          "Budong-Budong" => [],
          "Kalukku" => [],
          "Kalumpang" => [],
          "Karossa" => [],
          "Kep. Bala Balakang" => [],
          "Mamuju" => [],
          "Pangale" => [],
          "Papalang" => [],
          "Sampaga" => [],
          "Simboro dan Kepulauan" => [],
          "Tapalang" => [],
          "Tapalang Barat" => [],
          "Tobadak" => [],
          "Tommo" => [],
          "Topoyo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Barat', 'Mamuju Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bambaira" => [],
          "Bambalamotu" => [],
          "Baras" => [],
          "Bulu Taba" => [],
          "Dapurang" => [],
          "Duripoku" => [],
          "Lariang" => [],
          "Pasangkayu" => [],
          "Pedongga" => [],
          "Sarjo" => [],
          "Sarudu" => [],
          "Tikke Raya" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Barat', 'Polewali Mandar']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Alu (Allu)" => [],
          "Anreapi" => [],
          "Balanipa" => [],
          "Binuang" => [],
          "Bulo" => [],
          "Campalagian" => [],
          "Limboro" => [],
          "Luyo" => [],
          "Mapilli" => [],
          "Matakali" => [],
          "Matangnga" => [],
          "Polewali" => [],
          "Tapango" => [],
          "Tinambung" => [],
          "Tubbi Taramanu (Tutar/Tutallu)" => [],
          "Wonomulyo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Bantaeng']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bantaeng" => [],
          "Bissappu" => [],
          "Eremerasa" => [],
          "Gantarang Keke (Gantareng Keke)" => [],
          "Pajukukang" => [],
          "Sinoa" => [],
          "Tompobulu" => [],
          "Uluere" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Barru']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Balusu" => [],
          "Barru" => [],
          "Mallusetasi" => [],
          "Pujananting" => [],
          "Soppeng Riaja" => [],
          "Tanete Riaja" => [],
          "Tanete Rilau" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Bone']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ajangale" => [],
          "Amali" => [],
          "Awangpone" => [],
          "Barebbo" => [],
          "Bengo" => [],
          "Bontocani" => [],
          "Cenrana" => [],
          "Cina" => [],
          "Dua Boccoe" => [],
          "Kahu" => [],
          "Kajuara" => [],
          "Lamuru" => [],
          "Lappariaja" => [],
          "Libureng" => [],
          "Mare" => [],
          "Palakka" => [],
          "Patimpeng" => [],
          "Ponre" => [],
          "Salomekko" => [],
          "Sibulue" => [],
          "Tanete Riattang" => [],
          "Tanete Riattang Barat" => [],
          "Tanete Riattang Timur" => [],
          "Tellu Limpoe" => [],
          "Tellu Siattinge" => [],
          "Tonra" => [],
          "Ulaweng" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Bulukumba']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bonto Bahari" => [],
          "Bontotiro" => [],
          "Bulukumba (Bulukumpa)" => [],
          "Gantorang/Gantarang (Gangking)" => [],
          "Hero Lange-Lange (Herlang)" => [],
          "Kajang" => [],
          "Kindang" => [],
          "Rilau Ale" => [],
          "Ujung Bulu" => [],
          "Ujung Loe" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Enrekang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Alla" => [],
          "Anggeraja" => [],
          "Baraka" => [],
          "Baroko" => [],
          "Bungin" => [],
          "Buntu Batu" => [],
          "Cendana" => [],
          "Curio" => [],
          "Enrekang" => [],
          "Maiwa" => [],
          "Malua" => [],
          "Masalle" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Gowa']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bajeng" => [],
          "Bajeng Barat" => [],
          "Barombong" => [],
          "Biringbulu" => [],
          "Bontolempangang" => [],
          "Bontomarannu" => [],
          "Bontonompo" => [],
          "Bontonompo Selatan" => [],
          "Bungaya" => [],
          "Manuju" => [],
          "Pallangga" => [],
          "Parangloe" => [],
          "Parigi" => [],
          "Pattallassang" => [],
          "Somba Opu (Upu)" => [],
          "Tinggimoncong" => [],
          "Tombolo Pao" => [],
          "Tompobulu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Jeneponto']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Arungkeke" => [],
          "Bangkala" => [],
          "Bangkala Barat" => [],
          "Batang" => [],
          "Binamu" => [],
          "Bontoramba" => [],
          "Kelara" => [],
          "Rumbia" => [],
          "Tamalatea" => [],
          "Tarowang" => [],
          "Turatea" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Luwu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bajo" => [],
          "Bajo Barat" => [],
          "Basse Sangtempe Utara" => [],
          "Bassesang Tempe (Bastem)" => [],
          "Belopa" => [],
          "Belopa Utara" => [],
          "Bua" => [],
          "Bua Ponrang (Bupon)" => [],
          "Kamanre" => [],
          "Lamasi" => [],
          "Lamasi Timur" => [],
          "Larompong" => [],
          "Larompong Selatan" => [],
          "Latimojong" => [],
          "Ponrang" => [],
          "Ponrang Selatan" => [],
          "Suli" => [],
          "Suli Barat" => [],
          "Walenrang" => [],
          "Walenrang Barat" => [],
          "Walenrang Timur" => [],
          "Walenrang Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Luwu Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Angkona" => [],
          "Burau" => [],
          "Kalaena" => [],
          "Malili" => [],
          "Mangkutana" => [],
          "Nuha" => [],
          "Tomoni" => [],
          "Tomoni Timur" => [],
          "Towuti" => [],
          "Wasuponda" => [],
          "Wotu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Luwu Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Baebunta" => [],
          "Bone-Bone" => [],
          "Limbong" => [],
          "Malangke" => [],
          "Malangke Barat" => [],
          "Mappedeceng" => [],
          "Masamba" => [],
          "Rampi" => [],
          "Sabbang" => [],
          "Seko" => [],
          "Sukamaju" => [],
          "Tana Lili" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Makassar']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Biring Kanaya" => [],
          "Bontoala" => [],
          "Makassar" => [],
          "Mamajang" => [],
          "Manggala" => [],
          "Mariso" => [],
          "Panakkukang" => [],
          "Rappocini" => [],
          "Tallo" => [],
          "Tamalanrea" => [],
          "Tamalate" => [],
          "Ujung Pandang" => [],
          "Ujung Tanah" => [],
          "Wajo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Maros']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bantimurung" => [],
          "Bontoa (Maros Utara)" => [],
          "Camba" => [],
          "Cenrana" => [],
          "Lau" => [],
          "Mallawa" => [],
          "Mandai" => [],
          "Maros Baru" => [],
          "Marusu" => [],
          "Moncongloe" => [],
          "Simbang" => [],
          "Tanralili" => [],
          "Tompu Bulu" => [],
          "Turikale" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Palopo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bara" => [],
          "Mungkajang" => [],
          "Sendana" => [],
          "Telluwanua" => [],
          "Wara" => [],
          "Wara Barat" => [],
          "Wara Selatan" => [],
          "Wara Timur" => [],
          "Wara Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Pangkajene Kepulauan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Balocci" => [],
          "Bungoro" => [],
          "Labakkang" => [],
          "Liukang Kalmas (Kalukuang Masalima)" => [],
          "Liukang Tangaya" => [],
          "Liukang Tupabbiring" => [],
          "Liukang Tupabbiring Utara" => [],
          "Mandalle" => [],
          "Marang (Ma Rang)" => [],
          "Minasa Tene" => [],
          "Pangkajene" => [],
          "Segeri" => [],
          "Tondong Tallasa" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Parepare']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bacukiki" => [],
          "Bacukiki Barat" => [],
          "Soreang" => [],
          "Ujung" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Pinrang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batulappa" => [],
          "Cempa" => [],
          "Duampanua" => [],
          "Lanrisang" => [],
          "Lembang" => [],
          "Mattiro Bulu" => [],
          "Mattiro Sompe" => [],
          "Paleteang" => [],
          "Patampanua" => [],
          "Suppa" => [],
          "Tiroang" => [],
          "Watang Sawitto" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Selayar (Kepulauan Selayar)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Benteng" => [],
          "Bontoharu" => [],
          "Bontomanai" => [],
          "Bontomatene" => [],
          "Bontosikuyu" => [],
          "Buki" => [],
          "Pasilambena" => [],
          "Pasimarannu" => [],
          "Pasimassunggu" => [],
          "Pasimasunggu Timur" => [],
          "Takabonerate" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Sidenreng Rappang/Rapang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Baranti" => [],
          "Dua Pitue" => [],
          "Kulo" => [],
          "Maritengngae" => [],
          "Panca Lautan (Lautang)" => [],
          "Panca Rijang" => [],
          "Pitu Raise/Riase" => [],
          "Pitu Riawa" => [],
          "Tellu Limpoe" => [],
          "Watang Pulu" => [],
          "Wattang Sidenreng (Watang Sidenreng)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Sinjai']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bulupoddo" => [],
          "Pulau Sembilan" => [],
          "Sinjai Barat" => [],
          "Sinjai Borong" => [],
          "Sinjai Selatan" => [],
          "Sinjai Tengah" => [],
          "Sinjai Timur" => [],
          "Sinjai Utara" => [],
          "Tellu Limpoe" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Soppeng']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Citta" => [],
          "Donri-Donri" => [],
          "Ganra" => [],
          "Lalabata" => [],
          "Lili Rilau" => [],
          "Liliraja (Lili Riaja)" => [],
          "Mario Riawa" => [],
          "Mario Riwawo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Takalar']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Galesong" => [],
          "Galesong Selatan" => [],
          "Galesong Utara" => [],
          "Mangara Bombang" => [],
          "Mappakasunggu" => [],
          "Patallassang" => [],
          "Polombangkeng Selatan (Polobangkeng)" => [],
          "Polombangkeng Utara (Polobangkeng)" => [],
          "Sanrobone" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Tana Toraja']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bittuang" => [],
          "Bonggakaradeng" => [],
          "Gandang Batu Sillanan" => [],
          "Kurra" => [],
          "Makale" => [],
          "Makale Selatan" => [],
          "Makale Utara" => [],
          "Malimbong Balepe" => [],
          "Mappak" => [],
          "Masanda" => [],
          "Mengkendek" => [],
          "Rano" => [],
          "Rantetayo" => [],
          "Rembon" => [],
          "Saluputti" => [],
          "Sangalla (Sanggala)" => [],
          "Sangalla Selatan" => [],
          "Sangalla Utara" => [],
          "Simbuang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Toraja Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Awan Rante Karua" => [],
          "Balusu" => [],
          "Bangkelekila" => [],
          "Baruppu" => [],
          "Buntao" => [],
          "Buntu Pepasan" => [],
          "Dende' Piongan Napo" => [],
          "Kapalla Pitu (Kapala Pitu)" => [],
          "Kesu" => [],
          "Nanggala" => [],
          "Rantebua" => [],
          "Rantepao" => [],
          "Rindingallo" => [],
          "Sa'dan" => [],
          "Sanggalangi" => [],
          "Sesean" => [],
          "Sesean Suloara" => [],
          "Sopai" => [],
          "Tallunglipu" => [],
          "Tikala" => [],
          "Tondon" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Selatan', 'Wajo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Belawa" => [],
          "Bola" => [],
          "Gilireng" => [],
          "Keera" => [],
          "Majauleng" => [],
          "Maniang Pajo" => [],
          "Pammana" => [],
          "Penrang" => [],
          "Pitumpanua" => [],
          "Sabbang Paru" => [],
          "Sajoanging" => [],
          "Takkalalla" => [],
          "Tana Sitolo" => [],
          "Tempe" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tengah', 'Banggai']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Balantak" => [],
          "Balantak Selatan" => [],
          "Balantak Utara" => [],
          "Batui" => [],
          "Batui Selatan" => [],
          "Bualemo (Boalemo)" => [],
          "Bunta" => [],
          "Kintom" => [],
          "Lamala" => [],
          "Lobu" => [],
          "Luwuk" => [],
          "Luwuk Selatan" => [],
          "Luwuk Timur" => [],
          "Luwuk Utara" => [],
          "Mantoh" => [],
          "Masama" => [],
          "Moilong" => [],
          "Nambo" => [],
          "Nuhon" => [],
          "Pagimana" => [],
          "Simpang Raya" => [],
          "Toili" => [],
          "Toili Barat" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tengah', 'Banggai Kepulauan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banggai" => [],
          "Banggai Selatan" => [],
          "Banggai Tengah" => [],
          "Banggai Utara" => [],
          "Bangkurung" => [],
          "Bokan Kepulauan" => [],
          "Buko" => [],
          "Buko Selatan" => [],
          "Bulagi" => [],
          "Bulagi Selatan" => [],
          "Bulagi Utara" => [],
          "Labobo (Lobangkurung)" => [],
          "Liang" => [],
          "Peling Tengah" => [],
          "Tinangkung" => [],
          "Tinangkung Selatan" => [],
          "Tinangkung Utara" => [],
          "Totikum (Totikung)" => [],
          "Totikum Selatan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tengah', 'Buol']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Biau" => [],
          "Bokat" => [],
          "Bukal" => [],
          "Bunobogu" => [],
          "Gadung" => [],
          "Karamat" => [],
          "Lakea (Lipunoto)" => [],
          "Momunu" => [],
          "Paleleh" => [],
          "Paleleh Barat" => [],
          "Tiloan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tengah', 'Donggala']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Balaesang" => [],
          "Balaesang Tanjung" => [],
          "Banawa" => [],
          "Banawa Selatan" => [],
          "Banawa Tengah" => [],
          "Damsol (Dampelas Sojol)" => [],
          "Labuan" => [],
          "Pinembani" => [],
          "Rio Pakava (Riopakawa)" => [],
          "Sindue" => [],
          "Sindue Tobata" => [],
          "Sindue Tombusabora" => [],
          "Sirenja" => [],
          "Sojol" => [],
          "Sojol Utara" => [],
          "Tanantovea" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tengah', 'Morowali']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bahodopi" => [],
          "Bumi Raya" => [],
          "Bungku Barat" => [],
          "Bungku Pesisir" => [],
          "Bungku Selatan" => [],
          "Bungku Tengah" => [],
          "Bungku Timur" => [],
          "Bungku Utara" => [],
          "Lembo" => [],
          "Lembo Raya" => [],
          "Mamosalato" => [],
          "Menui Kepulauan" => [],
          "Mori Atas" => [],
          "Mori Utara" => [],
          "Petasia" => [],
          "Petasia Barat" => [],
          "Petasia Timur" => [],
          "Soyo Jaya" => [],
          "Wita Ponda" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tengah', 'Palu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Mantikulore" => [],
          "Palu Barat" => [],
          "Palu Selatan" => [],
          "Palu Timur" => [],
          "Palu Utara" => [],
          "Tatanga" => [],
          "Tawaeli" => [],
          "Ulujadi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tengah', 'Parigi Moutong']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ampibabo" => [],
          "Balinggi" => [],
          "Bolano" => [],
          "Bolano Lambunu/Lambulu" => [],
          "Kasimbar" => [],
          "Mepanga" => [],
          "Moutong" => [],
          "Ongka Malino" => [],
          "Palasa" => [],
          "Parigi" => [],
          "Parigi Barat" => [],
          "Parigi Selatan" => [],
          "Parigi Tengah" => [],
          "Parigi Utara" => [],
          "Sausu" => [],
          "Siniu" => [],
          "Taopa" => [],
          "Tinombo" => [],
          "Tinombo Selatan" => [],
          "Tomini" => [],
          "Toribulu" => [],
          "Torue" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tengah', 'Poso']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Lage" => [],
          "Lore Barat" => [],
          "Lore Piore" => [],
          "Lore Selatan" => [],
          "Lore Tengah" => [],
          "Lore Timur" => [],
          "Lore Utara" => [],
          "Pamona Barat" => [],
          "Pamona Puselemba" => [],
          "Pamona Selatan" => [],
          "Pamona Tenggara" => [],
          "Pamona Timur" => [],
          "Pamona Utara" => [],
          "Poso Kota" => [],
          "Poso Kota Selatan" => [],
          "Poso Kota Utara" => [],
          "Poso Pesisir" => [],
          "Poso Pesisir Selatan" => [],
          "Poso Pesisir Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tengah', 'Sigi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Dolo" => [],
          "Dolo Barat" => [],
          "Dolo Selatan" => [],
          "Gumbasa" => [],
          "Kinovaru" => [],
          "Kulawi" => [],
          "Kulawi Selatan" => [],
          "Lindu" => [],
          "Marawola" => [],
          "Marawola Barat" => [],
          "Nokilalaki" => [],
          "Palolo" => [],
          "Pipikoro" => [],
          "Sigi Biromaru" => [],
          "Tanambulava" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tengah', 'Tojo Una-Una']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ampana Kota" => [],
          "Ampana Tete" => [],
          "Togean" => [],
          "Tojo" => [],
          "Tojo Barat" => [],
          "Ulu Bongka" => [],
          "Una - Una" => [],
          "Walea Besar" => [],
          "Walea Kepulauan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tengah', 'Toli-Toli']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Baolan" => [],
          "Basidondo" => [],
          "Dako Pamean" => [],
          "Dampal Selatan" => [],
          "Dampal Utara" => [],
          "Dondo" => [],
          "Galang" => [],
          "Lampasio" => [],
          "Ogo Deide" => [],
          "Tolitoli Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tenggara', 'Bau-Bau']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batupoaro" => [],
          "Betoambari" => [],
          "Bungi" => [],
          "Kokalukuna" => [],
          "Lea-Lea" => [],
          "Murhum" => [],
          "Sora Walio (Sorowalio)" => [],
          "Wolio" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tenggara', 'Bombana']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kabaena" => [],
          "Kabaena Barat" => [],
          "Kabaena Selatan" => [],
          "Kabaena Tengah" => [],
          "Kabaena Timur" => [],
          "Kabaena Utara" => [],
          "Kepulauan Masaloka Raya" => [],
          "Lentarai Jaya S. (Lantari Jaya)" => [],
          "Mata Oleo" => [],
          "Mata Usu" => [],
          "Poleang" => [],
          "Poleang Barat" => [],
          "Poleang Selatan" => [],
          "Poleang Tengah" => [],
          "Poleang Tenggara" => [],
          "Poleang Timur" => [],
          "Poleang Utara" => [],
          "Rarowatu" => [],
          "Rarowatu Utara" => [],
          "Rumbia" => [],
          "Rumbia Tengah" => [],
          "Tontonunu (Tontonuwu)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tenggara', 'Buton']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batauga" => [],
          "Batu Atas" => [],
          "Gu" => [],
          "Kadatua" => [],
          "Kapontori" => [],
          "Lakudo" => [],
          "Lapandewa" => [],
          "Lasalimu" => [],
          "Lasalimu Selatan" => [],
          "Mawasangka" => [],
          "Mawasangka Tengah" => [],
          "Mawasangka Timur" => [],
          "Pasar Wajo" => [],
          "Sampolawa" => [],
          "Sangia Mambulu" => [],
          "Siompu" => [],
          "Siompu Barat" => [],
          "Siontapia (Siontapina)" => [],
          "Talaga Raya (Telaga Raya)" => [],
          "Wabula" => [],
          "Wolowa" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tenggara', 'Buton Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bonegunu" => [],
          "Kambowa" => [],
          "Kulisusu (Kalingsusu/Kalisusu)" => [],
          "Kulisusu Barat" => [],
          "Kulisusu Utara" => [],
          "Wakorumba Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tenggara', 'Kendari']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Abeli" => [],
          "Baruga" => [],
          "Kadia" => [],
          "Kambu" => [],
          "Kendari" => [],
          "Kendari Barat" => [],
          "Mandonga" => [],
          "Poasia" => [],
          "Puuwatu" => [],
          "Wua-Wua" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tenggara', 'Kolaka']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Baula" => [],
          "Kolaka" => [],
          "Ladongi" => [],
          "Lalolae" => [],
          "Lambandia (Lambadia)" => [],
          "Latambaga" => [],
          "Loea" => [],
          "Mowewe" => [],
          "Poli Polia" => [],
          "Polinggona" => [],
          "Pomalaa" => [],
          "Samaturu" => [],
          "Tanggetada" => [],
          "Tinondo" => [],
          "Tirawuta" => [],
          "Toari" => [],
          "Uluiwoi" => [],
          "Watumbangga (Watubanggo)" => [],
          "Wolo" => [],
          "Wundulako" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tenggara', 'Kolaka Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batu Putih" => [],
          "Katoi" => [],
          "Kodeoha" => [],
          "Lasusua" => [],
          "Lombai (Lambai)" => [],
          "Ngapa" => [],
          "Pakue" => [],
          "Pakue Tengah" => [],
          "Pakue Utara" => [],
          "Porehu" => [],
          "Ranteangin" => [],
          "Tiwu" => [],
          "Tolala" => [],
          "Watunohu" => [],
          "Wawo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tenggara', 'Konawe']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Abuki" => [],
          "Amonggedo" => [],
          "Anggaberi" => [],
          "Asinua" => [],
          "Besulutu" => [],
          "Bondoala" => [],
          "Kapoiala (Kapoyala)" => [],
          "Konawe" => [],
          "Lalonggasumeeto" => [],
          "Lambuya" => [],
          "Latoma" => [],
          "Meluhu" => [],
          "Onembute" => [],
          "Pondidaha" => [],
          "Puriala" => [],
          "Routa" => [],
          "Sampara" => [],
          "Soropia" => [],
          "Tongauna" => [],
          "Uepai (Uwepai)" => [],
          "Unaaha" => [],
          "Wawonii Barat" => [],
          "Wawonii Selatan" => [],
          "Wawonii Tengah" => [],
          "Wawonii Tenggara" => [],
          "Wawonii Timur" => [],
          "Wawonii Timur Laut" => [],
          "Wawonii Utara" => [],
          "Wawotobi" => [],
          "Wonggeduku" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tenggara', 'Konawe Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Andoolo" => [],
          "Angata" => [],
          "Baito" => [],
          "Basala" => [],
          "Benua" => [],
          "Buke" => [],
          "Kolono" => [],
          "Konda" => [],
          "Laeya" => [],
          "Lainea" => [],
          "Lalembuu / Lalumbuu" => [],
          "Landono" => [],
          "Laonti" => [],
          "Moramo" => [],
          "Moramo Utara" => [],
          "Mowila" => [],
          "Palangga" => [],
          "Palangga Selatan" => [],
          "Ranomeeto" => [],
          "Ranomeeto Barat" => [],
          "Tinanggea" => [],
          "Wolasi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tenggara', 'Konawe Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Andowia" => [],
          "Asera" => [],
          "Langgikima" => [],
          "Lasolo" => [],
          "Lembo" => [],
          "Molawe" => [],
          "Motui" => [],
          "Oheo" => [],
          "Sawa" => [],
          "Wiwirano" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tenggara', 'Muna']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Barangka" => [],
          "Batalaiwaru (Batalaiworu)" => [],
          "Batukara" => [],
          "Bone (Bone Tondo)" => [],
          "Duruka" => [],
          "Kabangka" => [],
          "Kabawo" => [],
          "Katobu" => [],
          "Kontu Kowuna" => [],
          "Kontunaga" => [],
          "Kusambi" => [],
          "Lasalepa" => [],
          "Lawa" => [],
          "Lohia" => [],
          "Maginti" => [],
          "Maligano" => [],
          "Marobo" => [],
          "Napabalano" => [],
          "Napano Kusambi" => [],
          "Parigi" => [],
          "Pasi Kolaga" => [],
          "Pasir Putih" => [],
          "Sawerigadi (Sawerigading/Sewergadi)" => [],
          "Tiworo Kepulauan" => [],
          "Tiworo Selatan" => [],
          "Tiworo Tengah" => [],
          "Tiworo Utara" => [],
          "Tongkuno" => [],
          "Tongkuno Selatan" => [],
          "Towea" => [],
          "Wa Daga" => [],
          "Wakorumba Selatan" => [],
          "Watopute" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Tenggara', 'Wakatobi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Binongko" => [],
          "Kaledupa" => [],
          "Kaledupa Selatan" => [],
          "Togo Binongko" => [],
          "Tomia" => [],
          "Tomia Timur" => [],
          "Wangi-Wangi" => [],
          "Wangi-Wangi Selatan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Bitung']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aertembaga (Bitung Timur)" => [],
          "Girian" => [],
          "Lembeh Selatan (Bitung Selatan)" => [],
          "Lembeh Utara" => [],
          "Madidir (Bitung Tengah)" => [],
          "Maesa" => [],
          "Matuari (Bitung Barat)" => [],
          "Ranowulu (Bitung Utara)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Bolaang Mongondow (Bolmong)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bilalang" => [],
          "Bolaang" => [],
          "Bolaang Timur" => [],
          "Dumoga" => [],
          "Dumoga Barat" => [],
          "Dumoga Tengah" => [],
          "Dumoga Tenggara" => [],
          "Dumoga Timur" => [],
          "Dumoga Utara" => [],
          "Lolak" => [],
          "Lolayan" => [],
          "Passi Barat" => [],
          "Passi Timur" => [],
          "Poigar" => [],
          "Sangtombolang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Bolaang Mongondow Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bolaang Uki" => [],
          "Pinolosian" => [],
          "Pinolosian Tengah" => [],
          "Pinolosian Timur" => [],
          "Posigadan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Bolaang Mongondow Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kotabunan" => [],
          "Modayag" => [],
          "Modayag Barat" => [],
          "Nuangan" => [],
          "Tutuyan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Bolaang Mongondow Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bintauna" => [],
          "Bolang Itang Barat" => [],
          "Bolang Itang Timur" => [],
          "Kaidipang" => [],
          "Pinogaluman" => [],
          "Sangkub" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Kepulauan Sangihe']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kendahe" => [],
          "Kepulauan Marore" => [],
          "Manganitu" => [],
          "Manganitu Selatan" => [],
          "Nusa Tabukan" => [],
          "Tabukan Selatan" => [],
          "Tabukan Selatan Tengah" => [],
          "Tabukan Selatan Tenggara" => [],
          "Tabukan Tengah" => [],
          "Tabukan Utara" => [],
          "Tahuna" => [],
          "Tahuna Barat" => [],
          "Tahuna Timur" => [],
          "Tamako" => [],
          "Tatoareng" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Kepulauan Siau Tagulandang Biaro (Sitaro)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Biaro" => [],
          "Siau Barat" => [],
          "Siau Barat Selatan" => [],
          "Siau Barat Utara" => [],
          "Siau Tengah" => [],
          "Siau Timur" => [],
          "Siau Timur Selatan" => [],
          "Tagulandang" => [],
          "Tagulandang Selatan" => [],
          "Tagulandang Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Kepulauan Talaud']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Beo" => [],
          "Beo Selatan" => [],
          "Beo Utara" => [],
          "Damao (Damau)" => [],
          "Essang" => [],
          "Essang Selatan" => [],
          "Gemeh" => [],
          "Kabaruan" => [],
          "Kalongan" => [],
          "Lirung" => [],
          "Melonguane" => [],
          "Melonguane Timur" => [],
          "Miangas" => [],
          "Moronge" => [],
          "Nanusa" => [],
          "Pulutan" => [],
          "Rainis" => [],
          "Salibabu" => [],
          "Tampan Amma" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Kotamobagu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kotamobagu Barat" => [],
          "Kotamobagu Selatan" => [],
          "Kotamobagu Timur" => [],
          "Kotamobagu Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Manado']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bunaken" => [],
          "Bunaken Kepulauan" => [],
          "Malalayang" => [],
          "Mapanget" => [],
          "Paal Dua" => [],
          "Sario" => [],
          "Singkil" => [],
          "Tikala" => [],
          "Tuminiting" => [],
          "Wanea" => [],
          "Wenang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Minahasa']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Eris" => [],
          "Kakas" => [],
          "Kakas Barat" => [],
          "Kawangkoan" => [],
          "Kawangkoan Barat" => [],
          "Kawangkoan Utara" => [],
          "Kombi" => [],
          "Langowan Barat" => [],
          "Langowan Selatan" => [],
          "Langowan Timur" => [],
          "Langowan Utara" => [],
          "Lembean Timur" => [],
          "Mandolang" => [],
          "Pineleng" => [],
          "Remboken" => [],
          "Sonder" => [],
          "Tombariri" => [],
          "Tombariri Timur" => [],
          "Tombulu" => [],
          "Tompaso" => [],
          "Tompaso Barat" => [],
          "Tondano Barat" => [],
          "Tondano Selatan" => [],
          "Tondano Timur" => [],
          "Tondano Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Minahasa Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Amurang" => [],
          "Amurang Barat" => [],
          "Amurang Timur" => [],
          "Kumelembuai" => [],
          "Maesaan" => [],
          "Modoinding" => [],
          "Motoling" => [],
          "Motoling Barat" => [],
          "Motoling Timur" => [],
          "Ranoyapo" => [],
          "Sinonsayang" => [],
          "Suluun Tareran" => [],
          "Tareran" => [],
          "Tatapaan" => [],
          "Tenga" => [],
          "Tompaso Baru" => [],
          "Tumpaan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Minahasa Tenggara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Belang" => [],
          "Pasan" => [],
          "Pusomaen" => [],
          "Ratahan" => [],
          "Ratahan Timur" => [],
          "Ratatotok" => [],
          "Silian Raya" => [],
          "Tombatu" => [],
          "Tombatu Timur" => [],
          "Tombatu Utara" => [],
          "Touluaan" => [],
          "Touluaan Selatan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Minahasa Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Airmadidi" => [],
          "Dimembe" => [],
          "Kalawat" => [],
          "Kauditan" => [],
          "Kema" => [],
          "Likupang Barat" => [],
          "Likupang Selatan" => [],
          "Likupang Timur" => [],
          "Talawaan" => [],
          "Wori" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sulawesi Utara', 'Tomohon']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Tomohon Barat" => [],
          "Tomohon Selatan" => [],
          "Tomohon Tengah" => [],
          "Tomohon Timur" => [],
          "Tomohon Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Agam']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ampek Nagari (IV Nagari )" => [],
          "Banuhampu" => [],
          "Baso" => [],
          "Candung" => [],
          "IV Angkat Candung (Ampek Angkek)" => [],
          "IV Koto (Ampek Koto)" => [],
          "Kamang Magek" => [],
          "Lubuk Basung" => [],
          "Malakak" => [],
          "Matur" => [],
          "Palembayan" => [],
          "Palupuh" => [],
          "Sungai Pua (Puar)" => [],
          "Tanjung Mutiara" => [],
          "Tanjung Raya" => [],
          "Tilatang Kamang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Bukittinggi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aur Birugo Tigo Baleh" => [],
          "Guguk Panjang (Guguak Panjang)" => [],
          "Mandiangin Koto Selayan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Dharmasraya']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Asam Jujuhan" => [],
          "Koto Baru" => [],
          "Koto Besar" => [],
          "Koto Salak" => [],
          "Padang Laweh" => [],
          "Pulau Punjung" => [],
          "Sembilan Koto (IX Koto)" => [],
          "Sitiung" => [],
          "Sungai Rumbai" => [],
          "Timpeh" => [],
          "Tiumang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Kepulauan Mentawai']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Pagai Selatan" => [],
          "Pagai Utara" => [],
          "Siberut Barat" => [],
          "Siberut Barat Daya" => [],
          "Siberut Selatan" => [],
          "Siberut Tengah" => [],
          "Siberut Utara" => [],
          "Sikakap" => [],
          "Sipora Selatan" => [],
          "Sipora Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Lima Puluh Koto/Kota']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Akabiluru" => [],
          "Bukik Barisan" => [],
          "Guguak (Gugu)" => [],
          "Gunuang Omeh (Gunung Mas)" => [],
          "Harau" => [],
          "Kapur IX/Sembilan" => [],
          "Lareh Sago Halaban" => [],
          "Luak (Luhak)" => [],
          "Mungka" => [],
          "Pangkalan Koto Baru" => [],
          "Payakumbuh" => [],
          "Situjuah Limo/Lima Nagari" => [],
          "Suliki" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Padang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bungus Teluk Kabung" => [],
          "Koto Tangah" => [],
          "Kuranji" => [],
          "Lubuk Begalung" => [],
          "Lubuk Kilangan" => [],
          "Nanggalo" => [],
          "Padang Barat" => [],
          "Padang Selatan" => [],
          "Padang Timur" => [],
          "Padang Utara" => [],
          "Pauh" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Padang Panjang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Padang Panjang Barat" => [],
          "Padang Panjang Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Padang Pariaman']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "2 X 11 Enam Lingkung" => [],
          "2 X 11 Kayu Tanam" => [],
          "Batang Anai" => [],
          "Batang Gasan" => [],
          "Enam Lingkung" => [],
          "IV Koto Aur Malintang" => [],
          "Lubuk Alung" => [],
          "Nan Sabaris" => [],
          "Padang Sago" => [],
          "Patamuan" => [],
          "Sintuk/Sintuak Toboh Gadang" => [],
          "Sungai Geringging/Garingging" => [],
          "Sungai Limau" => [],
          "Ulakan Tapakih/Tapakis" => [],
          "V Koto Kampung Dalam" => [],
          "V Koto Timur" => [],
          "VII Koto Sungai Sarik" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Pariaman']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Pariaman Selatan" => [],
          "Pariaman Tengah" => [],
          "Pariaman Timur" => [],
          "Pariaman Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Pasaman']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bonjol" => [],
          "Duo Koto (II Koto)" => [],
          "Lubuk Sikaping" => [],
          "Mapat Tunggul" => [],
          "Mapat Tunggul Selatan" => [],
          "Padang Gelugur" => [],
          "Panti" => [],
          "Rao" => [],
          "Rao Selatan" => [],
          "Rao Utara" => [],
          "Simpang Alahan Mati" => [],
          "Tigo Nagari (III Nagari)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Pasaman Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Gunung Tuleh" => [],
          "Kinali" => [],
          "Koto Balingka" => [],
          "Lembah Melintang" => [],
          "Luhak Nan Duo" => [],
          "Pasaman" => [],
          "Ranah Batahan" => [],
          "Sasak Ranah Pasisir/Pesisie" => [],
          "Sei Beremas" => [],
          "Sungai Aur" => [],
          "Talamau" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Payakumbuh']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Lamposi Tigo Nagari" => [],
          "Payakumbuh Barat" => [],
          "Payakumbuh Selatan" => [],
          "Payakumbuh Timur" => [],
          "Payakumbuh Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Pesisir Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Airpura" => [],
          "Basa Ampek Balai Tapan" => [],
          "Batang Kapas" => [],
          "Bayang" => [],
          "IV Jurai" => [],
          "IV Nagari Bayang Utara" => [],
          "Koto XI Tarusan" => [],
          "Lengayang" => [],
          "Linggo Sari Baganti" => [],
          "Lunang" => [],
          "Pancung Soal" => [],
          "Ranah Ampek Hulu Tapan" => [],
          "Ranah Pesisir" => [],
          "Silaut" => [],
          "Sutera" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Sawah Lunto']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Barangin" => [],
          "Lembah Segar" => [],
          "Silungkang" => [],
          "Talawi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Sijunjung (Sawah Lunto Sijunjung)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "IV Nagari" => [],
          "Kamang Baru" => [],
          "Koto VII" => [],
          "Kupitan" => [],
          "Lubuak Tarok" => [],
          "Sijunjung" => [],
          "Sumpur Kudus" => [],
          "Tanjung Gadang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Solok (Kabupaten)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bukit Sundi" => [],
          "Danau Kembar" => [],
          "Gunung Talang" => [],
          "Hiliran Gumanti" => [],
          "IX Koto Sei Lasi" => [],
          "Junjung Sirih" => [],
          "Kubung" => [],
          "Lembah Gumanti" => [],
          "Lembang Jaya" => [],
          "Pantai Cermin" => [],
          "Payung Sekaki" => [],
          "Tigo Lurah" => [],
          "X Koto Diatas" => [],
          "X Koto Singkarak" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Solok (Kota)']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Lubuk Sikarah" => [],
          "Tanjung Harapan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Solok Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Koto Parik Gadang Diateh" => [],
          "Pauh Duo" => [],
          "Sangir" => [],
          "Sangir Balai Janggo" => [],
          "Sangir Batang Hari" => [],
          "Sangir Jujuan" => [],
          "Sungai Pagu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Barat', 'Tanah Datar']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batipuh" => [],
          "Batipuh Selatan" => [],
          "Lima Kaum" => [],
          "Lintau Buo" => [],
          "Lintau Buo Utara" => [],
          "Padang Ganting" => [],
          "Pariangan" => [],
          "Rambatan" => [],
          "Salimpaung" => [],
          "Sepuluh Koto (X Koto)" => [],
          "Sungai Tarab" => [],
          "Sungayang" => [],
          "Tanjung Baru" => [],
          "Tanjung Emas" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Banyuasin']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Air Kumbang" => [],
          "Air Salek" => [],
          "Banyuasin I" => [],
          "Banyuasin II" => [],
          "Banyuasin III" => [],
          "Betung" => [],
          "Makarti Jaya" => [],
          "Muara Padang" => [],
          "Muara Sugihan" => [],
          "Muara Telang" => [],
          "Pulau Rimau" => [],
          "Rambutan" => [],
          "Rantau Bayur" => [],
          "Sembawa" => [],
          "Suak Tapeh" => [],
          "Sumber Marga Telang" => [],
          "Talang Kelapa" => [],
          "Tanjung Lago" => [],
          "Tungkal Ilir" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Empat Lawang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Lintang Kanan" => [],
          "Muara Pinang" => [],
          "Pasemah Air Keruh" => [],
          "Pendopo" => [],
          "Pendopo Barat" => [],
          "Saling" => [],
          "Sikap Dalam" => [],
          "Talang Padang" => [],
          "Tebing Tinggi" => [],
          "Ulu Musi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Lahat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Gumay Talang" => [],
          "Gumay Ulu" => [],
          "Jarai" => [],
          "Kikim Barat" => [],
          "Kikim Selatan" => [],
          "Kikim Tengah" => [],
          "Kikim Timur" => [],
          "Kota Agung" => [],
          "Lahat" => [],
          "Merapi Barat" => [],
          "Merapi Selatan" => [],
          "Merapi Timur" => [],
          "Muarapayang" => [],
          "Mulak Ulu" => [],
          "Pagar Gunung" => [],
          "Pajar Bulan" => [],
          "Pseksu" => [],
          "Pulau Pinang" => [],
          "Sukamerindu" => [],
          "Tanjung Sakti Pumi" => [],
          "Tanjung Sakti Pumu" => [],
          "Tanjung Tebat" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Lubuk Linggau']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Lubuk Linggau Barat Dua (II)" => [],
          "Lubuk Linggau Barat Satu (I)" => [],
          "Lubuk Linggau Selatan Dua (II)" => [],
          "Lubuk Linggau Selatan Satu (I)" => [],
          "Lubuk Linggau Timur Dua (II)" => [],
          "Lubuk Linggau Timur Satu (I)" => [],
          "Lubuk Linggau Utara Dua (II)" => [],
          "Lubuk Linggau Utara Satu (I)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Muara Enim']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Abab" => [],
          "Benakat" => [],
          "Gelumbang" => [],
          "Gunung Megang" => [],
          "Kelekar" => [],
          "Lawang Kidul" => [],
          "Lembak" => [],
          "Lubai" => [],
          "Muara Belida" => [],
          "Muara Enim" => [],
          "Penukal (Penukal Abab)" => [],
          "Penukal Utara" => [],
          "Rambang" => [],
          "Rambang Dangku" => [],
          "Semendo Darat Laut" => [],
          "Semendo Darat Tengah" => [],
          "Semendo Darat Ulu" => [],
          "Sungai Rotan" => [],
          "Talang Ubi" => [],
          "Tanah Abang" => [],
          "Tanjung Agung" => [],
          "Ujan Mas" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Musi Banyuasin']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Babat Supat" => [],
          "Babat Toman" => [],
          "Batanghari Leko" => [],
          "Bayung Lencir" => [],
          "Keluang" => [],
          "Lais" => [],
          "Lalan (Sungai Lalan)" => [],
          "Lawang Wetan" => [],
          "Plakat Tinggi (Pelakat Tinggi)" => [],
          "Sanga Desa" => [],
          "Sekayu" => [],
          "Sungai Keruh" => [],
          "Sungai Lilin" => [],
          "Tungkal Jaya" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Musi Rawas']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batukuning Lakitan Ulu (BTS)/Cecar" => [],
          "Jaya Loka" => [],
          "Karang Dapo" => [],
          "Karang Jaya" => [],
          "Megang Sakti" => [],
          "Muara Beliti" => [],
          "Muara Kelingi" => [],
          "Muara Lakitan" => [],
          "Nibung" => [],
          "Purwodadi" => [],
          "Rawas Ilir" => [],
          "Rawas Ulu" => [],
          "Rupit" => [],
          "Selangit" => [],
          "STL Ulu Terawas" => [],
          "Sukakarya" => [],
          "Sumber Harta" => [],
          "Tiang Pumpung Kepungut" => [],
          "Tuah Negeri" => [],
          "Tugumulyo" => [],
          "Ulu Rawas" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Ogan Ilir']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Indralaya" => [],
          "Indralaya Selatan" => [],
          "Indralaya Utara" => [],
          "Kandis" => [],
          "Lubuk Keliat" => [],
          "Muara Kuang" => [],
          "Payaraman" => [],
          "Pemulutan" => [],
          "Pemulutan Barat" => [],
          "Pemulutan Selatan" => [],
          "Rambang Kuang" => [],
          "Rantau Alai" => [],
          "Rantau Panjang" => [],
          "Sungai Pinang" => [],
          "Tanjung Batu" => [],
          "Tanjung Raja" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Ogan Komering Ilir']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Air Sugihan" => [],
          "Cengal" => [],
          "Jejawi" => [],
          "Kayu Agung" => [],
          "Lempuing" => [],
          "Lempuing Jaya" => [],
          "Mesuji" => [],
          "Mesuji Makmur" => [],
          "Mesuji Raya" => [],
          "Pampangan" => [],
          "Pangkalan Lampam" => [],
          "Pedamaran" => [],
          "Pedamaran Timur" => [],
          "Sirah Pulau Padang" => [],
          "Sungai Menang" => [],
          "Tanjung Lubuk" => [],
          "Teluk Gelam" => [],
          "Tulung Selapan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Ogan Komering Ulu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Baturaja Barat" => [],
          "Baturaja Timur" => [],
          "Lengkiti" => [],
          "Lubuk Batang" => [],
          "Lubuk Raja" => [],
          "Muara Jaya" => [],
          "Pengandonan" => [],
          "Peninjauan" => [],
          "Semidang Aji" => [],
          "Sinar Peninjauan" => [],
          "Sosoh Buay Rayap" => [],
          "Ulu Ogan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Ogan Komering Ulu Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Banding Agung" => [],
          "Buana Pemaca" => [],
          "Buay Pemaca" => [],
          "Buay Pematang Ribu Ranau Tengah" => [],
          "Buay Rawan" => [],
          "Buay Runjung" => [],
          "Buay Sandang Aji" => [],
          "Kisam Ilir" => [],
          "Kisam Tinggi" => [],
          "Mekakau Ilir" => [],
          "Muaradua" => [],
          "Muaradua Kisam" => [],
          "Pulau Beringin" => [],
          "Runjung Agung" => [],
          "Simpang" => [],
          "Sindang Danau" => [],
          "Sungai Are" => [],
          "Tiga Dihaji" => [],
          "Warkuk Ranau Selatan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Ogan Komering Ulu Timur']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Belitang" => [],
          "Belitang II" => [],
          "Belitang III" => [],
          "Belitang Jaya" => [],
          "Belitang Madang Raya" => [],
          "Belitang Mulya" => [],
          "Buay Madang" => [],
          "Buay Madang Timur" => [],
          "Buay Pemuka Bangsa Raja" => [],
          "Buay Pemuka Beliung / Peliung" => [],
          "Bunga Mayang" => [],
          "Cempaka" => [],
          "Jayapura" => [],
          "Madang Suku I" => [],
          "Madang Suku II" => [],
          "Madang Suku III" => [],
          "Martapura" => [],
          "Semendawai Barat" => [],
          "Semendawai Suku III" => [],
          "Semendawai Timur" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Pagar Alam']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Dempo Selatan" => [],
          "Dempo Tengah" => [],
          "Dempo Utara" => [],
          "Pagar Alam Selatan" => [],
          "Pagar Alam Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Palembang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Alang-Alang Lebar" => [],
          "Bukit Kecil" => [],
          "Gandus" => [],
          "Ilir Barat I" => [],
          "Ilir Barat II" => [],
          "Ilir Timur I" => [],
          "Ilir Timur II" => [],
          "Kalidoni" => [],
          "Kemuning" => [],
          "Kertapati" => [],
          "Plaju" => [],
          "Sako" => [],
          "Seberang Ulu I" => [],
          "Seberang Ulu II" => [],
          "Sematang Borang" => [],
          "Sukarami" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Selatan', 'Prabumulih']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Cambai" => [],
          "Prabumulih Barat" => [],
          "Prabumulih Selatan" => [],
          "Prabumulih Timur" => [],
          "Prabumulih Utara" => [],
          "Rambang Kapak Tengah" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Asahan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aek Kuasan" => [],
          "Aek Ledong" => [],
          "Aek Songsongan" => [],
          "Air Batu" => [],
          "Air Joman" => [],
          "Bandar Pasir Mandoge" => [],
          "Bandar Pulau" => [],
          "Buntu Pane" => [],
          "Kisaran Barat Kota" => [],
          "Kisaran Timur Kota" => [],
          "Meranti" => [],
          "Pulau Rakyat" => [],
          "Pulo Bandring" => [],
          "Rahuning" => [],
          "Rawang Panca Arga" => [],
          "Sei Dadap" => [],
          "Sei Kepayang" => [],
          "Sei Kepayang Barat" => [],
          "Sei Kepayang Timur" => [],
          "Setia Janji" => [],
          "Silau Laut" => [],
          "Simpang Empat" => [],
          "Tanjung Balai" => [],
          "Teluk Dalam" => [],
          "Tinggi Raja" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Batu Bara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Air Putih" => [],
          "Limapuluh" => [],
          "Medang Deras" => [],
          "Sei Balai" => [],
          "Sei Suka" => [],
          "Talawi" => [],
          "Tanjung Tiram" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Binjai']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Binjai Barat" => [],
          "Binjai Kota" => [],
          "Binjai Selatan" => [],
          "Binjai Timur" => [],
          "Binjai Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Dairi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Berampu (Brampu)" => [],
          "Gunung Sitember" => [],
          "Lae Parira" => [],
          "Parbuluan" => [],
          "Pegagan Hilir" => [],
          "Sidikalang" => [],
          "Siempat Nempu" => [],
          "Siempat Nempu Hilir" => [],
          "Siempat Nempu Hulu" => [],
          "Silahi Sabungan" => [],
          "Silima Pungga-Pungga" => [],
          "Sitinjo" => [],
          "Sumbul" => [],
          "Tanah Pinem" => [],
          "Tiga Lingga" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Deli Serdang']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bangun Purba" => [],
          "Batang Kuis" => [],
          "Beringin" => [],
          "Biru-Biru" => [],
          "Deli Tua" => [],
          "Galang" => [],
          "Gunung Meriah" => [],
          "Hamparan Perak" => [],
          "Kutalimbaru" => [],
          "Labuhan Deli" => [],
          "Lubuk Pakam" => [],
          "Namo Rambe" => [],
          "Pagar Merbau" => [],
          "Pancur Batu" => [],
          "Pantai Labu" => [],
          "Patumbak" => [],
          "Percut Sei Tuan" => [],
          "Sibolangit" => [],
          "Sinembah Tanjung Muda Hilir" => [],
          "Sinembah Tanjung Muda Hulu" => [],
          "Sunggal" => [],
          "Tanjung Morawa" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Gunungsitoli']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Gunungsitoli" => [],
          "Gunungsitoli Alo'oa" => [],
          "Gunungsitoli Barat" => [],
          "Gunungsitoli Idanoi" => [],
          "Gunungsitoli Selatan" => [],
          "Gunungsitoli Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Humbang Hasundutan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bakti Raja" => [],
          "Dolok Sanggul" => [],
          "Lintong Nihuta" => [],
          "Onan Ganjang" => [],
          "Pakkat" => [],
          "Paranginan" => [],
          "Parlilitan" => [],
          "Pollung" => [],
          "Sijama Polang" => [],
          "Tara Bintang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Karo']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Barus Jahe" => [],
          "Brastagi (Berastagi)" => [],
          "Dolat Rayat" => [],
          "Juhar" => [],
          "Kabanjahe" => [],
          "Kuta Buluh" => [],
          "Laubaleng" => [],
          "Mardinding" => [],
          "Merdeka" => [],
          "Merek" => [],
          "Munte" => [],
          "Nama Teran" => [],
          "Payung" => [],
          "Simpang Empat" => [],
          "Tiga Binanga" => [],
          "Tiga Panah" => [],
          "Tiganderket" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Labuhan Batu']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bilah Barat" => [],
          "Bilah Hilir" => [],
          "Bilah Hulu" => [],
          "Panai Hilir" => [],
          "Panai Hulu" => [],
          "Panai Tengah" => [],
          "Pangkatan" => [],
          "Rantau Selatan" => [],
          "Rantau Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Labuhan Batu Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kampung Rakyat" => [],
          "Kota Pinang" => [],
          "Sei/Sungai Kanan" => [],
          "Silangkitang" => [],
          "Torgamba" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Labuhan Batu Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aek Kuo" => [],
          "Aek Natas" => [],
          "Kuala Ledong (Kualuh Leidong)" => [],
          "Kualuh Hilir" => [],
          "Kualuh Hulu" => [],
          "Kualuh Selatan" => [],
          "Marbau" => [],
          "Na IX-X" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Langkat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Babalan" => [],
          "Bahorok" => [],
          "Batang Serangan" => [],
          "Besitang" => [],
          "Binjai" => [],
          "Brandan Barat" => [],
          "Gebang" => [],
          "Hinai" => [],
          "Kuala" => [],
          "Kutambaru" => [],
          "Padang Tualang" => [],
          "Pangkalan Susu" => [],
          "Pematang Jaya" => [],
          "Salapian" => [],
          "Sawit Seberang" => [],
          "Secanggang" => [],
          "Sei Binge (Bingai)" => [],
          "Sei Lepan" => [],
          "Selesai" => [],
          "Sirapit (Serapit)" => [],
          "Stabat" => [],
          "Tanjungpura" => [],
          "Wampu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Mandailing Natal']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batahan" => [],
          "Batang Natal" => [],
          "Bukit Malintang" => [],
          "Huta Bargot" => [],
          "Kotanopan" => [],
          "Langga Bayu (Lingga Bayu)" => [],
          "Lembah Sorik Merapi" => [],
          "Muara Batang Gadis" => [],
          "Muara Sipongi" => [],
          "Naga Juang" => [],
          "Natal" => [],
          "Pakantan" => [],
          "Panyabungan Barat" => [],
          "Panyabungan Kota" => [],
          "Panyabungan Selatan" => [],
          "Panyabungan Timur" => [],
          "Panyabungan Utara" => [],
          "Puncak Sorik Marapi/Merapi" => [],
          "Ranto Baek/Baik" => [],
          "Siabu" => [],
          "Sinunukan" => [],
          "Tambangan" => [],
          "Ulu Pungkut" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Medan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Medan Amplas" => [],
          "Medan Area" => [],
          "Medan Barat" => [],
          "Medan Baru" => [],
          "Medan Belawan Kota" => [],
          "Medan Deli" => [],
          "Medan Denai" => [],
          "Medan Helvetia" => [],
          "Medan Johor" => [],
          "Medan Kota" => [],
          "Medan Labuhan" => [],
          "Medan Maimun" => [],
          "Medan Marelan" => [],
          "Medan Perjuangan" => [],
          "Medan Petisah" => [],
          "Medan Polonia" => [],
          "Medan Selayang" => [],
          "Medan Sunggal" => [],
          "Medan Tembung" => [],
          "Medan Timur" => [],
          "Medan Tuntungan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Nias']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bawolato" => [],
          "Botomuzoi" => [],
          "Gido" => [],
          "Hili Serangkai (Hilisaranggu)" => [],
          "Hiliduho" => [],
          "Idano Gawo" => [],
          "Ma'u" => [],
          "Sogae Adu (Sogaeadu)" => [],
          "Somolo-Molo (Samolo)" => [],
          "Ulugawo" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Nias Barat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Lahomi (Gahori)" => [],
          "Lolofitu Moi" => [],
          "Mandrehe" => [],
          "Mandrehe Barat" => [],
          "Mandrehe Utara" => [],
          "Moro'o" => [],
          "Sirombu" => [],
          "Ulu Moro'o (Ulu Narwo)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Nias Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Amandraya" => [],
          "Aramo" => [],
          "Boronadu" => [],
          "Fanayama" => [],
          "Gomo" => [],
          "Hibala" => [],
          "Hilimegai" => [],
          "Hilisalawa'ahe (Hilisalawaahe)" => [],
          "Huruna" => [],
          "Lahusa" => [],
          "Lolomatua" => [],
          "Lolowau" => [],
          "Maniamolo" => [],
          "Mazino" => [],
          "Mazo" => [],
          "O'o'u (Oou)" => [],
          "Onohazumba" => [],
          "Pulau-Pulau Batu" => [],
          "Pulau-Pulau Batu Barat" => [],
          "Pulau-Pulau Batu Timur" => [],
          "Pulau-Pulau Batu Utara" => [],
          "Sidua'ori" => [],
          "Simuk" => [],
          "Somambawa" => [],
          "Susua" => [],
          "Tanah Masa" => [],
          "Teluk Dalam" => [],
          "Toma" => [],
          "Ulunoyo" => [],
          "Ulususua" => [],
          "Umbunasi" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Nias Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Afulu" => [],
          "Alasa" => [],
          "Alasa Talumuzoi" => [],
          "Lahewa" => [],
          "Lahewa Timur" => [],
          "Lotu" => [],
          "Namohalu Esiwa" => [],
          "Sawo" => [],
          "Sitolu Ori" => [],
          "Tugala Oyo" => [],
          "Tuhemberua" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Padang Lawas']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aek Nabara Barumun" => [],
          "Barumun" => [],
          "Barumun Selatan" => [],
          "Barumun Tengah" => [],
          "Batang Lubu Sutam" => [],
          "Huristak" => [],
          "Huta Raja Tinggi" => [],
          "Lubuk Barumun" => [],
          "Sihapas Barumun" => [],
          "Sosa" => [],
          "Sosopan" => [],
          "Ulu Barumun" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Padang Lawas Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Batang Onang" => [],
          "Dolok" => [],
          "Dolok Sigompulon" => [],
          "Halongonan" => [],
          "Hulu Sihapas" => [],
          "Padang Bolak" => [],
          "Padang Bolak Julu" => [],
          "Portibi" => [],
          "Simangambat" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Padang Sidempuan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Padang Sidempuan Angkola Julu" => [],
          "Padang Sidempuan Batunadua" => [],
          "Padang Sidempuan Hutaimbaru" => [],
          "Padang Sidempuan Selatan" => [],
          "Padang Sidempuan Tenggara" => [],
          "Padang Sidempuan Utara (Padangsidimpuan)" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Pakpak Bharat']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Kerajaan" => [],
          "Pagindar" => [],
          "Pergetteng Getteng Sengkut" => [],
          "Salak" => [],
          "Siempat Rube" => [],
          "Sitellu Tali Urang Jehe" => [],
          "Sitellu Tali Urang Julu" => [],
          "Tinada" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Pematang Siantar']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Siantar Barat" => [],
          "Siantar Marihat" => [],
          "Siantar Marimbun" => [],
          "Siantar Martoba" => [],
          "Siantar Selatan" => [],
          "Siantar Sitalasari" => [],
          "Siantar Timur" => [],
          "Siantar Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Samosir']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Harian" => [],
          "Nainggolan" => [],
          "Onan Runggu" => [],
          "Palipi" => [],
          "Pangururan" => [],
          "Ronggur Nihuta" => [],
          "Sianjur Mula-Mula" => [],
          "Simanindo" => [],
          "Sitio-Tio" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Serdang Bedagai']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bandar Khalifah" => [],
          "Bintang Bayu" => [],
          "Dolok Masihul" => [],
          "Dolok Merawan" => [],
          "Kotarih" => [],
          "Pantai Cermin" => [],
          "Pegajahan" => [],
          "Perbaungan" => [],
          "Sei Bamban" => [],
          "Sei Rampah" => [],
          "Serba Jadi" => [],
          "Silinda" => [],
          "Sipispis" => [],
          "Tanjung Beringin" => [],
          "Tebing Syahbandar" => [],
          "Tebing Tinggi" => [],
          "Teluk Mengkudu" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Sibolga']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Sibolga Kota" => [],
          "Sibolga Sambas" => [],
          "Sibolga Selatan" => [],
          "Sibolga Utara" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Simalungun']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bandar" => [],
          "Bandar Huluan" => [],
          "Bandar Masilam" => [],
          "Bosar Maligas" => [],
          "Dolok Batu Nanggar" => [],
          "Dolok Panribuan" => [],
          "Dolok Pardamean" => [],
          "Dolok Silau" => [],
          "Girsang Sipangan Bolon" => [],
          "Gunung Malela" => [],
          "Gunung Maligas" => [],
          "Haranggaol Horison" => [],
          "Hatonduhan" => [],
          "Huta Bayu Raja" => [],
          "Jawa Maraja Bah Jambi" => [],
          "Jorlang Hataran" => [],
          "Panei" => [],
          "Panombeian Panei" => [],
          "Pematang Bandar" => [],
          "Pematang Sidamanik" => [],
          "Pematang Silima Huta" => [],
          "Purba" => [],
          "Raya" => [],
          "Raya Kahean" => [],
          "Siantar" => [],
          "Sidamanik" => [],
          "Silimakuta" => [],
          "Silou Kahean" => [],
          "Tanah Jawa" => [],
          "Tapian Dolok" => [],
          "Ujung Padang" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Tanjung Balai']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Datuk Bandar" => [],
          "Datuk Bandar Timur" => [],
          "Sei Tualang Raso" => [],
          "Tanjung Balai Selatan" => [],
          "Tanjung Balai Utara" => [],
          "Teluk Nibung" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Tapanuli Selatan']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Aek Bilah" => [],
          "Angkola Barat (Padang Sidempuan)" => [],
          "Angkola Sangkunur" => [],
          "Angkola Selatan (Siais)" => [],
          "Angkola Timur (Padang Sidempuan)" => [],
          "Arse" => [],
          "Batang Angkola" => [],
          "Batang Toru" => [],
          "Marancar" => [],
          "Muara Batang Toru" => [],
          "Saipar Dolok Hole" => [],
          "Sayur Matinggi" => [],
          "Sipirok" => [],
          "Tano Tombangan Angkola" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Tapanuli Tengah']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Andam Dewi" => [],
          "Badiri" => [],
          "Barus" => [],
          "Barus Utara" => [],
          "Kolang" => [],
          "Lumut" => [],
          "Manduamas" => [],
          "Pandan" => [],
          "Pasaribu Tobing" => [],
          "Pinangsori" => [],
          "Sarudik" => [],
          "Sibabangun" => [],
          "Sirandorung" => [],
          "Sitahuis" => [],
          "Sorkam" => [],
          "Sorkam Barat" => [],
          "Sosor Gadong" => [],
          "Suka Bangun" => [],
          "Tapian Nauli" => [],
          "Tukka" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Tapanuli Utara']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Adian Koting" => [],
          "Garoga" => [],
          "Muara" => [],
          "Pagaran" => [],
          "Pahae Jae" => [],
          "Pahae Julu" => [],
          "Pangaribuan" => [],
          "Parmonangan" => [],
          "Purbatua" => [],
          "Siatas Barita" => [],
          "Siborong-Borong" => [],
          "Simangumban" => [],
          "Sipahutar" => [],
          "Sipoholon" => [],
          "Tarutung" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Tebing Tinggi']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Bajenis" => [],
          "Padang Hilir" => [],
          "Padang Hulu" => [],
          "Rambutan" => [],
          "Tebing Tinggi Kota" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['ID', 'Sumatera Utara', 'Toba Samosir']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          "Ajibata" => [],
          "Balige" => [],
          "Bonatua Lunasi" => [],
          "Bor-Bor" => [],
          "Habinsaran" => [],
          "Laguboti" => [],
          "Lumban Julu" => [],
          "Nassau" => [],
          "Parmaksian" => [],
          "Pintu Pohan Meranti" => [],
          "Porsea" => [],
          "Siantar Narumonda" => [],
          "Sigumpar" => [],
          "Silaen" => [],
          "Tampahan" => [],
          "Uluan" => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }

  }

}
